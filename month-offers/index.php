<?if(!isset($_REQUEST["AJAX_REQUEST"])){
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	$APPLICATION->SetTitle("Предложения месяца");
}else{
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
}?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"special.month",
	[
		"IBLOCK_ID" => 27,
		"NEWS_COUNT" => 3,
		"PROPERTY_CODE" => [
			"F_IMG"
		]
	],
	false
);?>

<div class="page__section">
	<div class="tradein tradein--flow">
		<div class="tradein__form">
			<div class="h1 wow fadeInUp" data-wow-delay=".2s" data-wow-duration=".5s">
				Как выбрать то, что нужно?
			</div>
			<div class="small wow fadeInUp" data-wow-delay=".2s" data-wow-duration=".5s">
				Заполните анкету, и в течение часа мы подготовим <br>
				оптимальное предложение, которое подойдет именно Вам
			</div>
			<form class="form wow fadeIn js-form" data-type="files" data-wow-delay=".2s" data-wow-duration=".5s" enctype="multipart/form-data" id="tradeinForm" method="post" name="tradeinForm" data-ya-target="order_done">
				<input type="hidden" name="action" value="MONTH">
				<?=bitrix_sessid_post();?>
				<div class="form__row">
					<div class="form__item">
						<div class="custom-select-wrap">
							<select class="custom-select js-select" name="FIELDS[DISTRICT]">
								<option></option>
								<option value="Октябрьский">Октябрьский</option>
								<option value="Свердловкий">Свердловкий</option>
								<option value="Кировский">Кировский</option>
							</select>
							<div class="custom-select-label">Удобный район для проживания</div>
						</div>
					</div>
					<div class="form__item">
						<div class="custom-select-wrap">
							<select class="custom-select js-select" name="FIELDS[ROOMS]">
								<option></option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
							<div class="custom-select-label">Комфортное количество комнат</div>
						</div>
					</div>
				</div>
				<div class="form__row">
					<div class="form__item">
						<div class="custom-select-wrap">
							<select class="custom-select js-select" name="FIELDS[YEAR]">
								<option value=""></option>
								<option value="2019">2019</option>
								<option value="2020">2020</option>
								<option value="2021">2021</option>
							</select>
							<div class="custom-select-label">Желаемый срок сдачи жилого комплекса</div>
						</div>
					</div>
					<div class="form__item">
						<div class="custom-select-wrap">
							<select class="custom-select js-select" name="FIELDS[FLOOR]">
								<option value=""></option>
								<option value="до 4 этажа">до 4 этажа</option>
								<option value="от 5 до 10 этажа">от 5 до 10 этажа</option>
								<option value="от 11 этажа">от 11 этажа</option>
								<option value="С панарамными видами">С панарамными видами</option>
							</select>
							<div class="custom-select-label">Предпочтения по этажу</div>
						</div>
					</div>
				</div>
				<div class="form__row">
					<div class="form__item">
						<div class="custom-select-wrap">
							<select data-tags="true" class="custom-select js-select" name="FIELDS[INFRA]">
								<option value=""></option>
								<option value="Развитая инфрастуктура">Развитая инфрастуктура</option>
								<option value="Высокая транспортная доступность">Высокая транспортная доступность</option>
								<option value="Концентрация бизнеса">Концентрация бизнеса</option>
								<option value="Соседство с зелеными зонами">Соседство с зелеными зонами</option>
								<option value="Спортивная инфраструктура">Спортивная инфраструктура</option>
								<option value="Разнообразие детского досуга">Разнообразие детского досуга</option>
							</select>
							<div class="custom-select-label">Приоритетные характеристики среды</div>
						</div>
					</div>
					<div class="form__item">
						<div class="custom-select-wrap">
							<select class="custom-select js-select" name="FIELDS[PRESET]">
								<option value=""></option>
								<option value="классика">классика</option>
								<option value="евро">евро</option>
							</select>
							<div class="custom-select-label">Вид планировочного решения</div>
						</div>
					</div>
				</div>
				<div class="form__row">
					<div class="form__item">
						<div class="custom-select-wrap">
							<select data-tags="true" class="custom-select js-select" name="FIELDS[MORE_COMFORT]">
								<option value=""></option>
								<option value="Подземный паркинг">Подземный паркинг</option>
								<option value="Кладовые для хранения">Кладовые для хранения</option>
								<option value="Колясочные">Колясочные</option>
								<option value="Консъерж и видеонаблюдение">Консъерж и видеонаблюдение</option>
								<option value="Безбарьерная среда">Безбарьерная среда</option>
								<option value="Авторская детская площадка">Авторская детская площадка</option>
								<option value="Ландшафтный дизайн">Ландшафтный дизайн</option>
							</select>
							<div class="custom-select-label">Дополнительные функции комфорта</div>
						</div>
					</div>
					<div class="form__item">
						<div class="custom-select-wrap">
							<select class="custom-select js-select" name="FIELDS[PAY]">
								<option value=""></option>
								<option value="Наличные">Наличные</option>
								<option value="рассрочка от застройщика">рассрочка от застройщика</option>
								<option value="ипотека">ипотека</option>
								<option value="trade-in">trade-in</option>
							</select>
							<div class="custom-select-label">Предлагаемая форма расчетов</div>
						</div>
					</div>
				</div>

				<div class="form__row">
					<label class="input-label">
						<span>Ваше имя</span>
						<input class="required" name="FIELDS[NAME]" type="text">
					</label>
					<label class="input-label">
						<span>Ваш телефон</span>
						<input class="required" name="FIELDS[PHONE]" type="text">
					</label>
				</div>

				<div class="form__row">
					<label class="input-label  input-label--checkbox">
						<input type="checkbox" name="agreed" class="checkbox required">
						<span class="checkbox-custom  checkbox-custom--mark"></span>
						<span class="input-label__agreed-text">Согласен с условиями <a href="/personal-confirm/">Согласия</a> на обработку персональных данных</span>
					</label>
				</div>

				<div class="form__row">
					<button class="button button--center-black" type="submit" data-hovercontent="отправить заявку" data-content="отправить заявку"></button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	(function(){
		document.addEventListener('DOMContentLoaded', function(event){
			initSelect2($('.js-select'));
			$('.js-select').on('select2:select', function(e) {
				$(this).addClass('is-filled');
			});
			$('.js-select').on('select2:open', function (e) {
				if($(this).attr('data-tags') == "true"){
					$('.custom-select__dropdown.is-tags').find('.select2-search__subtitle').remove();
					$('.custom-select__dropdown.is-tags').find('.select2-search').prepend('<span class="select2-search__subtitle">Ваш вариант:</span>');
				}
			});
		});
	})()
</script>

<?if(!isset($_REQUEST["AJAX_REQUEST"])){
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
}?>