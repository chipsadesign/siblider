<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты СК Сиблидер");
$APPLICATION->SetDirProperty("page_class", "page--no-padding-top");
$APPLICATION->SetDirProperty("page__wrap_class", "contacts");
?><div class="contacts__header" itemscope="" itemtype="http://schema.org/Organization">
	<div class="contacts__main-info">
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "contacts",
            Array(
                "IBLOCK_ID" => 20,
                "PROPERTY_CODE" => ["S_CONTACTS"]
            )
        );?>
	</div>
	<div class="contacts__address-wrap">
		<div class="contacts__address wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
			 <?$APPLICATION->IncludeComponent(
            	"bitrix:main.include",
            	".default",
            	Array(
            		"AREA_FILE_SHOW" => "file",
            		"COMPONENT_TEMPLATE" => ".default",
            		"EDIT_TEMPLATE" => "",
            		"PATH" => SITE_TEMPLATE_PATH."/includes/pages/contacts/address.php"
            	)
            );?>
		</div>
 <a data-fancybox="map" data-src="#modalMap" href="javascript:;" class="contacts__address-link hover-underline wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">смотреть на карте</a>
		<div class="contacts__working-hours wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
			<div class="contacts__info-item">
				<div class="contacts__info-title">
					часы работы
				</div>
				<div class="contacts__info-value">
					 <?$APPLICATION->IncludeComponent(
                    	"bitrix:main.include",
                    	".default",
                    	Array(
                    		"AREA_FILE_SHOW" => "file",
                    		"COMPONENT_TEMPLATE" => ".default",
                    		"EDIT_TEMPLATE" => "",
                    		"PATH" => SITE_TEMPLATE_PATH."/includes/pages/contacts/worktime.php"
                    	)
                    );?>
				</div>
			</div>
			<div class="contacts__info-item">
				<div class="contacts__info-title">
					<br>
				</div>
				<div class="contacts__info-value">
					 <?$APPLICATION->IncludeComponent(
                    	"bitrix:main.include",
                    	".default",
                    	Array(
                    		"AREA_FILE_SHOW" => "file",
                    		"COMPONENT_TEMPLATE" => ".default",
                    		"EDIT_TEMPLATE" => "",
                    		"PATH" => SITE_TEMPLATE_PATH."/includes/pages/contacts/dinner.php"
                    	)
                    );?>
				</div>
			</div>
			<div class="contacts__info-item">
				<div class="contacts__info-title">
					Суббота - Воскресенье
				</div>
				<div class="contacts__info-value">
					 <?$APPLICATION->IncludeComponent(
                    	"bitrix:main.include",
                    	".default",
                    	Array(
                    		"AREA_FILE_SHOW" => "file",
                    		"COMPONENT_TEMPLATE" => ".default",
                    		"EDIT_TEMPLATE" => "",
                    		"PATH" => SITE_TEMPLATE_PATH."/includes/pages/contacts/weekend.php"
                    	)
                    );?>
				</div>
			</div>
		</div>
		 <!-- <div class="contacts__optional-info wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".4s">Индивидуальные визиты в удобное для Вас время проводятся по записи.</div> -->
	</div>
    <div  class="contacts__photo" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/about_2.jpg');"></div>
	<div class="contacts__info">
		 <?$APPLICATION->ShowViewContent("other_contacts");?>
	</div>
</div>
 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"managers",
	Array(
		"IBLOCK_ID" => IBLOCK_ID_MANAGER,
		"PROPERTY_CODE" => ["S_POSITION",]
	)
);?>
<div class="modal modal--map" style="display: none;" id="modalMap">
	<div class="modal__wrap">
		<div class="modal__map" id="map">
		</div>
	</div>
</div>
<script>
    (function(){
        document.addEventListener('DOMContentLoaded', function(event){
            $('.page-header').addClass('page-header--light');
            $('.js-open-button').on('click', function(){
                $(this).toggleClass('is-opened');
                $(this).siblings().slideToggle(300);
            });
            if ($(window).width() <= 1024) {
                $('.js-manager:nth-child(2n)').attr('data-wow-delay', '.2s');
                $('.js-manager:nth-child(2n-1)').attr('data-wow-delay', '.3s');
            }
            var map,
                styles =
                [
                    {
                        "featureType": "landscape.man_made",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#ededed"
                            },
                            {
                                "lightness": "100"
                            },
                            {
                                "saturation": "1"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape.man_made",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "saturation": "100"
                            },
                            {
                                "color": "#e6e6e6"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#9e9e9e"
                            },
                            {
                                "lightness": "88"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "all",
                        "stylers": [
                            {
                                "saturation": "1"
                            },
                            {
                                "lightness": "1"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#b9947f"
                            },
                            {
                                "lightness": "86"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#7b7b7b"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#b9947f"
                            },
                            {
                                "weight": "2.50"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [
                            {
                                "color": "#25a4d8"
                            },
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#cee7e9"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#939393"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    }
                ];
            initMap(map);
            initFancyBox();

            function initMap(map) {
                var coordinates = { lat: 55.992831, lng: 92.898326 };

                map = new google.maps.Map(document.getElementById('map'), {
                    center: coordinates,
                    zoom: 16,
                    disableDefaultUI: true,
                    zoomControl: true,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.LEFT_BOTTOM
                    },
                    gestureHandling: 'greedy'
                });
                var image = {
                    url: '<?=SITE_TEMPLATE_PATH?>/images/point.svg',
                    scaledSize: new google.maps.Size(50, 50),
                };
                map.setOptions({ styles: styles });
                var marker = new google.maps.Marker({
                    position: coordinates,
                    map: map,
                    animation: google.maps.Animation.DROP,
                    icon: image
                });
            }
        });
    })()
</script><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>