<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Журнал изменений");
$APPLICATION->AddHeadString("<link rel=\"canonical\" href=\"".(CMain::IsHTTPS() ? "https://" : "http://").$_SERVER["SERVER_NAME"].$APPLICATION->GetCurDir()."\"/>");
?>

<?//$APPLICATION->SetDirProperty("page__wrap_class", "article");?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"journal",
	[
		"IBLOCK_ID" => 25,
		"NEWS_COUNT" => 20,
		"PROPERTY_CODE" => [
			"F_FILE",
			"E_USER",
			"E_HOUSE",
			"S_VIDEO"
		],
		"FIELD_CODE" => [
			"DATE_CREATE"
		]
	],
	false
);?>

<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");?>