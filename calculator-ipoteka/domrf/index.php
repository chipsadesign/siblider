<? require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Ипотека ДОМ.РФ");
$APPLICATION->SetPageProperty("page__wrap_class", "article");
echo '<div class="editor__inner">';
?><h2>Бизнес-ипотека&nbsp;от ДОМ.РФ</h2>
<h2>Основные условия кредитования</h2>
<p>
	 Платите за&nbsp;собственную недвижимость<br>
	 от&nbsp;7.5% годовых по&nbsp;программе Минэкономразвития&nbsp;РФ
</p>
 <br>
<table cellspacing="0" cellpadding="0">
 <colgroup><col><col></colgroup>
<tbody>
<tr>
	<td>
		 Название банка
	</td>
	<td>
		 Банк ДОМ.РФ<br>
	</td>
</tr>
<tr>
	<td>
		 Название программы
	</td>
	<td>
		 Бизнес-ипотека
	</td>
</tr>
<tr>
	<td>
		 Процентная ставка
	</td>
	<td>
		 7.5%
	</td>
</tr>
<tr>
	<td>
		 Первоначальный взнос*
	</td>
	<td>
		 20%
	</td>
</tr>
<tr>
	<td>
		 Максимальная сумма до
	</td>
	<td>
		 20 000 000 руб.
	</td>
</tr>
<tr>
	<td>
		 Минимальный срок
	</td>
	<td>
		 3 года
	</td>
</tr>
<tr>
	<td>
		 Максимальный срок
	</td>
	<td>
		 30 лет
	</td>
</tr>
<tr>
	<td>
		 Минимальный возраст заемщика
	</td>
	<td>
		 21 лет
	</td>
</tr>
<tr>
	<td>
		 Максимальный возраст заемщика
	</td>
	<td>
		 65 лет
	</td>
</tr>
</tbody>
</table>
 <br>
 *&nbsp;Залог приобретаемой коммерческой недвижимости<br>
 <br>
<br><?
echo '</div>';
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>