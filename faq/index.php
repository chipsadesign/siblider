<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Вопрос-ответ СК «СибЛидер»|FAQ");
$APPLICATION->SetPageProperty("description", "Задавайте интересующие вас вопросы и специалисты компании «СибЛидер» ответят вам!");
?>

<div class="page__header  page__header--with-border">
	<h1 class="page__title">Вопрос-ответ</h1>
	<a data-fancybox data-src="#askForm" href="javascript:;" class="page__header-link  page__header-link--right"><span class="hover-underline">задать вопрос</span></a>
</div>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"faq",
	[
		"IBLOCK_ID" => 6,
		"SET_TITLE" => "N",
		"NEWS_COUNT" => 200,
		"FIELD_CODE" => [
			"PREVIEW_TEXT",
			"DETAIL_TEXT",
			"DATE_CREATE"
		]
	],
	false
);?>

<div class="modal modal--flat" style="display: none;" id="askForm">
	<div class="modal__wrap">
		<div class="modal__title h1">Задать вопрос</div>
		<form class="modal__form js-form">
			<?=bitrix_sessid_post();?>
			<input type="hidden" name="action" value="FAQ">
			<input type="hidden" name="function" value="addFaq">
			<label class="input-label">
				<span>* Фамилия Имя Отчество</span>
				<input type="text" name="FIELDS[NAME]" class="required">
			</label>
			<label class="input-label">
				<span>* Адрес эл. почты</span>
				<input type="email" name="FIELDS[EMAIL]" class="required">
			</label>
			<label class="textarea-label">
				<span>* Вопрос</span>
				<textarea name="FIELDS[TEXT]" class="required"></textarea>
			</label>
			<label class="input-label  input-label--checkbox">
				<input type="checkbox" name="agreed" class="checkbox required">
				<span class="checkbox-custom  checkbox-custom--mark"></span>
				<span class="input-label__agreed-text">Согласен с условиями <a href="/personal-confirm/">Согласия</a> на обработку персональных данных</span>
			</label>
			<button class="modal__submit button button--center-black" type="submit" data-hoverContent="Задать вопрос" data-content="Задать вопрос"></button>
		</form>
	</div>
</div>
<div id="successMessage" class="success-message" style="display: none;">
	<div class="success-message__inner">
		<div class="h1">Ваша заявка успешно отправлена!</div>
		<div class="success-message__subtitle">Наши менеджеры перезвонят вам в течение рабочего дня.</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>