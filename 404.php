<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("404");
$APPLICATION->SetDirProperty("page_class", "page-error");
$APPLICATION->SetDirProperty("page__wrap_class", "error");
CHTTP::SetStatus(404);
?>
<div class="error__title  large  wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".2s">ошибка 404</div>
<div class="error__subtitle  small wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">К сожалению данная страница недоступна</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>