<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании - строительная компания «Сиблидер»");
$APPLICATION->SetPageProperty("title", "О компании - строительная компания «Сиблидер»");
$APPLICATION->SetPageProperty("description", "&#127959; Строительная компания «Сиблидер» - одна из крупнейших строительных компаний Красноярска &#128313; Важнейшим приоритетом для себя ставим строительство качественного и комфортного жилья, которое вы оцените по достоинству.");
?>
<div class="page__top-slide page__top-slide--dark" id="topSlide">
	<div class="page__top-slide-content">
		<h1 class="big wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".2s">
			 сиблидер
		</h1>
		<div class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
			<div class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					array(
						"COMPONENT_TEMPLATE" => ".default",
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_TEMPLATE_PATH."/includes/pages/about/text.php",
						"EDIT_TEMPLATE" => ""
					),
					false
				);?>
			</div>
		</div>
	</div>
	<?include $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/includes/pages/about/svg.php"?>
</div>
<div class="page__main-content about" id="about">
	<?$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"about",
		Array(
			"IBLOCK_ID" => 2,
			"NEWS_COUNT" => 20,
			"PROPERTY_CODE" => ["L_TYPE","F_IMG"],
			"SET_TITLE" => "N"
		)
	);?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"about.standart",
		Array(
			"IBLOCK_ID" => 3,
			"NEWS_COUNT" => 200,
			"SET_TITLE" => "N"
		)
	);?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"about.awards",
		Array(
			"IBLOCK_ID" => 4,
			"NEWS_COUNT" => 200,
			"SET_TITLE" => "N"
		)
	);?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"about.vacancy",
		Array(
			"IBLOCK_ID" => 5,
			"NEWS_COUNT" => 200,
			"SET_TITLE" => "N"
		)
	);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>