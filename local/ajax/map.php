<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(isset($mapID)){
	global $arrMapFilter;
	$arrMapFilter["ID"] = $mapID;
}
$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"map.json",
	[
		"HID" => $arResult["ID"],
		"IBLOCK_ID" => IBLOCK_ID_HOUSE,
		"CACHE_TYPE" => "A",
		"NEWS_COUNT" => 99,
		"USE_FILTER" => "Y",
		"FILTER_NAME" => "arrMapFilter",
		"SET_TITLE" => "N",
		"PROPERTY_CODE" => [
			"S_COORD",
			"E_PROJECT"
		]
	],
	false
);