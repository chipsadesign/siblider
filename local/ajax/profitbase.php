<?
$_SERVER["DOCUMENT_ROOT"] = "/var/www/siblider.ru/data/www/siblider.ru";
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/**
 * Получаем проекты по API
 */
$arProjects = siteHandlers::getData("projects");
foreach($arProjects as $arProject){
	// Создаём/обновляем ЖК
	$projectAddResult = siteHandlers::addProject($arProject);
	if($projectAddResult["id"]){
		$arProject["ELEMENT_ID"] = $projectAddResult["id"];
		$arProject["RESULT"] = $projectAddResult;

		// Получаем дома для каждого ЖК
		$arHouses = siteHandlers::getData("projects/".$arProject["id"]."/houses");
		foreach($arHouses as &$arHouse){
			$arHouse["floors"] = 0;

			// Получаем квартиры для каждого дома
			$arFlats = siteHandlers::getData("projects/".$arProject["id"]."/houses/".$arHouse["id"]."/properties/list");
			$arEntrances = [];
			foreach($arFlats as $arFlat){
				if($arFlat["floor"] > $arHouse["floors"])
					$arHouse["floors"] = $arFlat["floor"];
				$arEntrances[] = $arFlat["section"];
			}
			$arEntrances = array_unique($arEntrances);
			$arHouse["entrances"] = $arEntrances;
			$arHouse["entrances_cnt"] = count($arEntrances);

			// Создаём/обновляем дома
			$houseAddResult = siteHandlers::addHouse($arHouse, $arProject["ELEMENT_ID"]);
			$arHouse["ELEMENT_ID"] = $houseAddResult["id"];
			$arHouse["RESULT"] = $houseAddResult;

			foreach($arFlats as &$arFlat){
				// Создаём/обновляем квартиры
				$flatAddResult = siteHandlers::addFlat($arFlat, $arHouse["ELEMENT_ID"]);
				$arFlat["RESULT"] = $flatAddResult;
				if(strlen($arFlat["preset_id"])){
					$arHouse["PRESET"] = "Y";
				}
			}
			$arHouse["FLATS"] = $arFlats;
		}
		$arProject["HOUSES"] = $arHouses;
		$arResult["PROJECTS"][$arProject["id"]] = $arProject;
	}
}