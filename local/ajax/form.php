<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$rq = $_REQUEST;
/**
 * Тут будем отправлять все формы
 * $rq["FILEDS"] - Поля, отправленные в форме
 * $rs["action"] - EVENT_NAME почтового события
 */
$arResult = [
	"success" => "",
	"message" => ""
];
if(check_bitrix_sessid() || true){
	if(isset($rq["action"])){
		$arFields = $rq["FIELDS"];
		if(isset($rq["function"])){
			if($rq["function"] == "addFaq"){
				CModule::IncludeModule("iblock");
				$el = new CIBlockElement;
				$arLoadProductArray = [
					"NAME" => $rq["FIELDS"]["NAME"],
					"PREVIEW_TEXT" => $rq["FIELDS"]["TEXT"],
					"IBLOCK_ID" => IBLOCK_ID_FAQ,
					"ACTIVE" => "N",
				];
				if($id = $el->Add($arLoadProductArray))
					$arFields["LINK"] = "http://".$_SERVER["HTTP_HOST"]."/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=6&type=content&ID=".$id;
				else{
					$arResult["success"] = "false";
					$arResult["message"] = $el->LAST_ERROR;
				}
			}
		}
		$arFiles = [];
		if(!empty($_FILES)){
			foreach($_FILES["FIELDS"]["tmp_name"] as $path){
				if (!empty($path)) {
					$arFiles[] = $path;
				}
			}
		}

		/**
		 * Добавляем email'ы менеджеров
		 */
		if(is_array($_REQUEST["managers"]) && !empty($_REQUEST["managers"])){

			CModule::IncludeModule("iblock");
			$rsManagers = CIBlockElement::GetList(
				[
					"SORT" => "ASC"
				],
				[
					"IBLOCK_ID" => IBLOCK_ID_MANAGER,
					"!PROPERTY_S_EMAIL" => false,
					"ID" => $_REQUEST["managers"]
				],
				false,
				false,
				[
					"ID",
					"PROPERTY_S_EMAIL"
				]
			);

			while($arManager = $rsManagers->Fetch()){
				$arManagerEmails[] = $arManager["PROPERTY_S_EMAIL_VALUE"];
			}
			$arFields["MANAGER_EMAILS"] = implode(", ", $arManagerEmails);
		}
		$eventId = CEvent::Send($rq["action"], SITE_ID, $arFields, "Y", "", $arFiles);
		if (intval($eventId > 0)) {
			include "form_amo.php";
		}
		$arResult["fields"] = $arFields;
		$arResult["files"] = $_FILES;
	}else{
		$arResult["success"] = "false";
		$arResult["message"] = "Ошибка отправки письма, обратитесь к администратору сайта.";
	}
}else{
	$arResult["success"] = "false";
	$arResult["message"] = "Сессия истекла, перезагрузите страницу.";
}

echo json_encode($arResult);