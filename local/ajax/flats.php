<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$rq = $_REQUEST;

$arResult = siteHandlers::getFlats($rq["house_id"], $rq);

// Список квартир в деталке дома
if ($rq["action"] == "object_detail") {
    ob_start(); ?>
        <?if (!empty($arResult["ITEMS"])):?>
            <?foreach ($arResult["ITEMS"] as $arItem):?>
                <?if ($arItem["PROPERTIES"]["S_STATUS"]["VALUE"] !== "AVAILABLE") {
                    continue;
                }?>
                <div class="table__item js-flat-preview" data-id="<?=$arItem["ID"]?>">
                    <div class="table__floor">№ <?=$arItem["NAME"]?></div>
                    <div class="table__square"><?=$arItem["PROPERTIES"]["N_FLOOR"]["VALUE"]?></div>
                    <div class="table__place"><span data-hoverContent="<?=$arItem["PROPERTIES"]["N_ROOMS"]["VALUE"]?>" data-content="<?=$arItem["PROPERTIES"]["N_ROOMS"]["VALUE"]?>"></span></div>
                    <div class="table__size"><?=$arItem["PROPERTIES"]["N_AREA"]["VALUE"]?> м<sup>2</sup></div>
                    <div class="table__price"><?=number_format($arItem["PROPERTIES"]["N_PRICE"]["VALUE"], 0, ".", " ")?> ₽</div>
                </div>
            <?endforeach; ?>
        <?endif; ?>
    <?php
    $result["flats"] = ob_get_contents();
    ob_end_clean();

    $minPrice = -1;
    $maxPrice = -1;
    foreach ($arResult["ITEMS"] as $arItem) {
        if ($arItem["PROPERTIES"]["S_STATUS"]["VALUE"] !== "AVAILABLE") {
            continue;
        }

        $price = intval($arItem["PROPERTIES"]["N_PRICE"]["VALUE"]);
        if ($price <= 0) {
            continue;
        }
        if ($minPrice == -1 || $minPrice > $price) {
            $minPrice = $price;
        }
        if ($maxPrice == -1 || $maxPrice < $price) {
            $maxPrice = $price;
        }
    }
    $result["price"] = [
        "min" => number_format($minPrice, 0, ".", " "),
        "max" => number_format($maxPrice, 0, ".", " "),
    ];
    $result["items"] = $arResult["ITEMS"];
    echo json_encode($result);
}

if ($rq["action"] == "flats") {
    $arHouseFilter = ["IBLOCK_ID" => IBLOCK_ID_HOUSE, "ACTIVE" => "Y"];
    if (isset($rq["house_id"]) && $rq["house_id"] > 0) {
        $arCoordResult = siteHandlers::getCoord($rq["house_id"]);
        $arHouseFilter["ID"] = $rq["house_id"];
    }

    $rsHouse = CIBlockElement::GetList([], $arHouseFilter, false, false, ["ID", "NAME"]);
    while ($arHouse = $rsHouse->Fetch()) {
        $arResult["HOUSE_NAMES"][$arHouse["ID"]] = $arHouse["NAME"];
    }

    if ((isset($arCoordResult["coord"]) && isset($rq["house_id"])) || (!isset($arCoordResult["coord"]) && !isset($rq["house_id"]))) {
        ob_start();
        include($rq["map"]);
        $result["map"] = ob_get_contents();
        ob_end_clean();
    } else {
        $result["map"] = false;
    }

    ob_start();
    include($rq["filter"]);
    $result["filter"] = ob_get_contents();
    ob_end_clean();

    $test = 1;
    ob_start();
    include($rq["list"]);
    $result["list"] = ob_get_contents();
    ob_end_clean();

    ob_start();
    include($rq["building"]);
    $result["building"] = ob_get_contents();
    ob_end_clean();

    $count = count($arResult["ITEMS"]);

    foreach ($arResult["ITEMS"] as $key => $arItem) {
        if (/* isset($_REQUEST["status_active"]) &&  */$arItem["PROPERTIES"]["S_STATUS"]["VALUE"] !== "AVAILABLE") {
            $count--;
            // unset($arResult["ITEMS"][$key]);
        }
    }
    $result["count"] = $count." ".siteHandlers::getInclinationByNumber($count, ["подходящих вариант", "подходящих варианта", "подходящих вариантов"]);

    $result["arResult"] = $arResult;
    echo json_encode($result);
}
