<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (CModule::IncludeModule("chipsa.amocrm")) {

    if($rq["action"] == "FLAT_BOOK") {
        $str_Action = "Бронирование квартиры";
    }elseif($rq["action"] == "IPOTEKA"){
        $str_Action = "Ипотека";
    }elseif($rq["action"] == "TRADE_IN"){
        $str_Action = "форма отправки заявки на рассчет стоимости квартиры для трейд-ин";
    }

    $rq["FIELDS"]["PHONE"] = str_replace(["(",")", "-", " "], "", $rq["FIELDS"]["PHONE"]);

    $arTask = [
        "element_id" => 0,
        "element_type" => 1,
        "complete_till" => strtotime("+5 minutes"),
        "task_type" => TASK_TYPE,
        "text" => $str_Action,
        "is_completed" => 1
    ];

    $arDataForm = [];
    $arDataForm[] = "Дата заявки: ".date("d-m-Y H:i");

    foreach([
        "NAME" => "ФИО",
        "PHONE" => "Телефон",
        "FLAT_NAME" => "Квартира",
        "AGE" => "Возраст",
        "TIME" => "Срок",
        "CREDIT" => "Сумма кредита",
        "CITY" => "Город",
        "STREET" => "Улица",
        "FLAT_TYPE" => "Тип дома",
        "ROOMS" => "Количество комнат",
        "AREA" => "Площадь",
        "FLOOR" => "Этаж",
        "MATERIAL" => "Материал дома",
        "CONDITION" => "Состояние ремонта",
        "PROJECT" => "Проект",
        "PAY" => "Способ доплаты",
    ] as $key => $value){
        if(!empty($rq["FIELDS"][$key])){
            $arDataForm[] = $value.": ".$rq["FIELDS"][$key];
        }
    }

    if (!empty($rq["page_title"]) && !empty($rq["page_url"])) {
        $arDataForm[] = "Заявка отправлена со страницы ".$rq["page_title"]." (".$rq["page_url"].")";
    }

    $arNote = [
        "element_id" => 0,
        "element_type" => 1, // контакт
        "note_type" => 4, // Тип примечания
        "text" => ""
    ];

    $arNote["text"] = implode(",".PHP_EOL, $arDataForm);

    $obAmoCrm = new Amocrm();

    if($obAmoCrm->isConnect){

        // 1 - ищем контакт по емайлу и телефону.
        $arContact = [
            "NAME" => $rq["FIELDS"]["NAME"],
            "PHONE" => $rq["FIELDS"]["PHONE"],
            "EMAIL" => $rq["FIELDS"]["EMAIL"],
        ];

        // Trade in
        foreach([
            "FLAT_TYPE" => "тип планировки",
            "CONDITION" => "состояние",
            "ROOMS" => "число комнат",
            "AREA" => "площадь",
            "FLOOR" => "этаж",
            "MATERIAL" => "материал дома",
        ] as $key => $crmKey){
            if(!empty($rq["FIELDS"][$key])){
                $arContact[$crmKey] = $rq["FIELDS"][$key];
            }
        }
        $arContact["адрес"] = [];
        foreach([
            "CITY" => "город",
            "STREET" => "адрес"
        ] as $key => $crmKey){
            if(!empty($rq["FIELDS"][$key])){
                $arContact["адрес"][$crmKey] = $rq["FIELDS"][$key];
            }
        }
        if(empty($arContact["адрес"])){
            unset($arContact["адрес"]);
        }

        $arFilter = [];
        foreach (["EMAIL", "PHONE"] as $strField) {
            if (!empty($arContact[$strField])) {
                $arFilter[] = $arContact[$strField];
            }
        }
        if(!empty($arFilter)){
            $arContactSearchResult = $obAmoCrm->findContact($arFilter);
        }else{
            $arContactSearchResult = false;
        }
        if($arContactSearchResult){
            // Если в контакт найден, запоминиаем его id, В будующую сделку добавляем тег "Дубль"
            $contactID = array_shift($arContactSearchResult)["id"];
        }else{
            // Если в контакт не найден, запоминиаем его id, привязываем его к компании с которой определились ранее
            $contactID = $obAmoCrm->addContact($arContact);
        }

        if (intval($contactID) > 0) {
            // 2 Создаем задачу
            $arTask["element_id"] = $contactID;
            $taskID = $obAmoCrm->addTask($arTask);

            // 3 Создаем примечание
            $arNote["element_id"] = $contactID;
            $noteID = $obAmoCrm->addNote($arNote);
        }
    }
}