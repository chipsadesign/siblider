<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
CModule::IncludeModule("iblock");
CModule::IncludeModule('highloadblock');

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

//получить иб по коду таблицы
$hldata = Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter'=>array('TABLE_NAME'=>'socnet')))->fetch();

//получить иб по ид
$hl_iblock_id = 1;
$hlblock = HL\HighloadBlockTable::getById($hl_iblock_id)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

//get data
$rsData = $entity_data_class::getList(array(
    "select" => array("*"),
    "order" => ["UF_SORT" => "ASC"],
    "filter" => array("!UF_LINK" => false, "!UF_IMG" => false)
));
while($arData = $rsData->Fetch()){
	$arData["UF_IMG"] = CFile::GetPath($arData["UF_IMG"]);
    $arResult["ITEMS"][] = $arData;
}

$this->includeComponentTemplate();
?>