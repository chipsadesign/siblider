<?
$_SERVER["DOCUMENT_ROOT"] = "/var/www/siblider.ru/data/www/siblider.ru";
// $_SERVER["DOCUMENT_ROOT"] = "/Users/kozlovski/Sites/siblider";
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

addLog("Update profitbase [".date("d.m.y H:i:s")."] ---- Start ");

$arProjects = ProfitbaseProject::get();

foreach($arProjects as $arProject){
	$projectID = ProfitbaseProject::add($arProject);

	if($projectID){
		$arHouses = ProfitbaseHouse::get($arProject["id"]);

		foreach($arHouses as &$arHouse){
			$arHouse["floors"] = 0;

			$arFlats = ProfitbaseFlat::get($arProject["id"], $arHouse["id"]);

			$arEntrances = [];
			foreach($arFlats as $arFlat){
				if($arFlat["floor"] > $arHouse["floors"])
					$arHouse["floors"] = $arFlat["floor"];
				$arEntrances[] = $arFlat["section"];
			}
			$arEntrances = array_unique($arEntrances);
			$arHouse["entrances"] = $arEntrances;
			$arHouse["entrances_cnt"] = count($arEntrances);

			$houseID = ProfitbaseHouse::add($arHouse, $projectID);

			foreach($arFlats as &$arFlat){
				ProfitbaseFlat::add($arFlat, $houseID);
			}

			$arFloors = ProfitbaseFloor::get($arHouse["id"]);

			$arFloorsModified = [];
			foreach($arFloors as &$arFloor){
				$arFloorsModified[$arFloor["id"]]["id"] = $arFloor["id"];
				$arFloorsModified[$arFloor["id"]]["image"] = $arFloor["images"]["source"];
				$arFloorsModified[$arFloor["id"]]["floor"][] = $arFloor["number"];
			}
			unset($arFloor);

			foreach($arFloorsModified as $arFloor){
				ProfitbaseFloor::add($arFloor, $houseID);
			}
		}
	}
}

addLog("Update profitbase [".date("d.m.y H:i:s")."] ---- Stop ");