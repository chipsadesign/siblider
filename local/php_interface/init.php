<?php
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/local/php_interface/log.txt");
include_once $_SERVER["DOCUMENT_ROOT"]."/local/php_interface/debug.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/local/php_interface/const.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/local/php_interface/handlers/handlers.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/local/php_interface/handlers/seo.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/local/php_interface/handlers/siblider.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/local/php_interface/classes/profitbase.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/local/php_interface/classes/profitbaseProject.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/local/php_interface/classes/profitbaseHouse.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/local/php_interface/classes/profitbaseFloor.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/local/php_interface/classes/profitbaseFlat.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/changelog.php";

/**
 * Редиректы
 */
AddEventHandler("main", "OnBeforeProlog", "MyOnBeforePrologHandler");
function MyOnBeforePrologHandler()
{
    $arRelink = [];
    include 'redirects.php';

    if (array_key_exists($_SERVER["REQUEST_URI"], $arRelink)) {
        LocalRedirect($arRelink[$_SERVER["REQUEST_URI"]], false, "301 Moved permanently");
    }
}

AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "HouseUpdate");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "HouseUpdate");
function HouseUpdate(&$arFields){
    if($arFields["IBLOCK_ID"] == IBLOCK_ID_HOUSE){
        if($arFields["PROPERTY_VALUES"][92][0]["VALUE"]){ // Если проставляется активным этапом
            foreach($arFields["PROPERTY_VALUES"][29] as $arVal){
                if(!is_array($arVal)){
                    $objectID = $arVal; // ID жк
                }elseif($arVal["VALUE"]){
                    $objectID = $arVal["VALUE"]; // ID жк
                }
            }

            if($objectID){
                $rsElement = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_HOUSE, "PROPERTY_E_PROJECT" => $objectID, "!PROPERTY_L_ACTIVE" => false], false, false, ["ID"]);
                while($arElement = $rsElement->Fetch()){
                    CIBlockElement::SetPropertyValuesEx($arElement["ID"], IBLOCK_ID_HOUSE, array("L_ACTIVE" => false));
                }
            }
        }
    }
}