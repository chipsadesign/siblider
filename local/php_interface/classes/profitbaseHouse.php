<?
Class ProfitbaseHouse extends Profitbase {

	/**
	 * Получаем дома ЖК из profitbase
	 *
	 * @param integer $projectID
	 * @return void
	 */
	public function get(int $projectID){
		$options = self::$curlOptions;
		$options[CURLOPT_URL] = self::$url."/projects/".$projectID."/houses?access_token=".self::getAccessToken();

		return self::sendRequest($options);
	}

	/**
	 * Проверяем, есть ли дом. Если нет - создаём.
	 *
	 * @param array $arHouse
	 * @param integer $projectID
	 * @return int
	 */
	public function add(array $arHouse, int $projectID){
		$houseID = 0;
		if($arHouse["id"] && $projectID){
			$arPropVals = [
                "ID_PROFITBASE" => $arHouse["id"],
                "E_PROJECT" => $projectID,
                "S_ADRESS" => trim($arHouse["street"]." ".$arHouse["number"]),
                "N_FLOORS" => $arHouse["floors"],
                "N_ENTRANCE" => $arHouse["entrances_cnt"],
                "S_TYPE" => $arHouse["type"],
			];

			$arStatus = [
                "UNFINISHED" => 13,
                "BUILT" => 14,
                "HAND-OVER" => 15,
			];

            if(!empty($arHouse["development_start_quarter"])){
                $arPropVals["S_DATE_START"] = $arHouse["development_start_quarter"]["quarter"]."й квартал ".$arHouse["development_start_quarter"]["year"];
			}

            if(!empty($arHouse["development_end_quarter"])){
                $arPropVals["S_DATE_END"] = $arHouse["development_end_quarter"]["quarter"]."й квартал ".$arHouse["development_end_quarter"]["year"];
			}

            if($arHouse["building_state"]){
                $arPropVals["L_STATUS"] = $arStatus[$arHouse["building_state"]];
			}

            if($arHouse["material"]){
				$arPropVals["L_MATERIAL"] = self::getMaterial($arHouse["material"]);
			}

            if($arHouse["facing"]){
                $arPropVals["S_FACING"] = $arHouse["facing"];
            }

			$arFields = [
                "IBLOCK_ID" => IBLOCK_ID_HOUSE,
                "NAME" => $arHouse["title"],
                "CODE" => Cutil::translit($arHouse["title"], "ru", ["replace_space" => "-", "replace_other" => "-"]),
                "ACTIVE" => "Y",
                "PROPERTY_VALUES" => $arPropVals
            ];

			CModule::IncludeModule("iblock");
            $el = new CIBlockElement;
            $rsElement = CIBlockElement::GetList(
				[],
				[
					"IBLOCK_ID" => IBLOCK_ID_HOUSE,
					"PROPERTY_ID_PROFITBASE" => $arHouse["id"]
				],
				false,
				false,
				["ID"]
			);
            if($arElement = $rsElement->Fetch()){
				// Такой дом есть, обновляем
				$arFields = [];
                $arPropVals = $arFields["PROPERTY_VALUES"];
				unset($arFields["ACTIVE"]);
                unset($arFields["PROPERTY_VALUES"]);
                if($el->Update($arElement["ID"], $arFields)){
                    foreach($arPropVals as $code => $val){
                        CIBlockElement::SetPropertyValuesEx($arElement["ID"], IBLOCK_ID_HOUSE, array($code => $val));
					}
				}
				$houseID = $arElement["ID"];
			}else{
				// Создаём новый
				$houseID = $el->Add($arFields);
			}
		}

		return $houseID;
	}

	/**
     * Получаем ID значения свойства материал
     * Если такого нет - создаём
	 *
	 * @param string $material
	 * @return int
     */
    protected function getMaterial($material){
        CModule::IncludeModule("iblock");
        $rsMaterial = CIBlockPropertyEnum::GetList(
			[],
			[
				"VALUE" => $material,
				"IBLOCK_ID" => IBLOCK_ID_HOUSE,
				"CODE" => "L_MATERIAL"
			]
		);
        if($arMaterial = $rsMaterial->Fetch()){
            $materialID = $arMaterial["ID"];
        }else{
            $enum = new CIBlockPropertyEnum;
            $materialID = $enum->Add(["PROPERTY_ID" => 30, "VALUE" => $material]);
        }

        return $materialID;
    }
}