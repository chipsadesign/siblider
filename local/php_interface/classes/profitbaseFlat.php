<?
Class ProfitbaseFlat extends Profitbase {

	/**
	 * Получаем квартиры дома из profitbase
	 *
	 * @param integer $projectID
	 * @param integer $houseID
	 * @return array
	 */
	public function get(int $projectID, int $houseID){
		$options = self::$curlOptions;
		$options[CURLOPT_URL] = self::$url."/projects/".$projectID."/houses/".$houseID."/properties/list?access_token=".self::getAccessToken();

		return self::sendRequest($options);
	}

	/**
	 * Проверяем, есть ли квартира. Если нет создаём
	 *
	 * @param array $arFlat
	 * @param integer $houseID
	 * @return int
	 */
	public function add(array $arFlat, int $houseID){
		$flatID = 0;
		if($arFlat["id"] && $houseID){
            $arPropVals = [
                "ID_PROFITBASE" => $arFlat["id"],
                "E_HOUSE" => $houseID,
                "N_FLOOR" => $arFlat["floor"],
                "N_ROOMS" => $arFlat["rooms_amount"],
                "N_AREA" => $arFlat["area"]["area_total"],
                "N_PRICE" => $arFlat["price"]["value"],
                "S_ENTRANCE" => $arFlat["section"],
                "S_STATUS" => $arFlat["status"],
                "N_PRESET_CODE" => $arFlat["attributes"]["code"],
			];

            if($arFlat["preset_id"]){
                $arPropVals["F_PRESET"] = CFile::MakeFileArray($arFlat["preset_id"]);
            }

            $arFields = [
                "IBLOCK_ID" => IBLOCK_ID_FLAT,
                "NAME" => $arFlat["number"],
                "CODE" => $houseID."-".Cutil::translit($arFlat["number"], "ru", ["replace_space" => "-", "replace_other" => "-"]),
                "ACTIVE" => "Y",
                "PROPERTY_VALUES" => $arPropVals
            ];

            CModule::IncludeModule("iblock");
            $el = new CIBlockElement;
            $rsElement = CIBlockElement::GetList(
				[],
				[
					"IBLOCK_ID" => IBLOCK_ID_FLAT,
					"PROPERTY_ID_PROFITBASE" => $arFlat["id"]
				],
				false,
				false,
				["ID"]
			);
            if($arElement = $rsElement->Fetch()){
                // Обновляем квартиру
                unset($arFields["PROPERTY_VALUES"]);
                if($el->Update($arElement["ID"], $arFields)){
					$flatID = $arElement["ID"];

                    foreach($arPropVals as $code => $val){
						if(
							$code == "F_PRESET"
							&& md5_file($_SERVER["DOCUMENT_ROOT"].CFile::GetPath($arElement["PROPERTY_F_PRESET_VALUE"])) == md5_file($arFlat["preset_id"]) // Сравниваем планировки. Если файл тот же - пропускаем
							&& filesize($_SERVER["DOCUMENT_ROOT"].CFile::GetPath($arElement["PROPERTY_F_PRESET_VALUE"])) > 0
						)
							continue;

                        CIBlockElement::SetPropertyValuesEx($arElement["ID"], IBLOCK_ID_FLAT, array($code => $val));
                    }
                }
            }else{
                // Создаём новую
                $flatID = $el->Add($arFields);
            }
        }
        return $flatID;
	}
}