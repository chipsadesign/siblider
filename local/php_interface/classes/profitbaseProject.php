<?
Class ProfitbaseProject extends Profitbase {

	/**
	 * Получаем ЖК из Profitbase
	 *
	 * @return array
	 */
	public function get(){
		$options = self::$curlOptions;
		$options[CURLOPT_URL] = self::$url."/projects?access_token=".self::getAccessToken();

		return self::sendRequest($options);
	}

	/**
	 * Проверяем, есть ли такой ЖК. Если нет - создаём 
	 *
	 * @param array $arProject
	 * @return int
	 */
	public function add(array $arProject){
		$projectID = 0;
		if($arProject["id"]){
            CModule::IncludeModule("iblock");
            $el = new CIBlockElement;
            $rsElement = CIBlockElement::GetList(
				[],
				[
					"IBLOCK_ID" => IBLOCK_ID_OBJECT,
					"PROPERTY_ID_PROFITBASE" => $arProject["id"]
				],
				false,
				false,
				["ID"]
			);
            if($arElement = $rsElement->Fetch()){
                // Такой ЖК есть, возвращаем его ID
				$projectID = $arElement["ID"];
            }else{
                // Создаём новый
                $arFields = [
                    "IBLOCK_ID" => IBLOCK_ID_OBJECT,
                    "NAME" => $arProject["title"],
                    "CODE" => Cutil::translit($arProject["title"], "ru", ["replace_space" => "-", "replace_other" => "-"]),
                    "ACTIVE" => "Y",
                    "PROPERTY_VALUES" => [
                        "ID_PROFITBASE" => $arProject["id"]
                    ]
				];
				$projectID = $el->Add($arFields);
            }
		}

        return $projectID;
	}
}