<?
Class Profitbase {
	protected static $url = "https://pb818.profitbase.ru/api/v4/json";
	protected static $api = "app-5c73bb751f3b4";
	protected static $curlOptions = [
		CURLOPT_POST => false,
		CURLOPT_RETURNTRANSFER => true,
	];
	protected static $access_token = "";

	/**
	 * Получение access_token в Profitbase
	 *
	 * @return array
	 */
	protected function auth(){
		$options = self::$curlOptions;
		$data = [
			"type" => "api-app",
			"credentials" => [
			  "pb_api_key" => self::$api
			]
		];
		$get = http_build_query($data);
		$options[CURLOPT_URL] = self::$url."/authentication?".$get;
		$options[CURLOPT_POST] = true;

		$result = self::sendRequest($options);

		if($result["access_token"])
			self::$access_token = $result["access_token"];
	}

	/**
	 * Проверяем access_token
	 *
	 * @return void
	 */
	protected function getAccessToken(){
		if(!self::$access_token)
			self::auth();

		return self::$access_token;
	}

	/**
	 * Посылаем cURL запрос
	 *
	 * @param array $options
	 * @return array
	 */
	protected function sendRequest(array $options){
		$ch = curl_init();
		curl_setopt_array($ch, $options);
		$curlResult = curl_exec($ch);

		if($curlResult === false){
			$result = curl_error($ch);
		}else{
			$result = json_decode($curlResult, true);
		}

		curl_close($ch);

		return $result;
	}
}