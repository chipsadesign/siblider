<?
Class ProfitbaseFloor extends Profitbase {

	/**
	 * Получаем планировки этажей дома из profitbase
	 *
	 * @return array
	 */
	public function get(int $houseID){
		$options = self::$curlOptions;
		$options[CURLOPT_URL] = self::$url."/floor"."?houseId=".$houseID."&access_token=".self::getAccessToken();

		return self::sendRequest($options);
	}

	/**
	 * Добавляем этаж
	 *
	 * @param array $arFloor
	 * @param integer $houseID
	 * @return int
	 */
	public function add(array $arFloor, int $houseID){
		$floorID = 0;
		if($houseID && $arFloor["id"]){
			CModule::IncludeModule("iblock");
			$el = new CIBlockElement;
			$propertyValues = [
				"ID_PROFITBASE" => $arFloor["id"],
				"E_HOUSE" => $houseID,
				"S_FLOOR" => $arFloor["floor"],
			];
			$arFields = [
				"NAME" => "Этажи ".$houseID,
				"IBLOCK_ID" => IBLOCK_ID_FLOOR,
				"PROPERTY_VALUES" => $propertyValues,
				"PREVIEW_PICTURE" => CFile::MakeFileArray($arFloor["image"])
			];
			$rsElement = CIBlockElement::GetList(
				[],
				[
					"IBLOCK_ID" => IBLOCK_ID_FLOOR,
					"PROPERTY_ID_PROFITBASE" => $arFloor["id"]
				],
				false,
				false,
				["ID", "PREVIEW_PICTURE"]
			);
			if($arElement = $rsElement->Fetch()){
				$floorID = $arElement["ID"];
				if(
					md5_file($_SERVER["DOCUMENT_ROOT"].CFile::GetPath($arElement["PREVIEW_PICTURE"])) == md5_file($arFloor["image"]) // Сравниваем планировки. Если файл тот же - пропускаем
				)
					unset($arFields["PREVIEW_PICTURE"]);
				unset($arFields["PROPERTY_VALUES"]);
                if($el->Update($arElement["ID"], $arFields)){
					$floorID = $arElement["ID"];

                    foreach($propertyValues as $code => $val){
                        CIBlockElement::SetPropertyValuesEx($arElement["ID"], IBLOCK_ID_FLOOR, array($code => $val));
                    }
                }
			}else{
				$floorID = $el->Add($arFields);
			}
		}

		return $floorID;
	}
}