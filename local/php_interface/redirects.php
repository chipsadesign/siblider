<?
$arRelink = [
	"/objects" => "/catalog/",
	"/objects/42" => "/catalog/novyy-klenovyy-1-etap/",
	"/objects/43" => "/catalog/na-vysote-1-etap/",
	"/objects/41" => "/catalog/khoroshee-mesto/",
	"/objects/44" => "/catalog/na-vysote-2-etap/",
	"/objects/39" => "/catalog/dinastiya-i-etap/",
	"/objects/40" => "/catalog/svoi-lyudi/",
	"/objects/36" => "/catalog/zhk-vavilovskiy-14-2-etap/",
	"/objects/34" => "/catalog/per-yakornyy-12/",
	"/objects/37" => "/catalog/sudostroitelnaya-35/",
	"/objects/23" => "/catalog/fregat-neo-ii-ochered/",
	"/objects/35" => "/catalog/sudostroitelnaya-37/",
	"/objects/33" => "/catalog/vavilova-47-v/",
	"/objects/32" => "/catalog/shelkovaya-4-a/",
	"/objects/26" => "/catalog/vavilova-27-a/",
	"/objects/6" => "/catalog/vavilova-37-d/",
	"/objects/8" => "/catalog/medicinsky-14d/",
	"/objects/9" => "/catalog/semafornaya-335a/",
	"/objects/11" => "/catalog/sudostroitelnaya-20/",
	"/objects/24" => "/catalog/semafornaya-311/",
	"/objects/10" => "/catalog/vavilova-37-d/",
	"/objects/31" => "/catalog/kutuzova-36/",
	"/objects/30" => "/catalog/kutuzova-34-1-4/",
	"/objects/29" => "/catalog/per-mayakovskogo-18-3/",
	"/objects/28" => "/catalog/per-mayakovskogo-18-2/",
	"/objects/22" => "/catalog/fregat-neo-ii-ochered/",
	"/objects/21" => "/catalog/yakorniy-17-a/",
	"/objects/20" => "/catalog/semafornaya-293/",
	"/objects/19" => "/catalog/Semafornaya-287/",
	"/objects/18" => "/catalog/Ulyanovky-8a/",
	"/objects/17" => "/catalog/sudostroitelnaya-26-a-1/",
	"/objects/16" => "/catalog/Semafornaya-17a/",
	"/objects/15" => "/catalog/Kras-rab-165g/",
	"/objects/14" => "/catalog/semafornaya-17-g/",
	"/objects/13" => "/catalog/ylyanovsky-14-g/",
	"/objects/12" => "/catalog/sudostroitelnaya-26-a-1/",
	"/spetspredlozhenie" => "/special-offers/",
	"/company/" => "/about/",
	"/commercial" => "/business/",
	"/contact" => "/contacts/",
	"/contact/vashi-menedzhery-" => "/contacts/",
	"/objects-log" => "/journal/",

	// ---
	"/calculator/" => "/calculator-ipoteka/",
	"/calculator/pjsc-vtb-4/" => "/calculator-ipoteka/pao-vtb-for-family/",
	"/calculator/sberbank3/" => "/calculator-ipoteka/sberbank-for-family/",
	"/calculator/alfa-bank/" => "/calculator-ipoteka/alfa-bank-circs/",
	"/calculator/alfa-bank2/" => "/calculator-ipoteka/alfa-bank-two-circs/",
	"/calculator/pjsc-vtb/" => "/calculator-ipoteka/pao-vtb-circs/",
	"/calculator/opening/" => "/calculator-ipoteka/opening-bank-circs/",
	"/calculator/gazprombank-jsc/" => "/calculator-ipoteka/gazprombank-circs/",
	"/calculator/pjsc-vtb-3/" => "/calculator-ipoteka/pao-vtb-two-documents/",
	"/calculator/sberbank1/" => "/calculator-ipoteka/sberbank- constructed/",
	"/calculator/deltacredit/" => "/calculator-ipoteka/deltacredit-circs/",
	"/calculator/sberbank/" => "/calculator-ipoteka/sberbank-military/",
	// ---

	"/business/filter" => "/business/",
	"/business/filter/" => "/business/",

	"/catalog/podzemnaya-avtostoyanka/parking/" => "/catalog/parking-vavilovskiy-dvorik/parking/",
	"/catalog/parking-14/parking/" => "/catalog/parking-vavilovskiy-14-2-etap/parking/"
];