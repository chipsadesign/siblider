<?

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "iblock",
    "OnTemplateGetFunctionClass",
    array("ProjectTypeCustom", "eventHandler")
);

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "iblock",
    "OnTemplateGetFunctionClass",
    array("ProjectTypeCustomDe", "eventHandler")
);

include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/lib/template/functions/fabric.php");
class ProjectTypeCustom extends \Bitrix\Iblock\Template\Functions\FunctionBase
{
    //Обработчик события на вход получает имя требуемой функции
    //парсер её нашел в строке SEO
    public static function eventHandler($event)
    {
        $parameters = $event->getParameters();
        $functionName = $parameters[0];
        if ($functionName === "ob_type" )
        {
            //обработчик должен вернуть SUCCESS и имя класса
            //который будет отвечать за вычисления
            return new \Bitrix\Main\EventResult(
                \Bitrix\Main\EventResult::SUCCESS,
                "\\ProjectTypeCustom"
            );
        }
    }
    //собственно функция выполняющая "магию"
    public function calculate($parameters)
    {
        $result = $this->parametersToArray($parameters);
        $strResult = "";
        if($result[0] == "Y") {
            $strResult = "Помещение";
        }else{
            if ($result[0] > 0) {
                $strResult = $result[0]."-комнатная квартира";
            }
        }
        return $strResult;
    }
}

class ProjectTypeCustomDe extends \Bitrix\Iblock\Template\Functions\FunctionBase
{
    //Обработчик события на вход получает имя требуемой функции
    //парсер её нашел в строке SEO
    public static function eventHandler($event)
    {
        $parameters = $event->getParameters();
        $functionName = $parameters[0];
        if ($functionName === "ob_type_de" )
        {
            //обработчик должен вернуть SUCCESS и имя класса
            //который будет отвечать за вычисления
            return new \Bitrix\Main\EventResult(
                \Bitrix\Main\EventResult::SUCCESS,
                "\\ProjectTypeCustomDe"
            );
        }
    }
    //собственно функция выполняющая "магию"
    public function calculate($parameters)
    {
        $result = $this->parametersToArray($parameters);
        $strResult = "";
        if($result[0] == "Y") {
            $strResult = "помещение для бизнеса";
        }else{
            if ($result[0] > 0) {
                $strResult = $result[0]."-комнатную квартиру";
            }
        }
        return $strResult;
    }
}