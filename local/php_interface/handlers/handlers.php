<?
CONST BASE_URL = "https://pb818.profitbase.ru/api/v3/json/";
CONST API_KEY = "app-5c73bb751f3b4";

function addLog($field){
    AddMessage2Log(print_r($field, true));
}

class siteHandlers {
    /**
     * Функция для склонения слов
     * @param  [type] $number [число]
     * @param  [type] $arr    [массив слов]
     * @return [type]         [слово]
     */
    public function getInclinationByNumber($number, $arr = []){
        $number = (string) $number;
        $numberEnd = substr($number, -2);
        $numberEnd2 = 0;
        if(strlen($numberEnd) == 2){
            $numberEnd2 = $numberEnd[0];
            $numberEnd = $numberEnd[1];
        }
        if ($numberEnd2 == 1) return $arr[2];
        else if ($numberEnd == 1) return $arr[0];
        else if ($numberEnd > 1 && $numberEnd < 5)return $arr[1];
        else return $arr[2];
    }

 	/**
     * get запрос до API Profitbase
     * Возвращает json
     */
    public function getData($url, $data = []){
        $url = BASE_URL.$url;
        $data["access_token"] = API_KEY;
        if(!empty($data)){
            $data = http_build_query($data);
            $url .= "?".$data;
        }
        $result = file_get_contents($url);
        return json_decode($result, true);
    }

    /**
     * Получаем ID значения свойства материал
     * Если такого нет - создаём
     */
    public function getMaterial($material){
        CModule::IncludeModule("iblock");
        $rsMaterial = CIBlockPropertyEnum::GetList([], ["VALUE" => $material, "IBLOCK_ID" => IBLOCK_ID_HOUSE, "CODE" => "L_MATERIAL"]);
        if($arMaterial = $rsMaterial->Fetch()){
            $arResult["id"] = $arMaterial["ID"];
        }else{
            $enum = new CIBlockPropertyEnum;
            $arResult["id"] = $enum->Add(["PROPERTY_ID" => 30, "VALUE" => $material]);
        }

        return $arResult;
    }

    /**
     * Создаём ЖК
     */
    public function addProject($arProject){
        if($arProject["id"] > 0){
            $projectID = 0;
            CModule::IncludeModule("iblock");
            $el = new CIBlockElement;
            $rsElement = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_OBJECT, "PROPERTY_ID_PROFITBASE" => $arProject["id"]], false, false, ["ID"]);
            if($arElement = $rsElement->Fetch()){
                // Такой ЖК есть, возвращаем его ID
                $arResult = [
                    "result" => "success",
                    "id" => $arElement["ID"]
                ];
            }else{
                // Создаём новый
                $arFields = [
                    "IBLOCK_ID" => IBLOCK_ID_OBJECT,
                    "NAME" => $arProject["title"],
                    "CODE" => Cutil::translit($arProject["title"], "ru", ["replace_space" => "-", "replace_other" => "-"]),
                    "ACTIVE" => "Y",
                    "PROPERTY_VALUES" => [
                        "ID_PROFITBASE" => $arProject["id"]
                    ]
                ];
                if($projectID = $el->Add($arFields)){
                    $arResult = [
                        "result" => "success Add",
                        "id" => $projectID
                    ];
                }else{
                    $arResult = [
                        "result" => "error Add",
                        "message" => $el->LAST_ERROR
                    ];
                }
            }
        }else{
            $arResult = [
                "result" => "error",
                "message" => "Invalid project ID"
            ];
        }
        return $arResult;
    }

    /**
     * Создаём Дом
     */
    public function addHouse($arHouse, $projectID){
        if($arHouse["id"] > 0 && $projectID > 0){
            $arStatus = [
                "UNFINISHED" => 13,
                "BUILT" => 14,
                "HAND-OVER" => 15,
            ];
            $arPropVals = [
                "ID_PROFITBASE" => $arHouse["id"],
                "E_PROJECT" => $projectID,
                "S_ADRESS" => trim($arHouse["street"]." ".$arHouse["number"]),
                "N_FLOORS" => $arHouse["floors"],
                "N_ENTRANCE" => $arHouse["entrances_cnt"],
                "S_TYPE" => $arHouse["type"],
            ];
            if(!empty($arHouse["development_start_quarter"])){
                $arPropVals["S_DATE_START"] = $arHouse["development_start_quarter"]["quarter"]."й квартал ".$arHouse["development_start_quarter"]["year"];
            }
            if(!empty($arHouse["development_end_quarter"])){
                $arPropVals["S_DATE_END"] = $arHouse["development_end_quarter"]["quarter"]."й квартал ".$arHouse["development_end_quarter"]["year"];
            }
            if($arHouse["building_state"]){
                $arPropVals["L_STATUS"] = $arStatus[$arHouse["building_state"]];
            }
            if($arHouse["material"]){
                $arMaterialResult = self::getMaterial($arHouse["material"]);
                if($arMaterialResult["id"]){
                    $arPropVals["L_MATERIAL"] = $arMaterialResult["id"];
                }
            }
            if($arHouse["facing"]){
                $arPropVals["S_FACING"] = $arHouse["facing"];
            }

            $arFields = [
                "IBLOCK_ID" => IBLOCK_ID_HOUSE,
                "NAME" => $arHouse["title"],
                "CODE" => Cutil::translit($arHouse["title"], "ru", ["replace_space" => "-", "replace_other" => "-"]),
                "ACTIVE" => "Y",
                "PROPERTY_VALUES" => $arPropVals
            ];

            CModule::IncludeModule("iblock");
            $el = new CIBlockElement;
            $rsElement = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_HOUSE, "PROPERTY_ID_PROFITBASE" => $arHouse["id"]], false, false, ["ID"]);
            if($arElement = $rsElement->Fetch()){
                unset($arFields["ACTIVE"]);
                // Такой дом есть, обновляем
                $arPropVals = $arFields["PROPERTY_VALUES"];
                unset($arFields["PROPERTY_VALUES"]);
                if($el->Update($arElement["ID"], $arFields)){
                    $arResult = [
                        "result" => "success",
                        "id" => $arElement["ID"]
                    ];
                    foreach($arPropVals as $code => $val){
                        CIBlockElement::SetPropertyValuesEx($arElement["ID"], IBLOCK_ID_HOUSE, array($code => $val));
                    }
                }else{
                    $arResult = [
                        "result" => "error",
                        "message" => $el->LAST_ERROR
                    ];
                }
            }else{
                // Создаём новый
                if($houseID = $el->Add($arFields)){
                    $arResult = [
                        "result" => "success",
                        "id" => $houseID
                    ];
                }else{
                    $arResult = [
                        "result" => "error",
                        "message" => $el->LAST_ERROR
                    ];
                }
            }
        }else{
            $arResult = [
                "result" => "error",
                "message" => "Invalid project ID"
            ];
        }
        return $arResult;
    }

    /**
     * Добавляем/Обновляем квартиру
     */
    public function addFlat($arFlat, $houseID){
        if($arFlat["id"] > 0 && $houseID > 0){
            $arPropVals = [
                "ID_PROFITBASE" => $arFlat["id"],
                "E_HOUSE" => $houseID,
                "N_FLOOR" => $arFlat["floor"],
                "N_ROOMS" => $arFlat["rooms_amount"],
                "N_AREA" => $arFlat["area"]["area_total"],
                "N_PRICE" => $arFlat["price"]["value"],
                "S_ENTRANCE" => $arFlat["section"],
                "S_STATUS" => $arFlat["status"],
                "N_PRESET_CODE" => $arFlat["attributes"]["code"],
            ];
            if($arFlat["preset_id"]){
                $arPropVals["F_PRESET"] = CFile::MakeFileArray($arFlat["preset_id"]);
            }

            $arFields = [
                "IBLOCK_ID" => IBLOCK_ID_FLAT,
                "NAME" => $arFlat["number"],
                "CODE" => $houseID."-".Cutil::translit($arFlat["number"], "ru", ["replace_space" => "-", "replace_other" => "-"]),
                "ACTIVE" => "Y",
                "PROPERTY_VALUES" => $arPropVals
            ];

            CModule::IncludeModule("iblock");
            $el = new CIBlockElement;
            $rsElement = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_FLAT, "PROPERTY_ID_PROFITBASE" => $arFlat["id"]], false, false, ["ID"]);
            if($arElement = $rsElement->Fetch()){
                // Обновляем квартиру
                unset($arFields["PROPERTY_VALUES"]);
                if($el->Update($arElement["ID"], $arFields)){
                    $arResult = [
                        "result" => "success Update",
                        "id" => $arElement["ID"],
                        "update_fields" => $arFields
                    ];

                    foreach($arPropVals as $code => $val){
                        CIBlockElement::SetPropertyValuesEx($arElement["ID"], IBLOCK_ID_FLAT, array($code => $val));
                    }
                }else{
                    $arResult = [
                        "result" => "error Update",
                        "message" => $el->LAST_ERROR
                    ];
                }
            }else{
                // Создаём новую
                if($flatID = $el->Add($arFields)){
                    $arResult = [
                        "result" => "success Add",
                        "id" => $flatID
                    ];
                }else{
                    $arResult = [
                        "result" => "error Add",
                        "message" => $el->LAST_ERROR
                    ];
                }
            }
        }else{
            $arResult = [
                "result" => "error",
                "message" => "Invalid house ID"
            ];
        }
        return $arResult;
    }

    /**
     * Получаем координаты выбранного дома
     * $houseID - ID дома в битриксе
     */
    public function getCoord($houseID = false){
        if($houseID){
            $arResult = [
                "result" => "error",
            ];
            CModule::IncludeModule("iblock");
            $rsHouse = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_HOUSE, "ID" => $houseID], false, false, ["ID", "PROPERTY_S_COORD"]);
            if($arHouse = $rsHouse->Fetch()){
                if($arHouse["PROPERTY_S_COORD_VALUE"]){
                    $arResult = [
                        "result" => "success",
                        "coord" => $arHouse["PROPERTY_S_COORD_VALUE"],
                    ];
                }
            }
        }else{
            $arResult = [
                "result" => "error",
                "coord" => "House not found",
            ];
        }

        return $arResult;
    }

    /**
     * Получаем квартиры выбранного дома
     * $houseID - ID дома в битриксе
     * $rq - $_REQUEST
     */
    public function getFlats($houseID = false, $rq){
        CModule::IncludeModule("iblock");
        if($houseID === false){
            $arResult = [
                "result" => "error",
                "message" => "Invalid house ID"
            ];
        }else{
            $arFlatSelect = [
                "ID",
                "NAME",
                "CODE",
                "DETAIL_PAGE_URL",
                "PROPERTY_N_FLOOR",
                "PROPERTY_N_ROOMS",
                "PROPERTY_N_AREA",
                "PROPERTY_N_PRICE",
                "PROPERTY_S_ENTRANCE",
                "PROPERTY_F_PRESET",
                "PROPERTY_E_HOUSE",
                "PROPERTY_S_STATUS",
            ];

            $arFlatFilter = [
                "IBLOCK_ID" => IBLOCK_ID_FLAT,
            ];
            $parking = true;
            if($rq["flats_type"] == "flat")
                $arFlatFilter["!PROPERTY_L_COMMERCE_VALUE"] = "Y";
                $arFlatFilter["!PROPERTY_N_PRESET_CODE"] = "кл";
                $parking = false;
            if($rq["flats_type"] == "commerc"){
                $arFlatFilter["PROPERTY_L_COMMERCE_VALUE"] = "Y";
                $arFlatFilter["!PROPERTY_N_PRESET_CODE"] = "кл";
                $parking = false;
            }

            if($houseID > 0){
                $arFlatFilter["PROPERTY_E_HOUSE"] = $houseID;
            }elseif(!$parking){
                $rsHouse = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_HOUSE, "ACTIVE" => "Y", "!PROPERTY_S_TYPE" => "PARKING"], false, false, ["ID"]);
                while($arHouse = $rsHouse->Fetch()){
                    $arHouseID[] = $arHouse["ID"];
                }
                $arFlatFilter["PROPERTY_E_HOUSE"] = $arHouseID;
            }
            if(isset($rq["min_price"])){
                $arFlatFilter[">=PROPERTY_N_PRICE"] = str_replace(" ", "", $rq["min_price"]);
            }
            if(isset($rq["max_price"])){
                $arFlatFilter["<=PROPERTY_N_PRICE"] = str_replace(" ", "", $rq["max_price"]);
            }
            if(isset($rq["min_area"])){
                $arFlatFilter[">=PROPERTY_N_AREA"] = $rq["min_area"];
            }
            if(isset($rq["max_area"])){
                $arFlatFilter["<=PROPERTY_N_AREA"] = $rq["max_area"];
            }
            if(!empty($rq["rooms"])){
                if(count($rq["rooms"]) > 1){
                    $logicFilter = ["LOGIC" => "OR"];
                    foreach($rq["rooms"] as $room){
                        $logicFilter[] = ["PROPERTY_N_ROOMS" => $room];
                    }
                    $arFlatFilter[] = $logicFilter;
                }else{
                    $arFlatFilter["PROPERTY_N_ROOMS"] = $rq["rooms"][0];
                }
            }
            if(!empty($rq["price_range"])){
                $min_price = -1;
                $max_price = 0;
                foreach($rq["price_range"] as $range){
                    $arPrices = explode("-", $range);

                    if($arPrices[0] < $min_price || $min_price === -1)
                        $min_price = $arPrices[0];

                    if($arPrices[1] > $max_price)
                        $max_price = $arPrices[1];
                }
                $arFlatFilter[">=PROPERTY_N_PRICE"] = $min_price;
                $arFlatFilter["<=PROPERTY_N_PRICE"] = $max_price;
            }
            if(isset($rq["min_floor"])){
                $arFlatFilter[">=PROPERTY_N_FLOOR"] = $rq["min_floor"];
            }
            if(isset($rq["max_floor"])){
                $arFlatFilter["<=PROPERTY_N_FLOOR"] = $rq["max_floor"];
            }

            $arResult["flat_filter"] = $arFlatFilter;
            $arResult["rq"] = $rq;

            $arSort = ["PROPERTY_N_PRICE" => "ASC"];
            if(isset($rq["SORT"])){
                $arSort = $rq["SORT"];
            }

            $minPrice = 0;
            $maxPrice = 0;
            $rsFlat = CIBlockElement::GetList($arSort, $arFlatFilter, false, false, $arFlatSelect);
            while($arFlat = $rsFlat->GetNext()){
                $arFlatResult = [];
                $minPrice = 0;
                $maxPrice = 0;
                foreach($arFlat as $key => $val){
                    if(strpos($key, "~") === 0)
                        continue;
                    if(strpos($key, "PROPERTY_") === 0){
                        $key = str_replace("PROPERTY_", "", $key);
                        $key = str_replace("_VALUE", "", $key);

                        if($key == "F_PRESET" && !is_null($val) && intval($val)){
                            $arFile = CFile::ResizeImageGet($val, array("width" => 70, "height" => "70"), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                            $arFlatResult["PROPERTIES"][$key."_PREVIEW"] = $arFile["src"];
                            $arFile = CFile::ResizeImageGet($val, array("width" => 600, "height" => "600"), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                            $val = $arFile["src"];
                        }elseif(strpos($key, "F_") === 0 && !is_null($val)){
                            $val = CFile::GetPath($val);
                        }

                        $arFlatResult["PROPERTIES"][$key]["VALUE"] = $val;

                        if($key == "N_PRICE" && ($val < $minPrice || $minPrice == 0)){
                            $minPrice = $val;
                        }
                        if($key == "N_PRICE" && ($val > $maxPrice || $maxPrice == 0)){
                            $maxPrice = $val;
                        }
                    }else{
                        $arFlatResult[$key] = $val;
                    }
                }
                $arResult["FLOOR_FLAT"][$arFlatResult["PROPERTIES"]["N_FLOOR"]["VALUE"]][] = $arFlatResult;
                $arResult["FLOOR"][] = $arFlatResult["PROPERTIES"]["N_FLOOR"]["VALUE"];
                $arResult["AREA"][] = $arFlatResult["PROPERTIES"]["N_AREA"]["VALUE"];
                $arResult["ROOMS"][] = $arFlatResult["PROPERTIES"]["N_ROOMS"]["VALUE"];
                $arResult["ITEMS"][] = $arFlatResult;
            }

            $arResult["MIN_PRICE"] = $minPrice;
            $arResult["MAX_PRICE"] = $maxPrice;

            $arResult["ROOMS"] = array_unique($arResult["ROOMS"]);
            sort($arResult["ROOMS"]);

            $arResult["AREA"] = array_unique($arResult["AREA"]);
            sort($arResult["AREA"]);

            $arResult["FLOOR"] = array_unique($arResult["FLOOR"]);
            sort($arResult["FLOOR"]);

            $arResult["result"] = "success";
        }

        return $arResult;
    }

    /**
     * Добавляем элемент
     */
    public function addElement($arFields){
        CModule::IncludeModule("iblock");
        $el = new CIBlockElement;
        $ID = $el->Add($arFields);

        return $ID;
    }

    /**
     * Проверка изменения полей, перед обновлением элемента
     *
     * @param [type] $arFields - поля, отправленные в форме обновления элемента
     * @param [type] $key - поле, которое нужно проверить
     * @return true || false - изменилось ли поле
     */
    public function checkField($arFields, $key){
        $change = false;
        CModule::IncludeModule("iblock");
        $rsElement = CIBlockElement::GetList([], ["IBLOCK_ID" => $arFields["IBLOCK_ID"], "ID" => $arFields["ID"]], false, false, ["ID", $key]);
        if($arElement = $rsElement->Fetch()){
            if(strpos($key, "PROPERTY_") !== false){
                if(strpos($key, "PROPERTY_L_") !== false){
                    $shugar = "_ENUM_ID";
                }else{
                    $shugar = "_VALUE";
                }
                $arProp = explode(":", $arElement[$key."_VALUE_ID"]);
                if(isset($arFields["PROPERTY_VALUES"][$arProp[1]][0]["VALUE"])){
                    if($arFields["PROPERTY_VALUES"][$arProp[1]][0]["VALUE"] != $arElement[$key.$shugar]){
                        $change = true;
                    }
                }elseif(isset($arFields["PROPERTY_VALUES"][$arProp[1]][$arElement[$key."_VALUE_ID"]]["VALUE"])){
                    if($arFields["PROPERTY_VALUES"][$arProp[1]][$arElement[$key."_VALUE_ID"]]["VALUE"] != $arElement[$key.$shugar]){
                        $change = true;
                    }
                }
            }else{
                if($arFields[$key] != $arElement[$key]){
                    $change = true;
                }
            }
        }

        return $change;
    }

    /**
     * Форматируем размер файла
     */
    public function formatFileSize($size){
        if(strlen($size) > 6){
            $result = intval($size/1000000)." мб";
        }else{
            $result = intval($size/1000)." кб";
        }
        return $result;
    }
}