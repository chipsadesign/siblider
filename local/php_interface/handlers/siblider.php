<?
Class Siblider {

	/**
	 * Формируем сслыку предложений месяца, если они есть
	 *
	 * @return html
	 */
	public function showMonthOfferLink(){
		$result = "";
		CModule::IncludeModule("iblock");
		$rsElement = CIBlockElement::GetList(
			[],
			[
				"IBLOCK_ID" => 27,
				"ACTIVE" => "Y"
			],
			false,
			["nTopCount" => 1],
			["ID"]
		);
		if($arElement = $rsElement->Fetch()){
			$result = '<a class="hover-underline link link--label" href="/month-offers/">
				<span>Предложения месяца</span>
				<span class="link__label">!</span>
			</a>';
			global $APPLICATION;
			$APPLICATION->SetPageProperty("OFFERS_OF_THE_MONTH_LINK", "<li>".$result."</li>");
		}

		return $result;
	}

	/**
	 * Полуаем инфу о доме
	 *
	 * @param int $houseID
	 * @return array
	 */
	public function getHouseInfo($houseID, $arrSelect){
		CModule::IncludeModule("iblock");
		$result = [];

		$arSelect = [
			"ID",
			"NAME",
		];

		$rsElement = CIBlockElement::GetList(
			[],
			[
				"IBLOCK_ID" => IBLOCK_ID_HOUSE,
				"ID" => $houseID
			],
			false,
			false,
			array_merge(
				$arSelect,
				$arrSelect
			)
		);
		if($arElement = $rsElement->GetNext()){
			$result = $arElement;
		}

		if(!empty($result["PROPERTY_E_MANAGER_VALUE"])){
			$result["Managers"] = self::getManagers($result["PROPERTY_E_MANAGER_VALUE"]);
		}

		return $result;
	}

	/**
	 * Получаем список менеджеров
	 *
	 * @param array $arIDs
	 * @return array
	 */
	private function getManagers($arIDs){
		CModule::IncludeModule("iblock");
		$result = [];
		$rsElement = CIBlockElement::GetList(
			["SORT" => "ASC"],
			[
				"IBLOCK_ID" => IBLOCK_ID_MANAGER,
				"ID" => $arIDs
			],
			false,
			false,
			[
				"NAME",
				"PREVIEW_PICTURE",
				"PROPERTY_S_POSITION",
				"PROPERTY_S_PHONE",
				"PROPERTY_S_EMAIL",
			]
		);
		while($arElement = $rsElement->Fetch()){
			$arElement["PREVIEW_PICTURE"] = CFile::GetPath($arElement["PREVIEW_PICTURE"]);
			$result[] = $arElement;
		}

		return $result;
	}

	/**
	 * Получаем список квартир по ID
	 *
	 * @param array $arIDs
	 * @param array $arrSelect
	 * @return array
	 */
	public function getFlats($arIDs, $arrSelect){
		$result = [];
		CModule::IncludeModule("iblock");
		$arSelect = [
			"ID",
			"NAME"
		];
		$rsElement = CIBlockElement::GetList(
			["NAME" => "ASC"],
			[
				"IBLOCK_ID" => IBLOCK_ID_FLAT,
				"ID" => $arIDs,
			],
			false,
			false,
			array_merge($arSelect, $arrSelect)
		);
		while($arElement = $rsElement->Fetch()){
			if($arElement["PROPERTY_F_PRESET_VALUE"]){
				$arElement["PROPERTY_F_PRESET_VALUE"] = CFile::GetPath($arElement["PROPERTY_F_PRESET_VALUE"]);
			}
			$result[] = $arElement;
		}

		return $result;
	}
}