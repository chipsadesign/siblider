<?
/**
 * Генерируем журнал изменений [IBLOCK_ID_CHANGELOG]
 */
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "ChangeLogHandler");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "ChangeLogHandler");
function ChangeLogHandler($arFields){
	$arAddFields["IBLOCK_ID"] = IBLOCK_ID_CHANGELOG;
	$arAddFields["PROPERTY_VALUES"]["E_USER"] = $arFields["MODIFIED_BY"];

	//Обновление дома
	if($arFields["IBLOCK_ID"] == IBLOCK_ID_HOUSE){
		// NAME - название
		// PREVIEW_PICTURE - логотип
		// DETAIL_PICTURE - деталка
		// [33] PROPERT_S_ADRESS - адрес
		// [30] PROPERTY_L_MATERIAL - материал стан
		// [31] PROPERTY_S_DATE_START - Начало строительства
		// [32] PROPERTY_S_DATE_END - Срок сдачи
		// [34] PROPERTY_L_STATUS - Статус
		// [35] PROPERTY_N_FLOORS - Этажей
		// [36] PROPERTY_N_ENTRANCE - Подъездов
		// [45] PROPERTY_F_PICTURE - анонсовые фото
		// [46] PROPERTY_S_FACING - Отделка
		$arFiledsCheck = [
			["NAME" => "NAME"],
			["TYPE" => "file", "NAME" => "PREVIEW_PICTURE",],
			["TYPE" => "file", "NAME" => "DETAIL_PICTURE",],
			["NAME" => PROPERT_S_ADRESS],
			["NAME" => PROPERTY_L_MATERIAL],
			["NAME" => PROPERTY_S_DATE_START],
			["NAME" => PROPERTY_S_DATE_END],
			["NAME" => PROPERTY_L_STATUS],
			["NAME" => PROPERTY_N_FLOORS],
			["NAME" => PROPERTY_N_ENTRANCE],
			["TYPE" => "file", "NAME" => PROPERTY_F_PICTURE],
			["NAME" => PROPERTY_S_FACING],
		];
		if(isset($arFields["ID"])){
			foreach($arFiledsCheck as $arCheck){
				if($arCheck["TYPE"] == "file"){
					if($arFields[$arCheck["NAME"]]["name"]){
						$arAddFields["NAME"] = "На сайт добавлен файл ".$arFields[$arCheck["NAME"]]["name"];
						$arAddFields["PROPERTY_VALUES"]["F_FILE"] = $arFields[$arCheck["NAME"]];
						$arAddFields["PROPERTY_VALUES"]["E_HOUSE"] = $arFields["ID"];

						siteHandlers::addElement($arAddFields);
					}
				}else{
					$change = siteHandlers::checkField($arFields, $arCheck["NAME"]);
					if($change){
						$arAddFields["NAME"] = "На сайте изменено описание";
						$arAddFields["PROPERTY_VALUES"]["E_HOUSE"] = $arFields["ID"];

						siteHandlers::addElement($arAddFields);
					}
				}
			}
		}
	}

	//Обновление хода строительства дома
	if($arFields["IBLOCK_ID"] == IBLOCK_ID_PROGRESS){
		// PREVIEW_PICTURE - фото
		// [67] PROPERTY_S_VIDEO - видео
		if(!isset($arFields["ID"])){
			if(!empty($arFields["PREVIEW_PICTURE"])){
				foreach($arFields["PROPERTY_VALUES"][66] as $arHouse){
					$arAddFields["NAME"] = "На сайт добавлен файл ".$arValue["VALUE"]["name"];
					$arAddFields["PROPERTY_VALUES"]["F_FILE"] = $arValue["VALUE"];
					$arAddFields["PROPERTY_VALUES"]["E_HOUSE"] = $arHouse["VALUE"];
				}
				siteHandlers::addElement($arAddFields);
			}
			if(!empty($arFields["PROPERTY_VALUES"][67]["n0"]["VALUE"])){
				foreach($arFields["PROPERTY_VALUES"][66] as $arHouse){
					$arAddFields["NAME"] = "На сайт добавлено видео хода строительства";
					$arAddFields["PROPERTY_VALUES"]["E_HOUSE"] = $arHouse["VALUE"];
					$arAddFields["PROPERTY_VALUES"]["S_VIDEO"] = $arFields["PROPERTY_VALUES"][67]["n0"]["VALUE"];
				}
				siteHandlers::addElement($arAddFields);
			}
		}
	}

	//Обновление слайда дома
	if($arFields["IBLOCK_ID"] == IBLOCK_ID_HOUSE_SLIDES){
		// PREVIEW_TEXT - Описание
		// [50] PROPERTY_F_IMG - картинки слайдрв

		$propID = 50;
		foreach($arFields["PROPERTY_VALUES"][$propID] as $key => $arValue){
			if(strpos($key, "n") === 0){
				foreach($arFields["PROPERTY_VALUES"][48] as $arHouse){
					if(!$arHouse["VALUE"])
						continue;

					$arAddFields["NAME"] = "На сайт добавлена фотография ".$arValue["VALUE"]["name"];
					$arAddFields["PROPERTY_VALUES"]["F_FILE"] = $arValue["VALUE"];
					$arAddFields["PROPERTY_VALUES"]["E_HOUSE"] = $arHouse["VALUE"];

					siteHandlers::addElement($arAddFields);
				}
			}
		}
		if(!isset($arFields["ID"])){
			foreach($arFields["PROPERTY_VALUES"][48] as $arHouse){
				if(!$arHouse["VALUE"])
					continue;

				$arAddFields["NAME"] = "На сайте добавлено описание";
				$arAddFields["PROPERTY_VALUES"]["F_FILE"] = $arValue["VALUE"];
				$arAddFields["PROPERTY_VALUES"]["E_HOUSE"] = $arHouse["VALUE"];

				siteHandlers::addElement($arAddFields);
			}
		}else{
			$change = siteHandlers::checkField($arFields, "PREVIEW_TEXT");
			if($change){
				foreach($arFields["PROPERTY_VALUES"][48] as $arHouse){
					if(!$arHouse["VALUE"])
						continue;

					$arAddFields["NAME"] = "На сайте изменено описание";
					$arAddFields["PROPERTY_VALUES"]["E_HOUSE"] = $arHouse["VALUE"];

					siteHandlers::addElement($arAddFields);
				}
			}
		}
	}

	//Обновление документация дома
	if($arFields["IBLOCK_ID"] == IBLOCK_ID_DOCS){
		// [68] PROPERTY_F_FILE - файл
		if(!isset($arFields["ID"])){
			$propID = 68;
			foreach($arFields["PROPERTY_VALUES"][$propID] as $key => $arValue){
				if(strpos($key, "n") === 0){

					$arAddFields["NAME"] = "На сайт добавлен файл ".$arValue["VALUE"]["name"];
					$arAddFields["PROPERTY_VALUES"]["F_FILE"] = $arValue["VALUE"];
					$arAddFields["PROPERTY_VALUES"]["E_HOUSE"] = $arFields["PROPERTY_VALUES"][71]["n0"]["VALUE"];

					siteHandlers::addElement($arAddFields);
				}
			}
		}
	}
}