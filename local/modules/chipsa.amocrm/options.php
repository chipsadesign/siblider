<?

use Bitrix\Main\Localization\Loc;
use	Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

$request = HttpApplication::getInstance()->getContext()->getRequest();

$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);

Loader::includeModule($module_id);

$aTabs = array(
	array(
		"DIV" 	  => "edit",
		"TAB" 	  => Loc::getMessage("MODULE_OPTIONS_TAB_NAME"),
		"TITLE"   => Loc::getMessage("MODULE_OPTIONS_TAB_NAME"),
		"OPTIONS" => array(
			Loc::getMessage("MODULE_OPTIONS_TAB_COMMON"),
			array(
				"account",
				Loc::getMessage("MODULE_OPTIONS_TAB_ACCOUNT"),
				"",
				array("text", 60)
			),
			array(
				"login",
				Loc::getMessage("MODULE_OPTIONS_TAB_LOGIN"),
				"",
				array("text", 60)
			),
			array(
				"hash",
				Loc::getMessage("MODULE_OPTIONS_TAB_HASH"),
				"",
				array("text", 60)
			),
			Loc::getMessage("MODULE_OPTIONS_TAB_OTHER"),
			array(
				"created_by",
				Loc::getMessage("MODULE_OPTIONS_TAB_CREATED_BY"),
				"",
				array("text", 60)
			),
			array(
				"responsible_user_id",
				Loc::getMessage("MODULE_OPTIONS_TAB_RESPONSIBLE_USER_ID"),
				"",
				array("text", 60)
			),
		)
	)
);

if($request->isPost() && check_bitrix_sessid()){

	foreach($aTabs as $aTab){
		foreach($aTab["OPTIONS"] as $arOption){
			if(!is_array($arOption)){
				continue;
			}

			if($arOption["note"]){
				continue;
			}

			if($request["apply"]){
				$optionValue = $request->getPost($arOption[0]);
				if($arOption[0] == "switch_on"){
					if($optionValue == ""){
						$optionValue = "N";
					}
				}

				Option::set($module_id, $arOption[0], is_array($optionValue) ? implode(",", $optionValue) : $optionValue);
			}elseif($request["default"]){

				Option::set($module_id, $arOption[0], $arOption[2]);
			}
		}
	}

	LocalRedirect($APPLICATION->GetCurPage()."?mid=".$module_id."&lang=".LANG);
}

$tabControl = new CAdminTabControl(
	"tabControl",
	$aTabs
);

$tabControl->Begin();
?>

<form action="<? echo($APPLICATION->GetCurPage()); ?>?mid=<? echo($module_id); ?>&lang=<? echo(LANG); ?>" method="post">

	<?
	foreach($aTabs as $aTab){

		if($aTab["OPTIONS"]){

			$tabControl->BeginNextTab();

			__AdmSettingsDrawList($module_id, $aTab["OPTIONS"]);
		}
	}

	$tabControl->Buttons();
	?>

	<input type="submit" name="apply" value="<? echo(Loc::GetMessage("MODULE_OPTIONS_INPUT_APPLY")); ?>" class="adm-btn-save" />

	<?
	echo(bitrix_sessid_post());
	?>

</form>

<?

$tabControl->End();
?>