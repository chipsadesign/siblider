<?

$MESS["MODULE_NAME"] = "Интеграция с amoCRM";
$MESS["MODULE_DESCRIPTION"]  = "Интеграция";
$MESS["MODULE_PARTNER_NAME"] = "Чипса";
$MESS["MODULE_PARTNER_URI"]  = "https://chipsa.ru/";

$MESS["MODULE_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["MODULE_INSTALL_TITLE"] = "Установка модуля";
$MESS["MODULE_UNINSTALL_TITLE"] = "Деинсталляция модуля";