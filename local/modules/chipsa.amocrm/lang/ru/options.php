<?

$MESS["MODULE_OPTIONS_TAB_NAME"] = "Настройки";
$MESS["MODULE_OPTIONS_TAB_COMMON"] = "Общие";
$MESS["MODULE_OPTIONS_TAB_ACCOUNT"] = "Аккаунт:";
$MESS["MODULE_OPTIONS_TAB_LOGIN"] = "Логин:";

$MESS["MODULE_OPTIONS_TAB_OTHER"] = "Другие";
$MESS["MODULE_OPTIONS_TAB_CREATED_BY"] = "ID пользователя создающего сущности:";
$MESS["MODULE_OPTIONS_TAB_RESPONSIBLE_USER_ID"] = "ID ответственного:";


$MESS["MODULE_OPTIONS_TAB_HASH"] = "Хэш для доступа:";

$MESS["MODULE_OPTIONS_INPUT_APPLY"] = "Применить";