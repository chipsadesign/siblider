<?

// namespace Chipsa;

class Amocrm
{

	const CURL_ERRORS = [
		301 => "Moved permanently",
		400 => "Bad request",
		401 => "Unauthorized",
		403 => "Forbidden",
		404 => "Not found",
		500 => "Internal server error",
		502 => "Bad gateway",
		503 => "Service unavailable",
	];

	const COOKIE_PATH = "/upload/amocrm";

	protected $account;
	protected $login;
	protected $hash;

	protected $created_by;
	protected $responsible_user_id;
	protected $customFields = [];
	protected $customFieldsTypes = [
		"SELECT" => 4,
		"MULTISELECT" => 5,
		"SMART_ADDRESS" => 13
	];

	public $isConnect = false;
	public $errors = [];

	function __construct(){
		$this->account =  \Bitrix\Main\Config\Option::get("chipsa.amocrm", "account");
		$this->login =  \Bitrix\Main\Config\Option::get("chipsa.amocrm", "login");
		$this->hash =  \Bitrix\Main\Config\Option::get("chipsa.amocrm", "hash");

		$this->created_by =  \Bitrix\Main\Config\Option::get("chipsa.amocrm", "created_by");
		$this->responsible_user_id =  \Bitrix\Main\Config\Option::get("chipsa.amocrm", "responsible_user_id");

		if(empty($this->responsible_user_id)){
			$this->responsible_user_id = $this->created_by;
		}

		$this->isConnect = $this->auth();

		if($this->isConnect){
			$this->getCustomFields();
		}
	}

	/**
	 * получает пользовательские поля из настроек модуля. Если их там нет, получаем список через api и записываем в настроеки модуля
	 *
	*/
	protected function getCustomFields(){

		$customFields = []; \Bitrix\Main\Config\Option::get("chipsa.amocrm", "custom_fields");
		$arCustomFields = json_decode($customFields, true);
		if(
			empty($arCustomFields)
			|| !is_array($arCustomFields)
		){
			$this->updateCustomFields();
		}else{
			$this->customFields = $arCustomFields;
		}
	}

	/**
	 * Отправляем запрос к внешнему серверу и получаем пользовательские поля, результат записываем в переменную класса
	 *
	*/
	protected function customFieldsFromApi(){

		$arAnswer = $this->doRequest([
			"url" => "https://".$this->account.".amocrm.ru/api/v2/account?with=custom_fields",
		]);

		$arCustomFields = [];
		if(is_array($arAnswer["_embedded"]["custom_fields"])){
			foreach($arAnswer["_embedded"]["custom_fields"]  as $typeKey => $arType){
				foreach($arType as $arField){
					if(!empty($arField["code"])){
						$key = $arField["code"];
					}else{
						$key = $arField["name"];
					}
					$key = trim($key);
					$key = toLower($key);


					$arEnums = [];
					if(is_array($arField["enums"])){
						foreach($arField["enums"] as $enumKey => $enumItem){
							$enumItem = trim($enumItem);
							$enumItem = toLower($enumItem);
							$arEnums[$enumItem] = $enumKey;
						}
					}

					$arSubTypes = [];
					if(is_array($arField["subtypes"])){
						foreach($arField["subtypes"] as $enumKey => $arSubTypesItems){
							$arSubTypesItems["title"]= trim($arSubTypesItems["title"]);
							$arSubTypesItems["title"] = toLower($arSubTypesItems["title"]);
							$arSubTypes[$arSubTypesItems["title"]] = $arSubTypesItems["name"];
						}
					}

					$arCustomFields[$typeKey][$key] = [
						"ID" => $arField["id"],
						"NAME" => $arField["name"],
						"CODE" => $arField["code"],
						"FIELD_TYPE" => $arField["field_type"],
						"TYPE" => $typeKey,
					];

					if (!empty($arEnums)) {
						$arCustomFields[$typeKey][$key]["ENUMS"] = $arEnums;
					}
					if (!empty($arSubTypes)) {
						$arCustomFields[$typeKey][$key]["SUBTYPES"] = $arSubTypes;
					}
				}
			}
		}

		$this->customFields = $arCustomFields;
	}

	/**
	 * Обновляем список кастомных полей в настройках модуля
	 *
	*/
	public function updateCustomFields(){
		$this->customFieldsFromApi();
		\Bitrix\Main\Config\Option::set("chipsa.amocrm", "custom_fields", json_encode($this->customFields));
	}

	/**
	 * переформатируем входные параметры
	 *
	 * @param [type] $arParam - массив с полями сущности
	*/
	protected function prepareParamArray(&$arParam){
		// даем возможность добавлять в качестве параметра как массив контактов, так и один контакт
		foreach(["leads_id", "contacts_id"] as $strField){
			if(!is_array($arParam[$strField]) && intval($arParam[$strField]) > 0)
				$arParam[$strField] = [$arParam[$strField]];
		}

		// С тегами за пределеми класса работаем как с массивом
		if(is_array($arParam["tags"])){
			$arParam["tags"] = implode(",", $arParam["tags"]);
		}
	}

	/**
	 * Преобразуем входной массив в массив необходимого для api формата
	 *
	 * @param [type] $arParam - массив с полями сущности
	*/
	protected function createCrmArray(&$arParam, $strTypeObject){
		$this->prepareParamArray($arParam);

		foreach($arParam as $key => $value){
			if (empty($value)) {
				continue;
			}
			$key = trim(toLower($key));

			if(
				in_array($key, ["id", "name", "contacts_id", "company_id", "leads_id", "tags", "status_id", "element_id", "element_type", "complete_till", "task_type", "text"])
			){
				$arCrm[$key] = $value;
			}elseif(
				isset($this->customFields[$strTypeObject][$key])
			){
				if($this->customFields[$strTypeObject][$key]["FIELD_TYPE"] == $this->customFieldsTypes["SMART_ADDRESS"]){
					global $APPLICATION;
					$APPLICATION->RestartBuffer();
					if (!empty($value) && is_array($value)) {
						$arTemp = [
							"id" => $this->customFields[$strTypeObject][$key]["ID"],
							"values" => []
						];
						foreach ($value as $keyAddress => $valueAddress) {
							$arTemp["values"][] = [
								"value" => $valueAddress,
								"subtype" => $this->customFields[$strTypeObject][$key]["SUBTYPES"][$keyAddress]
							];
						}
					}

				}elseif (
						!in_array($key, ["phone", "email"])
						&& isset($this->customFields[$strTypeObject][$key]["ENUMS"])
						&& is_array($this->customFields[$strTypeObject][$key]["ENUMS"])
				){
					if (is_array($value)) {
						foreach($value as $strSubValue){
							$strSubValue = trim($strSubValue);
							$strSubValue = toLower($strSubValue);
							if(isset($this->customFields[$strTypeObject][$key]["ENUMS"][$strSubValue])){
								$arValuesEnum[] = $this->customFields[$strTypeObject][$key]["ENUMS"][$strSubValue];
							}
						}
					}else{
						$value = trim($value);
						$value = toLower($value);
						if(isset($this->customFields[$strTypeObject][$key]["ENUMS"][$value])){
							$value = $this->customFields[$strTypeObject][$key]["ENUMS"][$value];
						}
						if($this->customFields[$strTypeObject][$key]["FIELD_TYPE"] == $this->customFieldsTypes["MULTISELECT"]){
							$arValuesEnum = [
								$value
							];
						}else{
							$arValuesEnum = [
								["value" => $value]
							];
						}
					}
					$arTemp = [
						"id" => $this->customFields[$strTypeObject][$key]["ID"],
						"values" => $arValuesEnum
					];
				}else{
					$arTemp = [
						"id" => $this->customFields[$strTypeObject][$key]["ID"],
						"values" => [
							[
								"value" => $value,
							]
						]
					];
				}
				if(in_array($key, ["phone", "email"])){
					$arTemp["values"][0]["enum"] = "WORK";
				}

				$arCrm["custom_fields"][] = $arTemp;
			}
		}
		if (empty($arParam["id"])) {
			if(empty($arCrm["responsible_user_id"])){
				$arCrm["responsible_user_id"] = $this->responsible_user_id;
			}
			if(empty($arCrm["created_by"])){
				$arCrm["created_by"] = $this->created_by;
			}
			if(empty($arCrm["created_at"])){
				$arCrm["created_at"] = time();
			}
		}else{
			$arCrm["updated_at"] = time();
		}
		return $arCrm;

	}

	/**
	 * Преобразуем массив в формата api в удобный для работы
	 *
	 * @param [type] $arParam - массив с полями сущности
	*/
	protected function createSimpleArray($arCrm){
		$arConnect = [
			"company" => "companies",
			"contact" => "contacts",
			"lead" => "leads"
		];
		$arElement = [];
		$strFieldType = $arConnect[$arCrm["type"]];

		foreach($arCrm as $key => $value){
			if($key == "custom_fields"){
				foreach($value as $arField){
					$newKey = !empty($arField["code"]) ? toLower($arField["code"]) : trim(toLower($arField["name"]));

					if(isset($this->customFields[$arConnect[$arCrm["type"]]][$newKey])){
						$arElement[$newKey] = $arField["values"][0]["value"];
					}
				}
			}elseif($key == "company"){
				$arElement["company_id"] = $value["id"];
			}elseif($key == "tags"){
				$arElement["tags"] = array_column($value, "name");
			}elseif($key == "linked_leads_id"){
				$arElement["leads_id"] = $value;
			}else{
				$arElement[$key] = $value;
			}
		}
		return $arElement;

	}

	/**
	 * Отправляем запрос к внешнему серверу
	 *
	 * @param [type] $arParams - ["url" => url внешнего сервиса, "post_data" => моссив параметров post запроса]
	 * @return возвращает декодированный ответ внешнего сервиса
	*/
	protected function doRequest($arParams){
		if(empty($arParams["url"])){
			$this->errors["errors"][] = __FUNCTION__.": Не указан url";
			return false;
		}

		if(empty($arParams["post_data"])){
			$arParams["post_data"] = [];
		}

		$curl = curl_init(); #Сохраняем дескриптор сеанса cURL
		#Устанавливаем необходимые опции для сеанса cURL
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_USERAGENT, "amoCRM-API-client/1.0");
		curl_setopt($curl, CURLOPT_URL, $arParams["url"]);
		if(!empty($arParams["post_data"])){
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($arParams["post_data"]));
			curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		}
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_COOKIEFILE, $_SERVER["DOCUMENT_ROOT"].self::COOKIE_PATH."/cookie.txt");
		curl_setopt($curl, CURLOPT_COOKIEJAR, $_SERVER["DOCUMENT_ROOT"].self::COOKIE_PATH."/cookie.txt");
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		$out = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
		curl_close($curl);

		if ($code != 200 && $code != 204) {
			$this->errors[] = __FUNCTION__.": ".$code." > ".(!is_null(self::CURL_ERRORS[$code]) ? self::CURL_ERRORS[$code] : "Undescribed error");
		}
		$arResult = json_decode($out, true);

		return is_array($arResult["response"]) ? $arResult["response"] : (is_array($arResult) ? $arResult : false);
	}

	/**
	 * авторизация в amocrm
	 *
	 * @return возвращает true/false в зависимости от успеха авторизации
	*/
	protected function auth(){

		$arUser = [
			"USER_LOGIN" => $this->login,
			"USER_HASH" => $this->hash,
		];

		$arAnswer = $this->doRequest([
			"url" => "https://".$this->account.".amocrm.ru/private/api/auth.php?type=json",
			"post_data" => $arUser
		]);

		return isset($arAnswer["auth"]) ? $arAnswer["auth"] : false;
	}

	/**
	 * Ищем пользователя по параметру
	 *
	 * @param [type] $query - параметр поиска, строка или массив
	 * @return найденные записи
	*/
	public function findContact($query){
		if(empty($query)){
			return false;
		}

		$otherFilter = [];
		if(is_array($query)){
			$otherFilter = $query;
			$query = array_shift($otherFilter);
		}

		$arAnswer = $this->doRequest([
			"url" => "https://".$this->account.".amocrm.ru/private/api/v2/json/contacts/list?query=".$query
		]);

		$arResult = false;
		if(is_array($arAnswer["contacts"]) && !empty($arAnswer["contacts"])){

			foreach($arAnswer["contacts"] as $arItem){
				$arResult[$arItem["id"]] = $this->createSimpleArray($arItem);
			}
		}

		if(!empty($otherFilter)){
			$arResult = array_filter(
				$arResult,
				function ($arItem) use ($otherFilter){
					$isOk = true;
					foreach($otherFilter as $val){
						if(array_search($val, $arItem, true) === false){
							$isOk = false;
							break;
						}
					}
					return $isOk;
				}
			);
		}

		return $arResult;
	}

	/**
	 * Добавляем контакт в amocrm
	 *
	 * @param [type] $arContact - массив с полями контакта
	 * @return false если создать контакт не удалось и ID контакта если контакт создался
	*/
	public function addContact($arContact){
		if(!is_array($arContact)){
			return false;
		}
		$arCrmContact = $this->createCrmArray($arContact, "contacts");
		$arAnswer = $this->doRequest([
			"url" => "https://".$this->account.".amocrm.ru/api/v2/contacts",
			"post_data" => ["add" => [$arCrmContact]]
		]);

		$id = intval($arAnswer["_embedded"]["items"][0]["id"]);
		return ($id > 0) ? $id : false;

	}

	/**
	 * Ищем пользователя по параметру
	 *
	 * @param [type] $query - параметр поиска, строка
	 * @return найденные записи
	*/
	public function findCompany($query){
		if(empty($query)){
			return false;
		}

		$arAnswer = $this->doRequest([
			"url" => "https://".$this->account.".amocrm.ru/private/api/v2/json/companies/list?query=".$query
		]);
		$arResult = [];
		if(is_array($arAnswer["contacts"]) && !empty($arAnswer["contacts"])){

			foreach($arAnswer["contacts"] as $arItem){
				$arResult[$arItem["id"]] = $this->createSimpleArray($arItem);
			}
		}
		return $arResult;
	}

	/**
	 * Добавляем компанию в amocrm
	 *
	 * @param [type] $arCompany - массив с полями компании
	 * @return false если создать контакт не удалось и ID компании если компания создался
	*/
	public function addCompany($arCompany){
		if(!is_array($arCompany)){
			return false;
		}

		$arCrmCompany = $this->createCrmArray($arCompany, "companies");

		$arAnswer = $this->doRequest([
			"url" => "https://".$this->account.".amocrm.ru/api/v2/companies",
			"post_data" => ["add" => [$arCrmCompany]]
		]);

		$id = intval($arAnswer["_embedded"]["items"][0]["id"]);
		return ($id > 0) ? $id : false;

	}

	/**
	 * Обновляем компанию в amocrm
	 *
	 * @param [type] $id - id компании
	 * @param [type] $arCompany - массив с полями компании
	 * @return false если создать компанию не удалось и ID компании если компания создался
	*/
	public function updateCompany($id, $arCompany){
		if (intval($id) < 1) {
			return false;
		}
		if(!is_array($arCompany)){
			return false;
		}
		$arCompany["id"] = $id;

		$arCrmCompany = $this->createCrmArray($arCompany, "companies");

		$arAnswer = $this->doRequest([
			"url" => "https://".$this->account.".amocrm.ru/api/v2/companies",
			"post_data" => ["update" => [$arCrmCompany]]
		]);

		$id = intval($arAnswer["_embedded"]["items"][0]["id"]);
		return ($id > 0) ? $id : false;

	}

	/**
	 * Добавляем сделку в amocrm
	 *
	 * @param [type] $arContact - массив с полями контакта
	 * @return false если создать сделка не удалось и ID сделки если сделка создался
	*/
	public function addLead($arLead){
		if(!is_array($arLead)){
			return false;
		}

		$arCrmLead = $this->createCrmArray($arLead, "leads");

		$arAnswer = $this->doRequest([
			"url" => "https://".$this->account.".amocrm.ru/api/v2/leads",
			"post_data" => ["add" => [$arCrmLead]]
		]);

		$id = intval($arAnswer["_embedded"]["items"][0]["id"]);
		return ($id > 0) ? $id : false;

	}

	/**
	 * Добавляем задачу в amocrm
	 *
	 * @param [type] $arTask - массив с полями задачи
	 * @return false если создать задачу не удалось и ID задачи если задача создалась
	*/
	public function addTask($arTask){
		if(!is_array($arTask)){
			return false;
		}

		$arCrmTask = $this->createCrmArray($arTask, "tasks");

		$arAnswer = $this->doRequest([
			"url" => "https://".$this->account.".amocrm.ru/api/v2/tasks",
			"post_data" => ["add" => [$arCrmTask]]
		]);

		$id = intval($arAnswer["_embedded"]["items"][0]["id"]);
		return ($id > 0) ? $id : false;

	}

	/**
	 * Добавляем примечание в amocrm
	 *
	 * @param [type] $arNote - массив с полями примечания
	 * @return false если создать примечание не удалось и ID задачи если примечание создалась
	*/
	public function addNote($arNote){
		if(!is_array($arNote)){
			return false;
		}

		$arCrmNote = $this->createCrmArray($arNote, "notes");

		$arAnswer = $this->doRequest([
			"url" => "https://".$this->account.".amocrm.ru/api/v2/notes",
			"post_data" => ["add" => [$arCrmNote]]
		]);

		$id = intval($arAnswer["_embedded"]["items"][0]["id"]);
		return ($id > 0) ? $id : false;

	}
}
