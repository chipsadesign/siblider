<?$zoom = $zoom ? $zoom : 13 ?>
var token = 'pk.eyJ1Ijoia3VsZXNob2lkIiwiYSI6Imltczc0Y3MifQ.rHmq6Hxw6aqF5Xds80YUwg';
mapboxgl.accessToken = 'pk.eyJ1Ijoia3VsZXNob2lkIiwiYSI6Imltczc0Y3MifQ.rHmq6Hxw6aqF5Xds80YUwg';

var map = new mapboxgl.Map({
    container: '<?=$mapContainer ? $mapContainer : "map"?>',
    style: 'mapbox://styles/kuleshoid/cjma9ywztgh8i2sqqfmxjn6b5',
    center: [92.879682, 55.995052],
    zoom: <?=$zoom?>,
    maxZoom: 16.5,
    minZoom: 11.5,
    scrollZoom: '<?=$mapContainer ? false : true ?>',
});

var nav = new mapboxgl.NavigationControl();
map.addControl(nav, 'top-left');

var geojson = <?include($_SERVER["DOCUMENT_ROOT"]."/local/ajax/map.php")?>;

<?if(intval($mapID) > 0):?>
    map.flyTo({center: geojson["mapCenter"]});
<?endif;?>

geojson.features.forEach(function(marker){
    var el = document.createElement('div');
    var markerIcon = document.createElement('div');
    var html = '<img src="'+marker.properties.icon.src+'">';
    var additionalClass = '';
    if(marker.model !== false){
        html = '<div style="background: url('+marker.model+')" class="house-model"></div>' + html;
        additionalClass = 'withModel';
    }
    var markerClass = marker.properties.class;
    if(<?=$zoom?> >= 15.5 && markerClass !== undefined){
        markerClass = markerClass.replace('hidden', '');
    }
    el.className = 'mapbox--marker small-view '+markerClass;
    markerIcon.className = 'mapbox--marker__icon';
    if(marker.type != "Infra")
        html = '<a class="' + additionalClass + '" href="' + marker.properties.link + '">' + html + '<span>' + marker.properties.name + '</span>' + '</a>';
    markerIcon.innerHTML = html;
    el.appendChild(markerIcon);

    new mapboxgl.Marker(el)
        .setLngLat(marker.geometry.coordinates)
        .addTo(map);

    elements = document.querySelectorAll('.mapbox--infra');
    if(map.getZoom() <= 15.5){
        for(i in elements){
            var el = elements[i];
            if(el.classList)
                el.classList.add('hidden');
        }
    }else{
        for(i in elements){
            var el = elements[i];
            if(el.classList)
                el.classList.remove('hidden');
        }
    }

    map.on('zoom', function(){
        elements = document.querySelectorAll('.mapbox--marker');
        if(map.getZoom() <= 15){
            for(i in elements){
                var el = elements[i];
                if(el.classList)
                    el.classList.add('small-view');
            }
        }else{
            for(i in elements){
                var el = elements[i];
                if(el.classList)
                    el.classList.remove('small-view');
            }
        }

        elements = document.querySelectorAll('.withModel');
        if(map.getZoom() >= 15.5){
            for(i in elements){
                var el = elements[i];
                if(el.classList)
                    el.classList.add('view3d');
            }
        }else{
            for(i in elements){
                var el = elements[i];
                if(el.classList)
                    el.classList.remove('view3d');
            }
        }

        elements = document.querySelectorAll('.mapbox--infra');
        if(map.getZoom() <= 15.5){
            for(i in elements){
                var el = elements[i];
                if(el.classList)
                    el.classList.add('hidden');
            }
        }else{
            for(i in elements){
                var el = elements[i];
                if(el.classList)
                    el.classList.remove('hidden');
            }
        }
    });
});