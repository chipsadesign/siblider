<head>
    <title><?=$APPLICATION->ShowTitle()?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico">

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta property="og:image" content="<?=SITE_TEMPLATE_PATH?>/images/Opengraph.png" />

    <?include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/includes/js.php");?>
    <?include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/includes/css.php");?>
    <?$APPLICATION->ShowHead();?>
    <?include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/includes/panel.php");?>
    <?include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/includes/metric.php");?>
</head>