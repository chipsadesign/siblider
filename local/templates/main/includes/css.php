<?
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/swiper/swiper.min.css");
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/owl.carousel/owl.carousel.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/daterangepicker/daterangepicker.min.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/fancybox/jquery.fancybox.min.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/bootstraptabs/bootstrap.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/nouislider/nouislider.min.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/select2/select2.min.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/normalize.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/animate.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/style.css");
?>
<style>
	.curtain {
	    position: fixed;
	    top: 0;
	    bottom: 0;
	    left: 0;
	    right: 0;
	    display: flex;
	    z-index: 11;
	}

	.curtain__left,
	.curtain__right {
	    width: 50%;
	    height: 100%;
	    transition: transform .3s cubic-bezier(0.55, 0.13, 0.46, 0.85);
	    background: #ffffff;
	    transform-origin: 50% 0;
	}

	.curtain .curtain__left {
	    transform: translate3d(0, -100%, 0);
	}

	.curtain .curtain__right {
	    transform: translate3d(0, 100%, 0);
	}

	.curtain.is-hidden-start .curtain__left {
	    transform: translate3d(0, -100%, 0);
	}

	.curtain.is-hidden-start .curtain__right {
	    transform: translate3d(0, 100%, 0);
	}

	.curtain.is-hidden-end .curtain__left {
	    transform: translate3d(0, 100%, 0);
	}

	.curtain.is-hidden-end .curtain__right {
	    transform: translate3d(0, -100%, 0);
	}

	.curtain .curtain__left,
	.curtain .curtain__right {
	    transform: translate3d(0, 0, 0);
	    transition-delay: .2s;
	}

	.curtain__line {
	    position: absolute;
	    top: 0;
	    left: 50%;
	    transform-origin: 50% 0;
	    transform: translate3d(-50%, -100%, 0);
	    width: 4px;
	    height: 100%;
	    background: #BA9077;
	}

	.curtain.is-hidden-start .curtain__line,
	.curtain.is-hidden-end .curtain__line {
		animation: none;
	}

	.curtain .curtain__line {
	    animation: curtainLineAnimation 1.5s .5s infinite;
	}

	.curtain.is-visible-on-scroll .curtain__line {
		opacity: 1;
	    animation: curtainLineAnimation .4s .5s 1;
	}

	@keyframes curtainLineAnimation {
	    0% {
	        transform: translate3d(-50%, -100%, 0);
	    }
	    20% {
	    	transform: translate3d(-50%, 0, 0);
	    }
	    50% {
	        transform: translate3d(-50%, 0, 0);
	    }
	    80% {
			transform: translate3d(-50%, 0, 0);
	    }
	    100% {
	        transform: translate3d(-50%, 100%, 0);
	    }
	}
</style>