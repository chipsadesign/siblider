<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.css' rel='stylesheet' />
<link href='<?=SITE_TEMPLATE_PATH?>/css/map.css' rel='stylesheet' />

<div class="modal modal--map" style="display: none;" id="modalMap">
    <div class="modal__wrap">
        <div class="modal__map" id="map"></div>
    </div>
</div>

<style>
    #map {
        width: 100%;
    }
</style>

<script>
    $('#fancyMap, .fancyMap').fancybox({
        afterShow: function(instance, slide){
            <?include $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/includes/map.js.php"?>
        },
        touch: false,
    });
</script>