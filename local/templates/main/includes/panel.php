<?if($USER->IsAuthorized()):?>
    <?if($USER->IsAdmin()):?>
        <div id="bx_panel_wrapper" class="<?=$_COOKIE["show_panel"] == "y" ? "vis" : ""?>">
            <?$APPLICATION->ShowPanel();?>
            <div id="bx_show">
                <span <?=$_COOKIE["show_panel"] == "y" ? "style='display: none;'" : ""?>>Показать панель</span>
                <span <?=$_COOKIE["show_panel"] != "y" ? "style='display: none;'" : ""?>>Скрыть</span>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('#bx_show').click(function(){
                    $(this).find('span').toggle();
                    $('#bx_panel_wrapper').toggleClass('vis');
                    if($('#bx_panel_wrapper').hasClass('vis')){
                        $.cookie('show_panel', 'y', {path: '/'});
                    }else{
                        $.removeCookie('show_panel', {path: '/'});
                    }
                });
            });
        </script>
        <style>
            #bx_panel_wrapper {
                bottom: 100%;
                top: auto;
                position: fixed;
                z-index: 9999;
                width: 100%;
                transition: 0.2s ease;
            }
            #bx_panel_wrapper.vis {
                transform: translateY(100%);
            }
            #bx_show {
                position: absolute;
                left: 50%;
                top: 100%;
                display: block;
                width: 120px;
                height: 25px;
                font: normal normal bold 12px/25px "Helvetica Neue",Helvetica,Arial,sans-serif!important;
                background: scroll transparent url(/bitrix/js/main/core/images/panel/top-panel-sprite-2.png) center -1940px !important;
                color: #dde7e9!important;
                text-align: center;
                cursor: pointer;
                transform: translateX(-50%);
                border-radius: 0 0 5px 5px;
            }
            #bx_show:hover {
                background-position: 0 -1980px !important;
            }
            #bx-panel.bx-panel-fixed {
                position: static !important;
            }
            #bx-panel-back {
                display: none !important;
            }
        </style>
    <?endif;?>
<?endif;?>