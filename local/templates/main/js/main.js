$(document).ready(function() {
    // Свитчер в карточке квартиры

    var $flatImg = $('.js-flat-img[data-img-for="flat"]');
    var $flatLinks = $('.js-flat__photo-link[data-img-for="flat"]');
    var $floorImg = $('.js-flat-img[data-img-for="floor"]');
    var $floorLinks = $('.js-flat__photo-link[data-img-for="floor"]');

    $('.js-flat-img-switcher').on('change', function() {
        var isChecked = !!$(this).prop('checked');

        if (isChecked) {
            $flatImg.addClass('flat-img--active');
            $flatLinks.removeClass('flat__photo-link--hidden');

            $floorImg.removeClass('flat-img--active');
            $floorLinks.addClass('flat__photo-link--hidden');
        } else {
            $floorImg.addClass('flat-img--active');
            $floorLinks.removeClass('flat__photo-link--hidden');

            $flatImg.removeClass('flat-img--active');
            $flatLinks.addClass('flat__photo-link--hidden');
        }
    });

    // Reveal elements on entering viewport. WOW.js баганный, если элемент скрыт во внутреннем диве - анимация не срабатывает
    if (window.IntersectionObserver) {
        var observer = new IntersectionObserver(function(entries) {
            entries.forEach(function(entry) {
                if (entry.isIntersecting) {
                    var $target = $(entry.target);
                    var animationName = $target.data('reveal-animation') || 'fadeInLeft';
                    var delay = $target.data('reveal-delay') || '0.2';

                    setTimeout(function() {
                        $target.addClass(animationName + ' ' + 'js-reveal--completed');
                    }, parseFloat(delay) * 1000);

                    observer.unobserve(entry.target);
                }
            });
        });

        $('.js-reveal').each(function() {
            observer.observe(this);
        });
    } else {
        $('.js-reveal').each(function() {
            var $target = $(this);
            var animationName = $target.data('reveal-animation') || 'fadeInLeft';
            $target.addClass(animationName + ' ' + 'js-reveal--completed');
        });
    }

    $('#backBtn').click(function(){
        window.history.back();
        return false;
    });

    new WOW({
        offset: 100,
    }).init();

	$('#menuOpenBtn, #mobileBurger').on('click', function() {
		$('#menu').removeClass('is-hidden');
		setTimeout(function(){
			$('#menuLeft').removeClass('animation-start');
			$('#menuRight').removeClass('animation-start');
		}, 50);
	});

	$('#menuCloseBtn').on('click', function() {
		$('#menuLeft').addClass('animation-start');
		$('#menuRight').addClass('animation-start');
		setTimeout(function(){
			$('#menu').addClass('is-hidden');
		}, 500);
	});

    //закрытие меню на esc
    $(window).keydown(function(event) {
        if (event.which == 27) {
            if (!$('#menu').hasClass('is-hidden')) {
                $('#menuCloseBtn').click();
            }
        }
    });

    setTimeout(function(){
        if (($('.page--main').length || $('.page--fixed').length) && $(window).width() > 1000) {
            $('body').addClass('overflow-hidden');
        }
    }, 20);

    $(window).resize(function(){
        if (($('.page--main').length || $('.page--fixed').length) && $(window).width() > 1000) {
            $('body').addClass('overflow-hidden');
        } else if (($('.page--main').length || $('.page--fixed').length) && $(window).width() <= 1000) {
            $('body').removeClass('overflow-hidden');
        }
    });

    $('input').blur(function() {
        if (this.value != '') {
            $(this).parents('label').addClass('is-filled');
            $(this).parents('label').removeClass('is-active');
        } else {
            $(this).parents('label').removeClass('is-filled');
            $(this).parents('label').removeClass('is-active');
        }
    });
    $('textarea').blur(function() {
        if (this.value != '') {
            $(this).parents('label').addClass('is-filled');
            $(this).parents('label').removeClass('is-active');
        } else {
            $(this).parents('label').removeClass('is-filled');
            $(this).parents('label').removeClass('is-active');
        }
    });

    $('.input--phone').keyup(function(){
        if($(this).val() == '+7 (8'){
            $(this).val('+7');
        }
    });
    $('.input--phone').mask('+7 (000) 000-00-00', {
        onKeyPress: function(val, e, field, options){
            console.log(val, e, field, options);
        },
    });

    $('input').focus(function() {
        $(this).parents('label').addClass('is-active');
    });

    $('textarea').focus(function() {
        $(this).parents('label').addClass('is-active');
    });

    $('.js-inputfile').on('change',function(){
      if($(this).get(0).files.length > 0){
        var filesSize = 0,
            maxSize = parseInt($(this).data('max-size'),10);
        for (var i = 0; i < $(this).get(0).files.length; i ++) {
            var fileSize = $(this).get(0).files[i].size;
            filesSize += fileSize;
        }

        if (maxSize >= filesSize) {
            $(this).prev().removeClass('is-unvalid');
            $(this).removeClass('is-unvalid');
        } else {
            $(this).prev().addClass('is-unvalid');
            $(this).addClass('is-unvalid');
        }
      }
    });

    $('.js-inputfile').each( function() {
        var input = $(this),
            label = input.parents('label'),
            labelVal = label.html();

        input.on( 'change', function(e) {
            var fileName = '';
            if (this.files && this.files.length > 1) {
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            }
            else if (e.target.value) {
                fileName = e.target.value.split( '\\' ).pop();
            }

            if (fileName) {
                label.find( 'div.js-file-label' ).html( fileName );
            }
            else {
                label.html( labelVal );
            }
        });

        // Firefox bug fix
        input
        .on( 'focus', function(){ input.addClass( 'has-focus' ); })
        .on( 'blur', function(){ input.removeClass( 'has-focus' ); });
    });

    $(document).on('click', '.js-button-add-more', function(e) {
        var self = $(this);
        self.addClass('is-loading');
        var target = self;
        if(self.parent().hasClass('js-button-add-more-parent')){
            target = self.parent();
        }

        var opt = {
            url: window.location.href,
            dataType: 'html',
            data: {
                PAGEN_1: self.data('page'),
                AJAX_REQUEST: 'Y'
            },
            successCallback: function(response){
                target.replaceWith(response);
            },
        };
        ajaxRequest(opt);

       return false;
    });

    $('.js-scroll-to-link').on('click', function(e){
        e.preventDefault();
        var id = $(this).attr('href'),
            offset = $(id).offset().top - $('.page-header').height();
        $('body, html').animate({scrollTop: offset}, 500);
    });

    $('#selectObject').change(function(){
        var id = $(this).val();
        var form = $('#filterForm');
        var list = $('#tableList');
        var building = $('#building');
        var map = $('.map-wrapper');
        var mapBtn = $('#fancyMap');
        var sort = [
            {name: 'PROPERTY_E_HOUSE', value: 'ASC'},
            {name: 'PROPERTY_N_PRICE', value: 'ASC'},
        ]
        var data = {
            house_id: id,
            action: 'flats',
            AJAX_REQUEST: 'Y',
            filter: form.data('file'),
            list: list.data('file'),
            building: building.data('file'),
            map: mapBtn.data('file'),
            flats_type: $('.js-flatType').val(),
            status_active: 'Y',
            SORT: sort
        };

        $('#tableContent').addClass('is-changed');

        var opt = {
            url: '/local/ajax/flats.php',
            dataType: 'json',
            data: data,
            successCallback: function(response){
                if(response['map'] == false){
                    mapBtn.hide();
                }else{
                    map.html(response['map']);
                    mapBtn.show();
                }

                form.html(response['filter']);
                list.html(response['list']);
                building.html(response['building']);
                $('#tableContent').removeClass('is-changed');
                $('.js-flatCount').text(response['count']);
                initFancyBox();

                if(window.initSortTables !== undefined){
                    window.initSortTables('[data-sort-ajax-table]');
                }
            },
        };
        ajaxRequest(opt);
    });

    $(document).on('change', '#filterForm :checkbox', function(){
        $('#filterForm').submit();
    });

    $('#filterForm').submit(function(){
        var id = $('#selectObject').val(),
            form = $(this),
            data = form.serializeArray(),
            list = $('#tableList'),
            building = $('#building'),
            opt = {
                url: '/local/ajax/flats.php',
                dataType: 'json',
                data: data,
                successCallback: function(response){
                    list.html(response['list']);
                    building.html(response['building']);
                    $('#tableContent').removeClass('is-changed');
                    $('.js-flatCount').text(response['count']);
                    initFancyBox();
                    if(window.initSortTables !== undefined){
                        window.initSortTables('[data-sort-ajax-table]');
                    }
                },
            };

        data.push({name: 'house_id', value: id});
        data.push({name: 'action', value: 'flats'});
        data.push({name: 'AJAX_REQUEST', value: 'Y'});
        data.push({name: 'filter', value: form.data('file')});
        data.push({name: 'list', value: list.data('file')});
        data.push({name: 'building', value: building.data('file')});

        $('#tableContent').addClass('is-changed');
        ajaxRequest(opt);

        return false;
    });
});

function checkField(input){
    checkFieldsError = 0;
    if ($(input).val() == ''){
        checkFieldsError = 1;
        $(input).addClass('is-unvalid');
    } else if (input.validity.patternMismatch) {
        checkFieldsError = 1;
        $(input).addClass('is-unvalid');
    } else {
        $(input).removeClass('is-unvalid');
    }
    return checkFieldsError;
}

function setPosition(container) {
	$(container).each(function(i){
		var imgObj = $(this),
            yPos = 0;
        yPos = imagesArray[i].offsetLeft - ($(window).scrollTop() + window.innerHeight) * imagesArray[i].speed;
        if ($(window).width() < 1000) {
            yPos = imagesArray[i].offsetLeft - ($(window).scrollTop() + window.innerHeight) * (imagesArray[i].speed / 2);
        }
		imgObj.css({ transform: 'translate3d(' + yPos + 'px, 0, 0)' });
	});
}
function initParallax() {
	getPosition('.js-parallax-img');
	setPosition('.js-parallax-img');
}

function getPosition(container) {
	imagesArray = [];
	$(container).each(function(i, elem){
        setWidth($(this));
		var offsetLeft = 0,
            offsetTop = 0,
            speed = setSpeed($(this)),
            box = $(this)[0].getBoundingClientRect(),
            offsetTop = box.top,
            offsetLeft = ($(window).scrollTop() + offsetTop) * speed;
        if ($(window).width() < 1000) {
            offsetLeft = ($(window).scrollTop() + offsetTop) * (speed / 2);
        }
		var object = {
			image: elem,
			offsetLeft: offsetLeft,
			offsetTop: offsetTop,
			speed: speed
		};
		imagesArray.push(object);
	});
}

function setWidth(elem) {
    var parentWidth = elem.parent().outerWidth(),
        windowHeight = $(window).height();
    if($(window).width() <= 1920){
        elem.css('width', parentWidth + (windowHeight / 5));
    }else{
        elem.css('width', parentWidth + (windowHeight / 2));
    }
}

function setSpeed(elem) {
	var parentWidth = elem.parent().outerWidth(),
		thisWidth = elem.outerWidth(),
		speed = (thisWidth - parentWidth) / (parentWidth * elem.data('speed') * 0.9) * elem.data('direction');
    if ($(window).width() <= 1280 && $(window).width() > 1000) {
        speed = (thisWidth - parentWidth) / (parentWidth * elem.data('speed') * 1.5) * elem.data('direction');
    }
	return speed;
}

function filterChooseFlat() {
	//before
	$('#tableContent').addClass('is-changed');

	//TODO: загрузка контента

	//after
    //setTimeout для демонстрации загрузки
	setTimeout(function(){
	    $('#tableContent').removeClass('is-changed');
	}, 2000);
}

function filterObject() {
	$('#objectFlats').addClass('is-changed');
    var form = $('#filterForm_detail');
    var data = form.serializeArray();

    var target = $('.js-flatsList');
    var opt = {
        url: '/local/ajax/flats.php',
        dataType: 'json',
        data: data,
        successCallback: function(response){
            target.html(response.flats);
            $('.choose-flat__counter span').text(response.items.length);
            $('.js-flatH1').html('Цены от ' + response.price.min);
            $('#objectFlats').removeClass('is-changed');
            initFancyBox();
        },
    };

    ajaxRequest(opt);
}

function filterMortgage() {
    var cost = parseInt(document.getElementById('rangeCost').noUiSlider.get().replace(/\s/g,'')),
        contribution = parseInt(document.getElementById('rangeContribution').noUiSlider.get().replace(/\s/g,'')),
        time = document.getElementById('rangeTime').noUiSlider.get(),
        age = document.getElementById('rangeAge').noUiSlider.get(),
        credit = cost - contribution;

    $('#creditAmount').text(number_format(credit, 0, '.', ' '));
    $('#mortgageContainer').addClass('is-changed');

    var target = $('#mortgageList');
    var data = {
        AJAX_REQUEST: 'Y',
        CREDIT: credit,
        TYPE: $('#selectLendingType').val(),
        AGE: age,
        TIME: time
    };
    var opt = {
        url: window.location.href,
        dataType: 'html',
        data: data,
        successCallback: function(response){
            target.html(response);
            $('#mortgageContainer').removeClass('is-changed');
        },
    };
    ajaxRequest(opt);
}

function initSelect2(container, parent) {
    if (parent == undefined) {
        parent = $('body');
    }
    container.each(function(){
        var placeholder = '',
            isTags = false,
            tagsClass = '';

        if($(this).attr('data-placeholder') !== undefined){
            placeholder = $(this).attr('data-placeholder');
        }
        if($(this).attr('data-tags') !== undefined && $(this).attr('data-tags') == "true"){
            isTags = $(this).attr('data-tags');
            tagsClass = 'is-tags';
        }
        $(this).select2({
            minimumResultsForSearch: -1,
            allowClear: false,
            tags: isTags,
            placeholder: placeholder,
            disabled: false,
            dropdownParent: parent,
            containerCssClass: 'custom-select__select',
            dropdownCssClass: 'custom-select__dropdown ' + tagsClass,
        });
    });
}

function initFancyBox(){
    $('[data-fancybox]').fancybox({
        overlayShow: false,
        arrows: false,
        infobar: false,
        keyboard: true,
        touch:false,
        toolbar: false,
        animationEffect: 'fade',
        animationDuration: 1000,
        transitionDuration: 1000,
        baseClass: 'fancyBox-base',
        slideClass: 'fancyBox-slide',
        beforeClose: function( instance, current ) {
            $('#successMessage').hide();
        },
        afterClose: function( instance, current ){
            $(current.src).find('.modal__title').show();
        }
    });
    $('[data-fancybox="image"], [data-fancybox="video"], [data-fancybox="floorPlan"]').fancybox({
        overlayShow: false,
        arrows: false,
        infobar: false,
        keyboard: true,
        touch:false,
        toolbar: true,
        animationEffect: "fade",
        animationDuration: 1000,
        transitionDuration: 1000,
        baseClass: 'fancyBox-base  fancyBox-base--fixed',
        slideClass: 'fancyBox-slide',
        buttons: [
            "zoom",
            "fullScreen",
            "close"
        ]
    });
    $('[data-fancybox="map"], [data-fancybox="filter"]').fancybox({
        overlayShow: false,
        arrows: false,
        infobar: false,
        keyboard: true,
        touch:false,
        toolbar: false,
        animationEffect: "fade",
        animationDuration: 1000,
        transitionDuration: 1000,
        baseClass: 'fancyBox-base fancyBox-base--fixed',
        slideClass: 'fancyBox-slide'
    });
};
initFancyBox();

function initNoUiSliderTwoPoint(container) {
    noUiSlider.create(container, {
        start: [parseInt(container.getAttribute('data-min')), parseInt(container.getAttribute('data-max'))],
        connect: false,
        step: parseInt(container.getAttribute('data-step')),
        range: {
            'min': parseInt(container.getAttribute('data-min')),
            'max': parseInt(container.getAttribute('data-max'))
        },
        format: wNumb({
            decimals: 0,
            thousand: ' '
        })
    });
}

function initNoUiSliderOnePoint(container) {
    noUiSlider.create(container, {
        start: [parseInt(container.getAttribute('data-start'))],
        connect: false,
        step: parseInt(container.getAttribute('data-step')),
        range: {
            'min': parseInt(container.getAttribute('data-min')),
            'max': parseInt(container.getAttribute('data-max'))
        },
        format: wNumb({
            decimals: 0,
            thousand: ' '
        })
    });
}

function getYearSurfix(year){
	var mod = year % 10;
	if (year > 10 && year < 15) {return '  лет'}
	if (mod == 1) {return '  год'}
	if (mod > 1 && mod < 5) {return '  года'}
	if (mod >= 5 && mod <= 9 || mod == 0) {return '  лет'}
}

function ajaxRequest(opt){
    opt.dataType = opt.dataType ? opt.dataType : 'json';
    opt.method = opt.method ? opt.method : 'post';
    opt.success = opt.successCallback ? opt.successCallback : function(response){console.log(response)};
    opt.error = function(response){console.error(response);};
    $.ajax(opt);
};

function number_format(number, decimals, dec_point, thousands_sep){
    var i, j, kw, kd, km;

    if( isNaN(decimals = Math.abs(decimals)) ){
        decimals = 2;
    }
    if( dec_point == undefined ){
        dec_point = ",";
    }
    if( thousands_sep == undefined ){
        thousands_sep = ".";
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if( (j = i.length) > 3 ){
        j = j % 3;
    } else{
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");

    return km + kw + kd;
}



;(function(){
    var SortTable = function(table){
        var API = {},
            setting = {};

        if(table == null || table == undefined){return}

        function _checkOption(option, defaultValue){
            if(defaultValue == undefined){
                defaultValue = null;
            };
            return option ? option : defaultValue;
        };

        function init(){
            setting.valueAttribute = 'data-sort-value';
            setting.buttonAttribute = 'data-sort-button';
            setting.sectionSelector = '[data-sort-section]';
            setting.buttonSelector = '[' + setting.buttonAttribute + ']';
            setting.rowSelector = '[data-sort-row]';

            setting.sortDirection = 1;
            if(table.getAttribute('data-sort-direction') !== null){
                setting.sortDirection *= table.getAttribute('data-sort-direction');
            }

            setting.tableSections = table.querySelectorAll(setting.sectionSelector);
            setting.tableButtons = table.querySelectorAll(setting.buttonSelector);

            setEvents();
            setButtonState();
        };

        function update(){
            setting.tableSections = table.querySelectorAll(setting.sectionSelector);
            setting.tableButtons = table.querySelectorAll(setting.buttonSelector);

            setting.sortDirection = 1;
        }

        function getSortedRows(section, sortParam){
            var rows = Array.from(section.querySelectorAll(setting.rowSelector)),
                sortedRows,
                cellSelector = '[data-sort-' + sortParam + ']'


            sortedRows = rows.sort(function(a, b){
                var aValue, bValue;

                aValue = a.querySelector(cellSelector).getAttribute(setting.valueAttribute);
                bValue = b.querySelector(cellSelector).getAttribute(setting.valueAttribute);

                return (aValue - bValue) * setting.sortDirection;
            });
            return sortedRows;
        };

        function sortTable(sortParam){
            for (var i = 0; i < setting.tableSections.length; i++) {
                var sortedRows = getSortedRows(setting.tableSections[i], sortParam);

                setting.tableSections[i].innerHTML = '';
                setting.tableSections[i].append(...sortedRows);
            }

        };

        function setButtonState(){
            if(setting.sortDirection < 0) {
                setting.tableButtons[0].classList.add('is-reverse')
            }
            else {
                setting.tableButtons[0].classList.remove('is-reverse')
            }
        }

        function onButtonClick(event){
            event.preventDefault();
            var sortParam = this.getAttribute(setting.buttonAttribute);

            sortTable(sortParam);
            setting.sortDirection *= -1;

            setButtonState();
        };

        function setEvents(){
            for (var i = 0; i < setting.tableButtons.length; i++) {
                setting.tableButtons[i].addEventListener('click', onButtonClick);
            }
        };

        init();

        API.update = update;
        return API;
    }


    function initSortTables(selector){
        var tables,
            tableSelector = '[data-sort-table]';

        if(selector !== undefined && typeof selector === 'string'){
            tableSelector = selector;
        }

        tables = document.querySelectorAll(tableSelector);

        for (var i = 0; i < tables.length; i++) {
            var sortTable = new SortTable(tables[i]);
            tables[i].instance = sortTable;
        }

    }
    initSortTables();

    window.SortTable = SortTable;
    window.initSortTables = initSortTables;
})();


(function(){
    var flatContainers = document.querySelectorAll('.js-flat-slider');

    flatContainers.forEach(function(flatContainer){
        var flatSldier = new Swiper(flatContainer, {
            slidesPerView: 3,
            breakpoints: {
                960: {
                    slidesPerView: 2,
                },
                620: {
                    slidesPerView: 1,
                }
            },
            navigation: {
                nextEl: flatContainer.querySelector('.js-slide-next'),
                prevEl: flatContainer.querySelector('.js-slide-prev'),
            },
            pagination: {
                el: flatContainer.querySelector('.js-slide-dots'),
                type: 'bullets',
                clickable: true,
                bulletClass: 'dots__item',
                bulletActiveClass: 'is-active',
                renderBullet: function(index, className){
                    return '<div class="' + className + '"></div>'
                }
            },
        });
    });
})();

document.addEventListener('DOMContentLoaded', function(event){
    var sliderContainers = document.querySelectorAll('.js-build-gallery');

    initBuildSlider(sliderContainers);

    function initBuildSlider(sliderContainers){
        sliderContainers.forEach(function(sliderContainer){
            var currentSlideIndex = 0,
                isDragged = false,
                currentBullet = 0,
                bulletStatus = 0,
                slider,
                sliderWrap = $(sliderContainer).find('.slider__wrap'),
                sliderCurtain = $(sliderContainer).find('.js-slider-curtain'),
                sliderDots = $(sliderContainer).find('.js-build-gallery-dots');

            slider = new Swiper(sliderContainer, {
                speed: 1100,
                effect: 'fade',
                fadeEffect: {
                    crossFade: true
                },
                loop: true,
                longSwipes: false,
                longSwipesMs: '100ms',
                pagination: {
                    el: sliderDots,
                    type: 'bullets',
                    bulletClass: 'slider__bullet',
                    renderBullet: function (index, className) {
                        var bulletTitle = this.slides[index + this.loopedSlides].getAttribute('data-title');
                        if(bulletTitle === null){
                            bulletTitle = '';
                        }
                        return '<span class="' + className + '">' + bulletTitle + '</span>';
                    },
                    bulletActiveClass: 'is-active',
                    clickable: true
                },
                on: {
                    init: function() {
                        var self = this;

                        $(sliderContainer).find('.js-slider-prev').on('click', function(){
                            if (!isDragged) {
                                sliderWrap.addClass('animateCurtain-to-right');
                                self.slidePrev(1100);
                                setTimeout(function(){
                                    sliderWrap.addClass('animateCurtain-hide-to-right');
                                    setTimeout(function(){
                                        sliderWrap.removeClass('animateCurtain-hide-to-right').removeClass('animateCurtain-to-right');
                                        self.allowSlideNext = true;
                                        self.allowSlidePrev = true;
                                        self.allowTouchMove = true;
                                        isDragged = false;
                                    }, 500);
                                }, 600);
                            }
                            if (self != undefined) {
                                self.allowSlideNext = false;
                                self.allowSlidePrev = false;
                                self.allowTouchMove = false;
                            }
                        });

                        $(sliderContainer).find('.js-slider-next').on('click', function(){
                            if (!isDragged) {
                                sliderWrap.addClass('animateCurtain-to-left');
                                self.slideNext(1100);
                                setTimeout(function(){
                                    sliderWrap.addClass('animateCurtain-hide-to-left');
                                    setTimeout(function(){
                                        sliderWrap.removeClass('animateCurtain-hide-to-left').removeClass('animateCurtain-to-left');
                                        self.allowSlideNext = true;
                                        self.allowSlidePrev = true;
                                        self.allowTouchMove = true;
                                        isDragged = false;
                                    }, 500);
                                }, 600);
                            }
                            if (self != undefined) {
                                self.allowSlideNext = false;
                                self.allowSlidePrev = false;
                                self.allowTouchMove = false;
                            }
                        });
                    },
                    paginationRender: function() {
                        var self = this;
                        $(sliderContainer).find('.slider__bullet').on('click', function(){
                            if (currentBullet < $(this).index()) {
                                if (!isDragged) {
                                    sliderWrap.addClass('animateCurtain-to-left');
                                    setTimeout(function(){
                                        sliderWrap.addClass('animateCurtain-hide-to-left');
                                        setTimeout(function(){
                                            sliderWrap.removeClass('animateCurtain-hide-to-left').removeClass('animateCurtain-to-left');
                                            self.allowSlideNext = true;
                                            self.allowSlidePrev = true;
                                            self.allowTouchMove = true;
                                            isDragged = false;
                                        }, 500);
                                    }, 600);
                                }
                                bulletStatus = 1;
                            } else {
                                if (!isDragged) {
                                    sliderWrap.addClass('animateCurtain-to-right');
                                    setTimeout(function(){
                                        sliderWrap.addClass('animateCurtain-hide-to-right');
                                        setTimeout(function(){
                                            sliderWrap.removeClass('animateCurtain-hide-to-right').removeClass('animateCurtain-to-right');
                                            self.allowSlideNext = true;
                                            self.allowSlidePrev = true;
                                            self.allowTouchMove = true;
                                            isDragged = false;
                                        }, 500);
                                    }, 600);
                                }
                                bulletStatus = 2;
                            }

                            currentBullet = $(this).index();

                        });
                    },
                    touchStart: function(touchstart){
                        isDragged = true;
                    },
                    touchMove: function(touchMove) {
                        var self = this,
                            nextCurtainXOffset,
                            curtainWidth = sliderCurtain.width() / 2;

                        if (self.touches.diff > 0) {
                            nextCurtainXOffset = curtainWidth - self.touches.diff;
                        }
                        else {
                            nextCurtainXOffset = -curtainWidth - self.touches.diff;
                        }

                        sliderCurtain.css('transform', 'translate3d(' + nextCurtainXOffset + 'px, 0, 0)');
                    },
                    touchEnd: function(touchEnd){
                        var self = this;

                        if (self.touches.diff > 0) {
                            sliderCurtain.css('transform', 'translate3d(-100%, 0, 0)').css('transition', 'transform 1s cubic-bezier(.55, .13, .46, .85)');
                        }
                        else if (self.touches.diff < 0) {
                            sliderCurtain.css('transform', 'translate3d(100%, 0, 0)').css('transition', 'transform 1s cubic-bezier(.55, .13, .46, .85)');
                        }
                        else {
                            isDragged = false;
                        }

                        setTimeout(function(){
                            sliderCurtain.css('transition', 'unset');
                            isDragged = false;
                        }, 1000);
                    },
                    slideChange: function(e){
                        var offset = 0,
                            activeBullet,
                            activeBulletLeft,
                            sliderDotsScrollLeft;

                        activeBullet = sliderDots.find('.is-active');

                        if(!activeBullet.length){return}

                        activeBulletLeft = activeBullet.position().left;
                        sliderDotsScrollLeft = sliderDots.scrollLeft();

                        if (activeBullet.length) {
                            if (bulletStatus == 1) {
                                offset = sliderDotsScrollLeft + activeBulletLeft;
                            }
                            else if (bulletStatus == 2) {
                                offset = sliderDotsScrollLeft + activeBulletLeft - sliderDots.width() + activeBullet.outerWidth();
                            }
                            else {
                                offset = sliderDotsScrollLeft + activeBulletLeft;
                            }
                            currentBullet = activeBullet.index();
                        }
                        sliderDots.animate({scrollLeft: offset}, 500);
                    },
                }
            });

        });
    }
});
