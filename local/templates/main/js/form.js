$(document).on('submit', '.js-form', function(e){
    e.preventDefault();
    var formIsValid = 0,
        requiredInForm = 0,
        form = $(this),
        parentModal = $(this).parents('.modal'),
        filesIsValid = true;

    var yaTarget = form.data('ya-target');

    form.find('.required').each(function(){
        requiredInForm ++;

        //перебираем инпуты
        if($(this)[0].localName == 'input'){
            //отдельно проверяме чекбоксы
            if($(this).attr('type') == 'checkbox'){
                if($(this).is(":checked")){
                    formIsValid ++;
                    $(this).siblings('.checkbox-custom').removeClass('is-unvalid');
                }else{
                    $(this).siblings('.checkbox-custom').addClass('is-unvalid');
                }
            //остальные инпуты
            }else if($(this).attr('type') == 'text' || $(this).attr('type') == 'number' || $(this).attr('type') == 'email'){
                if(!checkField(this)){
                    formIsValid ++;
                }
            }
        //select
        }else if($(this)[0].localName == 'select' || $(this)[0].localName == 'textarea'){
            if($(this).val() == ''){
                $(this).addClass('is-unvalid');
            }else{
                $(this).removeClass('is-unvalid');
                formIsValid ++;
            }
        }
    });

    form.find('input[type="file"]').each(function(){
        if($(this).hasClass('is-unvalid')){
            filesIsValid = false;
        }
    });

    if(formIsValid == requiredInForm && filesIsValid){
        if(form.data('before') !== undefined){
            var fn = form.data('before');
            eval(fn);
        }

        $('#curtain').show();
        $('#curtain').removeClass('is-hidden-start');

        if(form.data('type') !== undefined && form.data('type') == 'files'){
            var data = new FormData(this);
            data.append('page_title', document.title);
            data.append('page_url', window.location);
            $('input:radio:checked').each(function( index ) {
                var thisRadio = $(this);
                data.set(
                    thisRadio.attr('name'),
                    thisRadio.closest('.custom-radio').find('.custom-radio__button').data('content')
                );
            });
        }else{
            var data = form.serializeArray();
            data[data.length] = {name: 'page_title', value: document.title};
            data[data.length] = {name: 'page_url', value: window.location};
        }
        var opt = {
            url: '/local/ajax/form.php',
            dataType: 'html',
            data: data,
            method: 'post',
            successCallback: function(response){
                form[0].reset();
                form.find('.is-filled').each(function(){
                    $(this).removeClass('is-filled');
                });
                form.find('.js-select').each(function(){
                    $(this).val(null).trigger('change').removeClass('is-filled');
                });
                parentModal.find('.modal__title').hide();
                $('#successMessage').show();
                $('#curtain').addClass('is-hidden-end');

                if(yaTarget != undefined){
                    yaCounter20863021.reachGoal(yaTarget);
                }
                setTimeout(function(){
                    $('#curtain').hide();
                    $('#curtain').removeClass('is-hidden-end');
                    $('#curtain').addClass('is-hidden-start');
                    if (!form.hasClass('modal__form')){
                        setTimeout(function(){
                            $('#successMessage').fadeOut(300);
                        }, 1000);
                    }
                }, 700);
            },
        };
        if(form.data('type') !== undefined && form.data('type') == 'files'){
            opt['cache'] = false;
            opt['contentType'] = false;
            opt['processData'] = false;
        }
        ajaxRequest(opt);
    }
});

function checkCalc(){
    var cost = parseInt(document.getElementById('rangeCost').noUiSlider.get().replace(/\s/g,'')),
        contribution = parseInt(document.getElementById('rangeContribution').noUiSlider.get().replace(/\s/g,'')),
        time = document.getElementById('rangeTime').noUiSlider.get(),
        age = document.getElementById('rangeAge').noUiSlider.get(),
        credit = cost - contribution;

    $('.js-formAge').val(age);
    $('.js-formTime').val(time);
    $('.js-formCredit').val(credit);
}