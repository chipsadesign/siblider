<?foreach($arResult as $arItem):?>
	<li>
		<a class="hover-underline" href="<?=$arItem["LINK"]?>">
			<?=$arItem["TEXT"]?>
			<?if($arItem["CNT"]):?>
				<span class="page-header__menu-counter"><?=$arItem["CNT"]?></span>
			<?endif;?>
		</a>
	</li>
<?endforeach;?>