<div class="page__top-slide" id="topSlide" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/tradein.png');">
    <div class="page__top-slide-content">
        <h1 class="big wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".2s">Trade-in. сервис</h1>
        <div class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                ".default",
                array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_TEMPLATE_PATH."/includes/pages/tradein/header.php",
                    "EDIT_TEMPLATE" => ""
                ),
                false
            );?>
        </div>
    </div>
</div>
<div class="page__main-content  tradein" id="tradein">
    <?$APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "tradein",
        [
            "IBLOCK_ID" => 21,
            "PROPERTY_CODE" => [
                "F_ICO"
            ],
            "SET_TITLE" => "N"
        ],
        false
    );?>

    <div class="tradein__form">
        <h2 class="h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">УЗНАЙТЕ РЕАЛЬНУЮ СТОИМОСТЬ ВАШЕЙ КВАРТИРЫ, И ПОЛУЧИТЕ ПЕРСОНАЛЬНОЕ ПРЕДЛОЖЕНИЕ ОТ КОМПАНИИ СИБЛИДЕР.</h2>
        <div class="small wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">Подробно заполните анкету на TRADE IN, чтобы оценка была точной. Если вы не помните тип вашего дома или материал постройки – проверьте кадастровый или технический паспорт, там всё есть.<br>Это бесплатно, и не обязывает вас к сделке. Можете заполнить её просто ради интереса.</div>
        <form method="post" class="form wow fadeIn js-form" data-wow-duration=".5s" data-wow-delay=".2s" id="tradeinForm" enctype="multipart/form-data" data-type="files" data-ya-target="order_done">
            <?=bitrix_sessid_post();?>
            <input type="hidden" name="action" value="TRADE_IN">
            <div class="form__row">
                <label class="input-label">
                    <span>* Имя</span>
                    <input type="text" name="FIELDS[NAME]" class="required">
                </label>
                <label class="input-label">
                    <span>* Телефон</span>
                    <input type="text" name="FIELDS[PHONE]" class="required">
                </label>
            </div>
            <div class="form__row">
                <label class="input-label">
                    <span>* Город</span>
                    <input type="text" name="FIELDS[CITY]" class="required">
                </label>
                <label class="input-label">
                    <span>* Улица, номер дома</span>
                    <input type="text" name="FIELDS[STREET]" class="required">
                </label>
            </div>
            <div class="form__row">
                <div class="form__item">
                    <div class="custom-select-wrap" id="selectBuildingTypeParent">
                        <select class="custom-select js-select required" id="selectBuildingType" name="FIELDS[FLAT_TYPE]">
                            <option value=""></option>
                            <option value="хрущевка">хрущевка</option>
                            <option value="сталинка">сталинка</option>
                            <option value="ленинградка">ленинградка</option>
                            <option value="индивидульный проект">индивидульный проект</option>
                            <option value="новостройка">новостройка</option>
                            <option value="другое">другое</option>
                        </select>
                        <script>
                            (function(){
                                document.addEventListener('DOMContentLoaded', function(event){
                                    initSelect2($('#selectBuildingType'), $('#selectBuildingTypeParent'));
                                    $('#selectBuildingType').on('select2:select', function(e) {
                                        $(this).addClass('is-filled');
                                    });
                                });
                            })()
                        </script>
                        <div class="custom-select-label">* Тип дома</div>
                    </div>
                </div>
                <div class="form__item  form__item--auto-width">
                    <div class="form__item-title">Количество комнат</div>
                    <label class="custom-radio">
                        <input type="radio" checked="checked" name="FIELDS[ROOMS]">
                        <span class="custom-radio__button" data-hoverContent="1" data-content="1"></span>
                    </label>
                    <label class="custom-radio">
                        <input type="radio" name="FIELDS[ROOMS]">
                        <span class="custom-radio__button" data-hoverContent="2" data-content="2"></span>
                    </label>
                    <label class="custom-radio">
                        <input type="radio" name="FIELDS[ROOMS]">
                        <span class="custom-radio__button" data-hoverContent="3" data-content="3"></span>
                    </label>
                    <label class="custom-radio">
                        <input type="radio" name="FIELDS[ROOMS]">
                        <span class="custom-radio__button" data-hoverContent="4" data-content="4"></span>
                    </label>
                </div>
            </div>
            <div class="form__row">
                <label class="input-label">
                    <span>* Площадь, м<sup>2</sup></span>
                    <input type="number" name="FIELDS[AREA]" class="required">
                </label>
                <label class="input-label">
                    <span>* Этаж</span>
                    <input type="number" name="FIELDS[FLOOR]" class="required">
                </label>
            </div>
            <div class="form__row">
                <div class="form__item">
                    <div class="custom-select-wrap" id="selectBuildingMaterialParent">
                        <select class="custom-select js-select required" id="selectBuildingMaterial" name="FIELDS[MATERIAL]">
                            <option value=""></option>
                            <option value="кирпич">кирпич</option>
                            <option value="панель">панель</option>
                            <option value="кирпич-монолит">кирпич-монолит</option>
                            <option value="Монолит">Монолит</option>
                            <option value="другое">другое</option>
                        </select>
                        <script>
                            (function(){
                                document.addEventListener('DOMContentLoaded', function(event){
                                    initSelect2($('#selectBuildingMaterial'), $('#selectBuildingMaterialParent'));
                                    $('#selectBuildingMaterial').on('select2:select', function(e) {
                                        $(this).addClass('is-filled');
                                    });
                                });
                            })()
                        </script>
                        <div class="custom-select-label">* Материал дома</div>
                    </div>
                </div>
                <div class="form__item">
                    <div class="custom-select-wrap" id="selectRepairsParent">
                        <select class="custom-select js-select required" id="selectRepairs" name="FIELDS[CONDITION]">
                            <option value=""></option>
                            <option value="хорошее">хорошее</option>
                            <option value="дизайнерский ремонт">дизайнерский ремонт</option>
                            <option value="требует косметического ремонта">требует косметического ремонта</option>
                            <option value="требует капитального ремонта">требует капитального ремонта</option>
                        </select>
                        <script>
                            (function(){
                                document.addEventListener('DOMContentLoaded', function(event){
                                    initSelect2($('#selectRepairs'), $('#selectRepairsParent'));
                                    $('#selectRepairs').on('select2:select', function(e) {
                                        $(this).addClass('is-filled');
                                    });
                                });
                            })()
                        </script>
                        <div class="custom-select-label">* Состояние ремонта</div>
                    </div>
                </div>
            </div>
            <div class="form__row">
                <div class="form__item">
                    <div class="custom-select-wrap" id="selectObjectParent">
                        <select class="custom-select js-select required" id="selectObject_jk" name="FIELDS[PROJECT]">
                            <option value=""></option>
                            <?foreach($arResult["PROJECTS"] as $arProject):?>
                                <option value="<?=$arProject["NAME"]?>"><?=$arProject["NAME"]?></option>
                            <?endforeach;?>
                        </select>
                        <script>
                            (function(){
                                document.addEventListener('DOMContentLoaded', function(event){
                                    initSelect2($('#selectObject_jk'), $('#selectObjectParent'));
                                    $('#selectObject_jk').on('select2:select', function(e) {
                                        $(this).addClass('is-filled');
                                    });
                                });
                            })()
                        </script>
                        <div class="custom-select-label">* Новый жилой комплекс</div>
                    </div>
                </div>
                <div class="form__item">
                    <div class="custom-select-wrap" id="selectPayParent">
                        <select class="custom-select js-select required" id="selectPay" name="FIELDS[PAY]">
                            <option value=""></option>
                            <option value="наличный расчет">наличный расчет</option>
                            <option value="ипотека">ипотека</option>
                            <option value="рассрочка от застройщика">рассрочка от застройщика</option>
                            <option value="без доплаты">без доплаты</option>
                        </select>
                        <script>
                            (function(){
                                document.addEventListener('DOMContentLoaded', function(event){
                                    initSelect2($('#selectPay'), $('#selectPayParent'));
                                    $('#selectPay').on('select2:select', function(e) {
                                        $(this).addClass('is-filled');
                                    });
                                });
                            })()
                        </script>
                        <div class="custom-select-label">* Способ доплаты</div>
                    </div>
                </div>
            </div>
            <div class="form__row">
                <label class="input-label  input-label--file">
                    <div class="input-label__file js-file-label">Загрузить фотографии квартиры</div>
                    <input type="file" name="FIELDS[FILE_PHOTO]" multiple class="inputfile  js-inputfile" data-multiple-caption="выбрано: {count}" data-max-size="1000000">
                    <div class="input-label__file-error">Размер файлов не может превышать 10 Мб</div>
                </label>
                <label class="input-label  input-label--file">
                    <div class="input-label__file js-file-label">Загрузить документы</div>
                    <input type="file" name="FIELDS[FILE_DOCS]" multiple class="inputfile  js-inputfile" data-multiple-caption="выбрано: {count}" data-max-size="1000000">
                    <div class="input-label__file-error">Размер файлов не может превышать 10 Мб</div>
                </label>
            </div>
            <div class="form__row">
                <label class="input-label  input-label--checkbox">
                    <input type="checkbox" name="agreed" class="checkbox required">
                    <span class="checkbox-custom  checkbox-custom--mark"></span>
                    <span class="input-label__agreed-text">Согласен с условиями <a href="/personal-confirm/">Согласия</a> на обработку персональных данных</span>
                </label>
            </div>
            <div class="form__row">
                <button class="button button--center-black" type="submit" data-hoverContent="отправить заявку" data-content="отправить заявку"></button>
            </div>
        </form>
    </div>

    <?$APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "tradein.flats",
        [
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
            "SET_TITLE" => "N"
        ],
        false
    );?>

    <div class="h1 tradein__contacts wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            ".default",
            array(
                "COMPONENT_TEMPLATE" => ".default",
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_TEMPLATE_PATH."/includes/pages/tradein/knowhow.php",
                "EDIT_TEMPLATE" => ""
            ),
            false
        );?>
    </div>
    <div class="tradein__criteria">
        <div class="h1  wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">критерии оценки квартир</div>
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            ".default",
            array(
                "COMPONENT_TEMPLATE" => ".default",
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_TEMPLATE_PATH."/includes/pages/tradein/tradeinlist.php",
                "EDIT_TEMPLATE" => ""
            ),
            false
        );?>
    </div>
    <script>
        (function(){
            document.addEventListener('DOMContentLoaded', function(event){
                $('body').addClass('light');
            });
        })()
    </script>
    <div id="successMessage" class="success-message" style="display: none;">
        <div class="success-message__inner">
            <div class="h1">Ваша заявка успешно отправлена!</div>
            <div class="success-message__subtitle">Наши менеджеры перезвонят вам в течение рабочего дня.</div>
        </div>
    </div>
</div>