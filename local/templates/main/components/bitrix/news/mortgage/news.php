<?if(!isset($_REQUEST["AJAX_REQUEST"])):?>
	<?
		$arFilterDef = [
			"price" => 3500000,
			"first" => 1000000,
			"long" => 15,
			"age" => 30
		];
	?>
	<div class="page__top-slide" id="topSlide" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/calculator.png');">
		<div class="page__top-slide-content  page__top-slide-content--calculator">
			<div class="big wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".2s">Программы банков<br>на любой вкус</div>
			<div class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">Подавайте заявки онлайн и получите максимально выгодные предложения от банков</div>
		</div>
	</div>

	<div class="page__main-content  calculator" id="calculator">
	    <div class="calculator__left page__left-content  wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s" id="calcForm">
			<form class="form" id="calculatorForm">
				<div class="form__title" id="calculatorCallForm">Параметры кредитования <span class="calculator__form-btn icon-range"></span></div>
	            <div class="form__wrap" id="filterForm">
	            	<?/*
	                <div class="form__item js-form-item">
	                    <div class="form__item-title">вид расчета</div>
	                    <div class="custom-select-wrap" id="selectLendingTypeParent">
	                        <select class="custom-select js-select" id="selectLendingType" name="type">
	                            <option value="0">стандартный расчет</option>
	                            <option value="1">семейная ипотека</option>
	                        </select>
	                        <script>
	                            (function(){
	                                document.addEventListener('DOMContentLoaded', function(event){
	                                    $('#selectLendingType').select2({
	                                        minimumResultsForSearch: -1,
	                                        allowClear: false,
	                                        disabled: false,
	                                        containerCssClass: 'custom-select__select',
	                                        dropdownCssClass: 'custom-select__dropdown',
	                                    });

	                                    $('#selectLendingType').on('select2:select', function(e) {
	                                        $(this).addClass('is-filled');
	                                        $('#filterReset').show().addClass('is-visible');
	                                        filterMortgage();
	                                    });
	                                });
	                            })()
	                        </script>
	                    </div>
	                </div>
	                */?>
					<div class="form__item js-form-item">
	                    <div class="form__item-title">стоимость недвижимости</div>
	                    <div class="form__item-content">
	                        <div class="custom-range">
	                            <div class="custom-range__values">
	                                <div id="rangeCostLastValue">10</div>
	                            </div>
	                            <div id="rangeCost" data-step="100000" data-start="<?=$arFilterDef["price"]?>" data-min="1000000" data-max="10000000" class="js-range"></div>
	                        </div>
	                        <script>
	                            (function(){
	                                document.addEventListener('DOMContentLoaded', function(event){
	                                    var sliderCost = document.getElementById('rangeCost');
	                                    initNoUiSliderOnePoint(sliderCost);
	                                    var sliderValues = [document.getElementById('rangeCostLastValue')];

	                                    sliderCost.noUiSlider.on('update', function( values, handle ) {
	                                        sliderValues[handle].innerHTML = values[handle];
	                                    });
	                                    sliderCost.noUiSlider.on('change', function( values, handle ) {
	                                        // var amount = document.getElementById('rangeCost').noUiSlider.get().replace(/\s/g,'') - document.getElementById('rangeContribution').noUiSlider.get().replace(/\s/g,'');
	                                        var amount = parseInt(document.getElementById('rangeCost').noUiSlider.get().replace(/\s/g,''));
	                                        $('#filterReset').show().addClass('is-visible');
	                                        // $('#creditAmount').text(amount);

		                                    var sliderContribution = document.getElementById('rangeContribution');
		                                    var contribution = parseInt(sliderContribution.noUiSlider.get().replace(/\s/g,''));
		                                    if((amount - 500000) < contribution)
	                                        	sliderContribution.noUiSlider.set(amount - 500000);
	                                        sliderContribution.noUiSlider.updateOptions(
											    {
											    	range:{
														'min': 0,
														'max': amount - 500000,
													},
												},
												true
											);
	                                        filterMortgage();
	                                    });
	                                });
	                            })()
	                        </script>
	                    </div>
	                </div>
	                <div class="form__item js-form-item">
	                    <div class="form__item-title">первоначальный взнос</div>
	                    <div class="form__item-content">
	                        <div class="custom-range">
	                            <div class="custom-range__values">
	                                <div id="rangeContributionLastValue">10</div>
	                            </div>
	                            <div id="rangeContribution" data-step="10000" data-start="<?=$arFilterDef["first"]?>" data-min="0" data-max="1000000" class="js-range"></div>
	                        </div>
	                        <script>
	                            (function(){
	                                document.addEventListener('DOMContentLoaded', function(event){
	                                    var sliderContribution = document.getElementById('rangeContribution');
	                                    initNoUiSliderOnePoint(sliderContribution);
	                                    var sliderValues = [document.getElementById('rangeContributionLastValue')];

	                                    sliderContribution.noUiSlider.on('update', function( values, handle ) {
	                                        sliderValues[handle].innerHTML = values[handle];
	                                    });
	                                    sliderContribution.noUiSlider.on('change', function( values, handle ) {
	                                        var amount = document.getElementById('rangeCost').noUiSlider.get().replace(/\s/g,'') - document.getElementById('rangeContribution').noUiSlider.get().replace(/\s/g,'');
	                                        // $('#creditAmount').text(amount);
	                                        $('#filterReset').show().addClass('is-visible');
	                                        filterMortgage();
	                                    });
	                                });
	                            })()
	                        </script>
	                    </div>
	                </div>
	                <div class="form__item js-form-item">
	                    <div class="form__item-title">срок</div>
	                    <div class="form__item-content">
	                        <div class="custom-range">
	                            <div class="custom-range__values">
	                                <div id="rangeTimeLastValue">10</div>
	                            </div>
	                            <div id="rangeTime" data-step="1" data-start="<?=$arFilterDef["long"]?>" data-min="1" data-max="30" class="js-range"></div>
	                        </div>
	                        <script>
	                            (function(){
	                                document.addEventListener('DOMContentLoaded', function(event){
	                                    var sliderTime = document.getElementById('rangeTime');
	                                    initNoUiSliderOnePoint(sliderTime);
	                                    var sliderValues = [document.getElementById('rangeTimeLastValue')];

	                                    sliderTime.noUiSlider.on('update', function( values, handle ) {
	                                        sliderValues[handle].innerHTML = values[handle] + getYearSurfix(values[handle]);
	                                    });
	                                    sliderTime.noUiSlider.on('change', function( values, handle ) {
	                                        $('#filterReset').show().addClass('is-visible');
	                                        filterMortgage();
	                                    });
	                                });
	                            })()
	                        </script>
	                    </div>
	                </div>
	                <div class="form__item js-form-item">
	                    <div class="form__item-title">возраст</div>
	                    <div class="form__item-content">
	                        <div class="custom-range">
	                            <div class="custom-range__values">
	                                <div id="rangeAgeLastValue">10</div>
	                            </div>
	                            <div id="rangeAge" data-step="1" data-start="<?=$arFilterDef["age"]?>" data-min="18" data-max="65" class="js-range"></div>
	                        </div>
	                        <script>
	                            (function(){
	                                document.addEventListener('DOMContentLoaded', function(event){
	                                    var sliderAge = document.getElementById('rangeAge');
	                                    initNoUiSliderOnePoint(sliderAge);
	                                    var sliderValues = [document.getElementById('rangeAgeLastValue')];

	                                    sliderAge.noUiSlider.on('update', function( values, handle ) {
	                                        sliderValues[handle].innerHTML = values[handle] + getYearSurfix(values[handle]);
	                                    });
	                                    sliderAge.noUiSlider.on('change', function( values, handle ) {
	                                        $('#filterReset').show().addClass('is-visible');
	                                        filterMortgage();
	                                    });
	                                });
	                            })()
	                        </script>
	                    </div>
	                </div>
	                <div class="form__item form__item--flex js-form-item">
	                	<div class="calculator__amount-tltle">сумма кредита</div>
	                	<div class="calculator__amount h1"><span id="creditAmount"></span> ₽</div>
	                </div>
	                <div class="form__buttons js-form-item">
	                    <button class="form__reset button button--light" type="reset" id="filterReset"><span>сбросить</span><span></span></button>
	                    <a href="javascript:;" data-fancybox data-src="#personalOffer" class="form__submit button button--center-black" data-hoverContent="получить персональный расчет" data-content="получить персональный расчет"></a>
	                </div>
	                <script>
	                    (function(){
	                        document.addEventListener('DOMContentLoaded', function(event){
	                            var amount = document.getElementById('rangeCost').noUiSlider.get().replace(/\s/g,'') - document.getElementById('rangeContribution').noUiSlider.get().replace(/\s/g,'');
	                            // $('#creditAmount').text(amount);
	                            $('#filterReset').on('click', function(e) {
	                                e.preventDefault();
	                                //сброс чекбоксов
	                                $('#filterForm input[type="checkbox"]').each(function(){
	                                    $(this).prop( "checked", false );
	                                });
	                                //сброс бегунков
	                                $('#filterForm .js-range').each(function(){
	                                    $(this)[0].noUiSlider.reset();
	                                });
	                                var amount = document.getElementById('rangeCost').noUiSlider.get().replace(/\s/g,'') - document.getElementById('rangeContribution').noUiSlider.get().replace(/\s/g,'');
	                                // $('#creditAmount').text(amount);
	                                $('#filterReset').hide().removeClass('is-visible');
	                                filterMortgage();
	                            });
	                            $('#calculatorCallForm').on('click', function(){
                                    var filterForm = $('#filterForm');
                                    if (!filterForm.hasClass('is-visible')) {
                                        filterForm.addClass('is-visible');
                                        setTimeout(function(){
                                            filterForm.find('.js-form-item').each(function(){$(this).addClass('is-visible');});
                                        }, 500);
                                    }
                                    else {
                                        filterForm.find('.js-form-item').each(function(){$(this).removeClass('is-visible');});
                                        setTimeout(function(){
                                            filterForm.removeClass('is-visible');
                                        }, 500);
                                    }
                                });
	                        });
	                    })()
	                </script>
	            </div>
			</form>
		</div>

		<div class="calculator__right page__right-content" id="fancyboxParent">
	        <div class="page__header">
	            <h1 class="calculator__title page__title wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">ипотечный калькулятор</h1>
	        </div>
	        <div class="calculator__descr small wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">Окончательный расчет суммы кредита и размер ежемесячного платежа производятся банком после предоставления полного комплекта документов и проведения оценки платежеспособности клиента.</div>
	        <div class="calculator__right-wrap" id="mortgageContainer">
				<?/*<div class="calculator__more-link  wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"><a href="#">акционные предложения</a></div>*/?>
<?endif;?>

<?
// if(isset($_REQUEST["AJAX_REQUEST"])){
	global ${$arParams["FILTER_NAME"]};

	$arTypes = [
		0 => 10,
		1 => 11
	];

	${$arParams["FILTER_NAME"]} = [
		// "PROPERTY_L_TYPE" => $arTypes[$_REQUEST["TYPE"]],
		">=PROPERTY_N_AGE" => $_REQUEST["AGE"] ? $_REQUEST["AGE"] : $arFilterDef["age"],
		">=PROPERTY_N_YEARS" => $_REQUEST["TIME"] ? $_REQUEST["TIME"] : $arFilterDef["long"],
		">=PROPERTY_N_MAX" => $_REQUEST["CREDIT"] ? $_REQUEST["CREDIT"] : $arFilterDef["price"] - $arFilterDef["first"],
		"<=PROPERTY_N_MIN" => $_REQUEST["CREDIT"] ? $_REQUEST["CREDIT"] : $arFilterDef["price"] - $arFilterDef["first"],
	];
// }
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"mortgage",
	Array(
		"FILTER_DEF" => $arFilterDef,
		"USE_FILTER" => $arParams["USE_FILTER"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"NEWS_COUNT" => $arParams["NEWS_COUNT"],
		"SORT_BY1" => $arParams["SORT_BY1"],
		"SORT_ORDER1" => $arParams["SORT_ORDER1"],
		"SORT_BY2" => $arParams["SORT_BY2"],
		"SORT_ORDER2" => $arParams["SORT_ORDER2"],
		"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"SHOW_404" => $arParams["SHOW_404"],
		"FILE_404" => $arParams["FILE_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
		"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
		"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
		"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
		"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
	),
	$component
);?>

<?if(!isset($_REQUEST["AJAX_REQUEST"])):?>

	            <div class="preloader">
	                <div class="preloader__item"></div>
	                <div class="preloader__item"></div>
	                <div class="preloader__item"></div>
	            </div>
	        </div>
		</div>

	    <div class="modal" style="display: none;" id="personalOffer">
	        <div class="modal__wrap">
	            <div class="modal__title h1">заполните форму заявки</div>
	            <form class="modal__form js-form" data-before="checkCalc();" data-ya-target="order_done">
	                <?=bitrix_sessid_post();?>
					<input type="hidden" name="action" value="IPOTEKA">
					<input type="hidden" name="FIELDS[AGE]" class="js-formAge">
					<input type="hidden" name="FIELDS[TIME]" class="js-formTime">
					<input type="hidden" name="FIELDS[CREDIT]" class="js-formCredit">
	                <label class="input-label">
	                    <span>* Фамилия Имя Отчество</span>
	                    <input type="text" name="FIELDS[NAME]" class="required">
	                </label>
	                <label class="input-label">
	                    <span>* Мобильный телефон</span>
	                    <input type="text" name="FIELDS[PHONE]" class="required input--phone" pattern="^\+7\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}">
	                </label>
	                <label class="input-label">
	                    <span>Адрес эл. почты</span>
	                    <input type="email" name="FIELDS[EMAIL]">
	                </label>
	                <label class="input-label  input-label--checkbox">
	                    <input type="checkbox" name="agreed" class="checkbox required">
	                    <span class="checkbox-custom  checkbox-custom--mark"></span>
	                    <span class="input-label__agreed-text">Согласен с условиями <a href="/personal-confirm/">Согласия</a> на обработку персональных данных</span>
	                </label>
	                <button class="modal__submit button button--center-black" type="submit" data-hoverContent="Далее" data-content="Далее"></button>
	            </form>
	        </div>
	    </div>
	    <div id="successMessage" class="success-message" style="display: none;">
	        <div class="success-message__inner">
	            <div class="h1">Ваша заявка успешно отправлена!</div>
	            <div class="success-message__subtitle">Наши менеджеры перезвонят вам в течение рабочего дня.</div>
	        </div>
	    </div>

	    <script>
	        (function(){
	            document.addEventListener('DOMContentLoaded', function(event){
	                $('.page-header').addClass('page-header--no-bg');
	                $('#pageFooter').addClass('page-footer--half-width');
	                initFancyBox();

	                var mortgageContainer = $('#mortgageContainer'),
	                    mortgageList = $('#mortgageList'),
	                    mortgageContainerPositionTop = mortgageContainer.position().top,
	                    mortgageListPositionTop = mortgageList.position().top,
	                    calcForm = $('#calcForm'),
	                    topSlide = $('#topSlide'),
	                    calcFormOffsetBottom = calcForm.offset().top + calcForm.outerHeight(),
	                    pageFooter = $('#pageFooter'),
	                    aside = $('aside.aside');
                    setTimeout(function(){
                        if ($(window).width() > 1000) {
                            calcForm.css('top', mortgageListPositionTop);
                        } else {
                            calcForm.css('top', mortgageContainerPositionTop);
                        }
                    }, 20);
	                if ($(window).width() > 1000) {
	                    $(window).on('scroll', function(){
	                        if (($(window).scrollTop() + $(window).height()) >= calcFormOffsetBottom && (mortgageContainer.outerHeight() > calcForm.outerHeight())) {
	                            calcForm.addClass('is-fixed');
	                        } else {
	                            calcForm.removeClass('is-fixed');
	                        }

	                        if (($(window).scrollTop() + $(window).height()) >= (topSlide.outerHeight() + mortgageContainerPositionTop +  mortgageContainer.outerHeight()) && (mortgageContainer.outerHeight() < calcForm.outerHeight())) {
	                            pageFooter.addClass('is-fixed');
	                            pageFooter.css('left', (aside.width() + calcForm.width()) + 'px');
	                        }
	                        else {
	                            pageFooter.removeClass('is-fixed');
	                            pageFooter.css('left', '0');
	                        }
	                    });
	                }

	                $(window).resize(function(){
	                    mortgageListPositionTop = mortgageList.position().top;
                        if ($(window).width() > 1000) {
                            calcForm.css('top', mortgageListPositionTop);
                        } else {
                            calcForm.css('top', mortgageContainerPositionTop);
                        }
	                    if ($(window).width() <= 1000) {
	                        calcForm.removeClass('is-fixed');
	                        pageFooter.removeClass('is-fixed');
	                        pageFooter.css('left', '0');
	                    }
	                });

	            });
	        })()
	    </script>
	</div>
<?endif;?>