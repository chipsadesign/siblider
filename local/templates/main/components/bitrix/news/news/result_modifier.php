<?
$rsElement = CIBlockElement::GetList(["created" => "ASC"], ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y"], false, ["nTopCount" => 1], ["ID", "DATE_CREATE"]);
if($arElement = $rsElement->Fetch()){
	$arResult["DATE_START"] = FormatDate("d.m.Y", MakeTimeStamp($arElement["DATE_CREATE"]));
}
$rsElement = CIBlockElement::GetList(["created" => "DESC"], ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y"], false, ["nTopCount" => 1], ["ID", "DATE_CREATE"]);
if($arElement = $rsElement->Fetch()){
	$arResult["DATE_STOP"] = FormatDate("d.m.Y", MakeTimeStamp($arElement["DATE_CREATE"]));
}