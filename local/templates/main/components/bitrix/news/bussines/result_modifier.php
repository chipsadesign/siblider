<?
if($this->getComponent()->getTemplatePage() == "parking"){
    $rsElement = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_HOUSE, "CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"]], false, false, ["ID", "PROPERTY_E_PROJECT"]);
    if($arElement = $rsElement->Fetch()){
        $rsParking = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_HOUSE, "PROPERTY_E_PROJECT" => $arElement["PROPERTY_E_PROJECT_VALUE"], "PROPERTY_S_TYPE" => "PARKING"], false, false, ["ID"]);
        if($arParking = $rsParking->Fetch()){
            $arResult["ELEMENT_ID"] = $arParking["ID"];
        }
    }
}