<div class="page__header">
	<h1 class="page__title big wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".2s">предложения месяца</h1>
</div>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?if(empty($arItem["House"])) continue;?>
	<div class="page__section">
		<div class="offer-section">
			<div class="offer-section__head container">
				<div class="offer-section__title h1"><?=$arItem["House"]["PROPERTY_E_PROJECT_NAME"]?></div>
			</div>
			<div class="offer-section__body">
				<div class="offer-section__gallery">
					<div class="slider slider--build">
						<div class="slider__container swiper-container js-build-gallery">
							<div class="slider__wrap swiper-wrapper">
								<?foreach($arItem["IMAGES"] as $arFile):?>
									<div data-title="<?=$arFile["DESCRIPTION"]?>" class="slider__item swiper-slide parallax-img">
										<div class="js-parallax-img parallax-img__img js-image" data-direction="1" data-speed="1.2" style="background-image: url(<?=$arFile["SRC"]?>);"></div>
									</div>
								<?endforeach;?>
							</div>
							<div class="slider__curtain js-slider-curtain"></div>
							<div class="slider__text-dots">
								<div class="slider__text-dots-wrap swiper-pagination js-build-gallery-dots"></div>
							</div>
							<div class="slider__nav slider__nav--triangle">
								<div class="slider__nav-item js-slider-prev"></div>
								<div class="slider__nav-item js-slider-next"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="offer-section__contact">
					<div class="build-info">
						<div class="build-info__body container">
							<div class="characteristics characteristics--cols">
								<div class="characteristics__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
									<div class="characteristics__title">адрес</div>
									<div class="characteristics__value"><?=$arItem["House"]["PROPERTY_S_ADRESS_VALUE"]?></div>
								</div>
								<div class="characteristics__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
									<div class="characteristics__title">Начало строительства</div>
									<div class="characteristics__value"><?=$arItem["House"]["PROPERTY_S_DATE_START_VALUE"]?></div>
								</div>
								<div class="characteristics__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
									<div class="characteristics__title">Кол. этажей</div>
									<div class="characteristics__value"><?=$arItem["House"]["PROPERTY_N_FLOORS_VALUE"]?></div>
								</div>
								<div class="characteristics__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
									<div class="characteristics__title">Материал стен</div>
									<div class="characteristics__value"><?=$arItem["House"]["PROPERTY_L_MATERIAL_VALUE"]?></div>
								</div>
								<div class="characteristics__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
									<div class="characteristics__title">срок сдачи</div>
									<div class="characteristics__value"><?=$arItem["House"]["PROPERTY_S_DATE_END_VALUE"]?></div>
								</div>
								<div class="characteristics__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
									<div class="characteristics__title">Кол. подъездов</div>
									<div class="characteristics__value"><?=$arItem["House"]["PROPERTY_N_ENTRANCE_VALUE"]?></div>
								</div>
								<div class="characteristics__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
									<div class="characteristics__title">отделка квартир</div>
									<div class="characteristics__value"><?=$arItem["House"]["PROPERTY_S_FACING_VALUE"]?></div>
								</div>
							</div>
						</div>
						<a href="<?=$arItem["House"]["DETAIL_PAGE_URL"]?>" class="build-info__more container button button--default button--light button--rectangle-animate wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
							<span>Узнайте больше о жилом комплексе</span>
							<span class="icon-arrow icon-arrow--right"></span>
						</a>
					</div>
				</div>
			</div>
			<div class="offer-section__flats">
				<div class="flats-slider swiper-container js-flat-slider">
					<div class="flats-slider__head container">
						<div class="flats-slider__title h1">квартир — <?=count($arItem["PROPERTIES"]["E_FLAT"]["VALUE"])?></div>
						<div class="flats-slider__nav">
							<div class="flats-slider__button button-icon button-icon--animate js-slide-prev">
								<span class="button-icon__icon icon-triangle-left"></span>
							</div>
							<div class="flats-slider__dots dots js-slide-dots"></div>
							<div href="" class="flats-slider__button button-icon button-icon--animate js-slide-next">
								<span class="button-icon__icon icon-triangle-right"></span>
							</div>
						</div>
					</div>
					<div class="flats-slider__inner swiper-wrapper">
						<?foreach($arItem["Fats"] as $arFlat):?>
							<div class="flats-slider__slide swiper-slide">
								<div class="flat-preview">
									<div class="flat-preview__card container">
										<div class="flat-preview__description">
											<div class="flat-preview__left"><?=$arFlat["PROPERTY_N_AREA_VALUE"]?> м²</div>
											<div class="flat-preview__right"><?=number_format($arFlat["PROPERTY_N_PRICE_VALUE"], 0, ".", " ");?> ₽</div>
										</div>
										<div class="flat-preview__photo">
											<div class="flat-preview__img img" style="background-image: url(<?=$arFlat["PROPERTY_F_PRESET_VALUE"]?>"></div>
										</div>
										<div class="flat-preview__info">
											<div class="characteristics characteristics--cols">
												<div class="characteristics__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
													<div class="characteristics__title">подъезд</div>
													<div class="characteristics__value"><?=$arFlat["PROPERTY_S_ENTRANCE_VALUE"]?></div>
												</div>
												<div class="characteristics__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
													<div class="characteristics__title">этаж</div>
													<div class="characteristics__value"><?=$arFlat["PROPERTY_N_FLOOR_VALUE"]?></div>
												</div>
												<div class="characteristics__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
													<div class="characteristics__title">Кв.</div>
													<div class="characteristics__value"><?=$arFlat["NAME"]?></div>
												</div>
												<div class="characteristics__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
													<div class="characteristics__title">комнат</div>
													<div class="characteristics__value"><?=$arFlat["PROPERTY_N_ROOMS_VALUE"]?></div>
												</div>
											</div>
										</div>
									</div>
									<div class="flat-preview__ctrl">
										<a data-fancybox data-src="#bookingModal" class="button button--default button--black button--rectangle-animate button--icon-animate wow fadeIn js-monthBook" data-wow-duration=".5s" data-wow-delay=".2s" data-id="<?=$arFlat["NAME"]?>, <?=$arItem["House"]["NAME"]?>" data-managers="<?=implode(", ", $arItem["House"]["PROPERTY_E_MANAGER_VALUE"])?>">
											<span>Забронировать</span>
											<span class="icon-arrow icon-arrow--right"></span>
										</a>
									</div>
								</div>
							</div>
						<?endforeach;?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?endforeach;?>

<script>
	document.addEventListener('DOMContentLoaded', function(event){
		$('.js-monthBook').click(function(){
			var modal = $('#bookingModal');
			var managers = $(this).data('managers');
			var arManagers = managers.split(", ");
			for(i in arManagers){
				$('<input type="hidden" name="managers[]" value="'+arManagers[i]+'">').appendTo(modal.find('form'));
			}
			$('#js-field_flatName').val($(this).data('id'));
		});
	});
</script>
<div class="modal  modal--flat" style="display: none;" id="bookingModal">
    <div class="modal__wrap">
        <div class="modal__title h1">забронировать</div>
        <form class="modal__form js-form" autocomplete="off" data-ya-target="order_done">
			<?=bitrix_sessid_post();?>
			<input type="hidden" name="action" value="FLAT_BOOK">
			<input type="hidden" name="FIELDS[FLAT_NAME]" value="<?=$arResult["HOUSE"]["NAME"]?>, <?=$arResult["NAME"]?>" id="js-field_flatName">
            <label class="input-label">
                <span>* Фамилия Имя Отчество</span>
                <input type="text" name="FIELDS[NAME]" class="required">
            </label>
            <label class="input-label">
                <span>* Мобильный телефон</span>
                <input type="text" name="FIELDS[PHONE]" autocomplete="nope" class="required input--phone" pattern="^\+7\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}">
            </label>
            <label class="input-label">
                <span>Адрес эл. почты</span>
                <input type="email" name="FIELDS[EMAIL]">
            </label>
            <label class="input-label  input-label--checkbox">
                <input type="checkbox" name="agreed" class="checkbox required">
                <span class="checkbox-custom  checkbox-custom--mark"></span>
                <span class="input-label__agreed-text">Согласен с условиями <a href="/personal-confirm/">Согласия</a> на обработку персональных данных</span>
            </label>
            <button class="modal__submit button button--center-black" type="submit" data-hoverContent="Забронировать" data-content="Забронировать"></button>
        </form>
    </div>
</div>