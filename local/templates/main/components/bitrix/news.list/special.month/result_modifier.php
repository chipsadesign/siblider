<?

foreach($arResult["ITEMS"] as &$arItem){
	$houseID = $arItem["PROPERTIES"]["E_HOUSE"]["VALUE"];
	$arrHouseSelect = [
		"DETAIL_PAGE_URL",
		"PROPERTY_E_PROJECT.NAME",
		"PROPERTY_S_ADRESS",
		"PROPERTY_S_DATE_START",
		"PROPERTY_S_DATE_END",
		"PROPERTY_N_FLOORS",
		"PROPERTY_L_MATERIAL",
		"PROPERTY_N_ENTRANCE",
		"PROPERTY_S_FACING",
	];
	$arItem["House"] = Siblider::getHouseInfo($houseID, $arrHouseSelect);

	if(isset($arItem["DISPLAY_PROPERTIES"]["F_IMG"]["FILE_VALUE"]["ID"]))
		$arItem["IMAGES"][] = $arItem["DISPLAY_PROPERTIES"]["F_IMG"]["FILE_VALUE"];
	else
		$arItem["IMAGES"] = $arItem["DISPLAY_PROPERTIES"]["F_IMG"]["FILE_VALUE"];

	$flatIDs = $arItem["PROPERTIES"]["E_FLAT"]["VALUE"];
	$arrFlatSelect = [
		"PROPERTY_N_FLOOR",
		"PROPERTY_N_PRICE",
		"PROPERTY_N_AREA",
		"PROPERTY_F_PRESET",
		"PROPERTY_S_ENTRANCE",
		"PROPERTY_N_ROOMS",
	];
	$arItem["Fats"] = Siblider::getFlats($flatIDs, $arrFlatSelect);
}
