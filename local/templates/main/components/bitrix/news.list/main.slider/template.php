<div class="main-page-slider">
	<div class="large main-page-slider__title wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".2s">для тех, кто ценит</div>
	<div class="swiper-container wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".3s" id="mainPageSlider">
		<div class="swiper-wrapper">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<div class="swiper-slide">
					<a class="large" href="<?=$arItem["CODE"]?>"><?=$arItem["NAME"]?></a>
				</div>
			<?endforeach;?>
		</div>
	</div>
	<div class="swiper-pagination main-page-slider__dots"></div>
</div>
<div class="main-page-content">
	<div class="main-page-content__background wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s" id="mainBackground" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/main.jpg')">
		<div class="swiper-container" id="mainPageImagesSlider">
			<div class="swiper-wrapper">
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<div class="swiper-slide" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>')"></div>
				<?endforeach;?>
			</div>
		</div>
	</div>
	<script>
		(function(){
			document.addEventListener('DOMContentLoaded', function(event){
				$('.page-header').addClass('page-header--big');
				$('#menuOpenBtn').addClass('is-hidden');
				$('#pageFooter').addClass('is-hidden');
				$('#mainPageMenu').removeClass('is-hidden');
				var mainImagesSwiper = new Swiper('#mainPageImagesSlider', {
					speed: 660,
					effect: 'fade',
					fadeEffect: {
					 crossFade: false
					},
					loop: true
				});

				var mainSwiper = new Swiper('#mainPageSlider', {
					speed: 660,
					effect: 'slide',
					direction: 'vertical',
					fadeEffect: {
					 crossFade: false
					},
					loop: true,
					pagination: {
						el: '.swiper-pagination',
						clickable: true,
						bulletClass: 'main-page-slider__dot',
						bulletActiveClass: 'active'
					},
					autoplay: {
						delay: 4000,
						disableOnInteraction: false
					},
					on: {
						slideChange: function() {
							mainImagesSwiper.slideNext(660);
						}
					}
				});
				$('#mainPageSlider').on('mouseenter', function(e){
					mainSwiper.autoplay.stop();
				});
				$('#mainPageSlider').on('mouseleave', function(e){
					mainSwiper.autoplay.start();
				});

				$('#mainRightBtn, #mainBottomBtn, #mainLeftBtn').addClass('animated');
			});
		})()
	</script>
	<a href="/news/" class=" button button--light button--rectangle-animate main-page-content__left-button fadeInLeft"id="mainLeftBtn">
		<span>новости</span><span class="icon-arrow"></span>
	</a>
	<a href="/catalog/" class=" button button--light button--down button--rectangle-animate main-page-content__center-button fadeInUp"id="mainBottomBtn">
		<span>наши объекты</span><span class="icon-arrow icon-arrow--down"></span>
	</a>
	<a href="/special-offers/" class=" button button--light button--left button--rectangle-animate main-page-content__right-button fadeInRight"id="mainRightBtn">
		<span class="icon-arrow icon-arrow--right"></span><span>Спецпредложения</span>
	</a>
	<div class="change-log"><a href="/journal/">Журнал изменений</a></div>
</div>