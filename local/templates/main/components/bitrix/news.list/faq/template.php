<div class="questions">
	<div class="questions__tabs-header">
		<ul class="nav nav-tabs" id="questionsTabs" role="tablist">
			<?foreach($arResult["SECTIONS"] as $arSection):?>
				<?if(!empty($arResult["FAQ"][$arSection["ID"]])):?>
		            <li class="nav-item">
		                <a class="nav-link <?=!$active ? "active show" : ""?>" id="section<?=$arSection["ID"]?>-tab" data-toggle="tab" href="#section<?=$arSection["ID"]?>" role="tab" aria-controls="section<?=$arSection["ID"]?>" aria-selected="false"><?=$arSection["NAME"]?></a>
		            </li>
		            <?if(!$active) $active = $arSection["ID"];?>
	            <?endif;?>
            <?endforeach;?>
        </ul>
	</div>
	<div class="questions__tabs-content">
		<div class="tab-content" id="questionsTabContent">
			<?foreach($arResult["FAQ"] as $sid => $arSection):?>
				<div class="tab-pane js-tab-pane fade <?=$sid == $active ? "active show" : ""?>" id="section<?=$sid?>" role="tabpanel" aria-labelledby="section<?=$sid?>-tab">
					<?foreach($arSection as $arItem):?>
						<div class="questions__item js-dropdown dropdown">
							<div class="dropdown__header js-open-button">
								<div class="dropdown__title">
									<div class="questions__title"><span><?=$arItem["NAME"]?></span><span><?=FormatDate("d.m.Y", MakeTimeStamp($arItem["DATE_CREATE"]))?></span></div>
									<?=$arItem["PREVIEW_TEXT"]?>
								</div>
								<span class="icon-triangle"></span>
							</div>
							<div class="dropdown__content">
								<div class="dropdown__border"></div>
								<?=$arItem["DETAIL_TEXT"]?>
							</div>
						</div>
					<?endforeach;?>
				</div>
			<?endforeach;?>
		</div>
	</div>
</div>
<script>
	(function(){
		document.addEventListener('DOMContentLoaded', function(event){
			initFancyBox();
			$('.js-open-button').on('click', function(){
                $(this).toggleClass('is-opened');
                $(this).siblings().slideToggle(300);
            });
            $('#questionsTabContent .js-tab-pane').each(function(index){
            	if (index == 0) {
            		$(this).find('.js-dropdown').each(function(index){
            			$(this).css('animation-delay', ((index + 1) * 0.1 + 0.6) + 's');
            		});
            	} else {
            		$(this).find('.js-dropdown').each(function(index){
            			$(this).css('animation-delay', ((index + 1) * 0.1) + 's');
            		});
            	}
            });
            $('#questionsTabs a').on('click', function(){
            	$('#questionsTabContent .tab-pane:first-child .js-dropdown').each(function(index){
        			$(this).css('animation-delay', ((index + 1) * 0.1) + 's');
        		});
            });
        });
    })()
</script>