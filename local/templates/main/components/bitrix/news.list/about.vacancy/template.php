<?if(!empty($arResult["ITEMS"])):?>
	<div class="vacancy">
		<h2 class="h1">вакансии:</h2>
		<div class="vacancy__list">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<div class="vacancy__item js-open-vacancy">
					<div class="vacancy__title"><?=$arItem["NAME"]?></div>
					<div class="vacancy__info js-vacancy-info" style="display: none;"><?=$arItem["PREVIEW_TEXT"]?></div>
				</div>
			<?endforeach;?>
		</div>
		<script>
			(function(){
				document.addEventListener('DOMContentLoaded', function(event){
					$('.js-open-vacancy').on('click', function(){
                        $(this).find('.js-vacancy-info').slideToggle(300);
                    });
                });
            })()
		</script>
	</div>
<?endif;?>