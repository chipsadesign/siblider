<?if(!isset($_REQUEST["AJAX_REQUEST"])):?>
    <div class="page__header page__header--with-border wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".2s">
        <h1 class="page__title">каталог объектов</h1>
        <a href="/business/" class="page__header-link"><span class="hover-underline">подобрать офис</span><span class="icon-choose-flat"></span></a>
        <a id="fancyMap" href="#modalMap" class="page__header-link"><span class="hover-underline">все объекты на карте</span><span class="icon-pointer-on-map"></span></a>
    </div>

    <?include($_SERVER["DOCUMENT_ROOT"]."/".$this->getFolder()."/map.php");?>
    <div class="objects-list">
<?endif;?>
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?if(count($arItem["HOUSE"]) === 0) continue;?>
        <div class="objects-list__item object-item js-object-item">
            <?foreach($arItem["HOUSE"] as $key => $arHouse):?>
                <div class="objects-list__item_wrapper <?=$key > 0 ? "is-hidden" : ""?>">
                    <div class="object-item__content js-object-content fadeInRight">
                        <div class="object-item__info">
                            <div class="object-item__header">
                                <a href="<?=$arHouse["DETAIL_PAGE_URL"]?>" class="object-item__title h1 js-object-title"><?=$arItem["NAME"]?></a>
                                <?if(count($arItem["HOUSE"]) > 1):?>
                                    <div class="object-item__stages">
                                        <?foreach($arItem["HOUSE"] as $jay => $arStage):?>
                                            <?if($arStage["PROPERTY_S_TYPE_VALUE"] == "PARKING"):?>
                                                <div class="object-item__stage <?if($jay == $key):?>is-active<?endif;?> js-object-stage">Парковка</div>
                                            <?else:?>
                                                <div class="object-item__stage <?if($jay == $key):?>is-active<?endif;?> js-object-stage"><?=$jay + 1?> этап</div>
                                            <?endif;?>
                                        <?endforeach;?>
                                    </div>
                                <?endif;?>
                            </div>
                            <div class="object-item__characteristics characteristics js-object-characteristics">
                                <?if($arHouse["PROPERTY_S_ADRESS_VALUE"]):?>
                                    <div class="characteristics__item">
                                        <div class="characteristics__title">адрес строения</div>
                                        <div class="characteristics__value"><?=$arHouse["PROPERTY_S_ADRESS_VALUE"];?></div>
                                    </div>
                                <?endif;?>
                                <?if($arHouse["PROPERTY_S_DATE_END_VALUE"]):?>
                                    <div class="characteristics__item">
                                        <div class="characteristics__title">дата сдачи</div>
                                        <div class="characteristics__value"><?=$arHouse["PROPERTY_S_DATE_END_VALUE"];?></div>
                                    </div>
                                <?endif;?>
                                <?if($arHouse["PROPERTY_S_MATERIAL_VALUE"]):?>
                                    <div class="characteristics__item">
                                        <div class="characteristics__title">материал стен</div>
                                        <div class="characteristics__value"><?=$arHouse["PROPERTY_S_MATERIAL_VALUE"]?></div>
                                    </div>
                                <?endif;?>
                                <?if($arHouse["PROPERTY_N_FLOORS_VALUE"]):?>
                                    <div class="characteristics__item">
                                        <div class="characteristics__title">количество этажей</div>
                                        <div class="characteristics__value"><?=$arHouse["PROPERTY_N_FLOORS_VALUE"]?></div>
                                    </div>
                                <?endif;?>
                                <?if($arHouse["PROPERTY_S_FACING_VALUE"]):?>
                                    <div class="characteristics__item">
                                        <div class="characteristics__title">отделка квартир</div>
                                        <div class="characteristics__value"><?=$arHouse["PROPERTY_S_FACING_VALUE"]?></div>
                                    </div>
                                <?endif;?>
                            </div>
                        </div>
                        <a href="<?=$arHouse["DETAIL_PAGE_URL"]?>" class="button button--light button--icon-animate">
                            <span>подробнее</span><span class="icon-arrow icon-arrow--right"></span>
                        </a>
                    </div>
                    <a href="<?=$arHouse["DETAIL_PAGE_URL"]?>" class="object-item__img-wrap">
                        <?if(isset($arHouse["PICTURE"][0])):?>
                            <div class="object-item__img scaleOut js-object-img" style="background-image: url(<?=$arHouse["PICTURE"][0]?>);">
                                <?if(isset($arHouse["PICTURE"][1])):?>
                                    <div class="object-item__second-img"style="background-image: url(<?=$arHouse["PICTURE"][1]?>);"></div>
                                <?endif;?>
                            </div>
                        <?endif;?>
                        <?if($arHouse["PROPERTY_L_STATUS_VALUE"]):?>
                            <div class="object-item__status fadeInRight js-object-status"><?=$arHouse["PROPERTY_L_STATUS_VALUE"]?></div>
                        <?endif;?>
                    </a>
                </div>
            <?endforeach;?>
        </div>
    <?endforeach;?>

<?if(!isset($_REQUEST["AJAX_REQUEST"])):?>
        <?if(isset($arResult["ARCHIVE"])):?>
            <a href="#" class="button button--big-dark button--down button--rectangle-animate js-showArchive">
                <span>архив объектов</span><span class="icon-arrow icon-arrow--down"></span>
            </a>
        <?endif;?>
    </div>
    <script>
        $(document).ready(function(){
            $('.js-showArchive').click(function(){
                var target = $(this);
                var opt = {
                    url: window.location.href,
                    dataType: 'html',
                    data: {
                        ARCHIVE: 'Y',
                        AJAX_REQUEST: 'Y'
                    },
                    successCallback: function(response){
                        target.replaceWith(response);
                    },
                };
                ajaxRequest(opt);

                return false;
            });
        });
    </script>
<?endif;?>
<script>
    <?if(!isset($_REQUEST["AJAX_REQUEST"])):?>
        (function(){
            document.addEventListener('DOMContentLoaded', function(event){
    <?endif;?>
            initFancyBox();
            var maxDelay = 0;
            $('.js-object-item').each(function(index){
                $(this).find('.js-object-content').css('animation-delay', ((index + 1) * 0.2 + 0.6) + 's');
                $(this).find('.js-object-img').css('animation-delay', ((index + 1) * 0.2 + 0.6) + 's');
                $(this).find('.js-object-status').css('animation-delay', ((index + 1) * 0.2 + 0.6) + 's');

                maxDelay = (index + 1) * 200 + 600;
            });

            setTimeout(function(){
                $('.js-object-item').each(function(index){
                    $(this).find('.js-object-content').removeClass('fadeInRight');
                    $(this).find('.js-object-img').removeClass('scaleOut');
                    $(this).find('.js-object-status').removeClass('fadeInRight');
                });
            }, maxDelay);
            $('.js-object-stage').on('click', function(e){
                if($(this).hasClass('is-active')){
                    return;
                }else{
                    var container = $(this).parents('.js-object-item');
                    container.addClass('is-changing');
                    container.find('.js-object-content > *').addClass('animate-hide-to-left');

                    var parent = $(this).parents('.objects-list__item_wrapper');
                    var index = $(this).index();
                    var target = container.find('.objects-list__item_wrapper:eq('+index+')');

                    target.find('.js-object-content > *').addClass('animate-hide-to-left');

                    setTimeout(function(){
                        parent.addClass('is-hidden');
                        target.removeClass('is-hidden');
                        container.addClass('is-changed');
                        container.find('.js-object-content > *').removeClass('animate-hide-to-left');
                        target.find('.js-object-content > *').addClass('animate-visible-to-left');
                        setTimeout(function(){
                            container.removeClass('is-changed  is-changing');
                            target.find('.js-object-content > *').removeClass('animate-visible-to-left animate-hide-to-left');
                        }, 500);
                    }, 350);
                }
            });
    <?if(!isset($_REQUEST["AJAX_REQUEST"])):?>
            });
        })()
    <?endif;?>
</script>