<?
foreach($arResult["ITEMS"] as $arItem){
	$arHouse[] = $arItem["PROPERTIES"]["E_HOUSE"]["VALUE"];
}
$arHouse = array_unique($arHouse);

$rsElement = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_HOUSE, "!PROPERTY_S_TYPE" => "PARKING", "!PROPERTY_L_ARCHIVE" => "Y", "ID" => $arHouse], false, false, ["ID", "IBLOCK_ID", "PROPERTY_E_PROJECT"]);
while($arElement = $rsElement->Fetch()){
	$arProjects[] = $arElement["PROPERTY_E_PROJECT_VALUE"];
}
$arProjects = array_unique($arProjects);

unset($arResult["ITEMS"]);
$rsElement = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_OBJECT, "ID" => $arProjects], false, false, ["ID", "NAME"]);
while($arElement = $rsElement->Fetch()){
	$arResult["ITEMS"][] = $arElement;
}

$arSelect = [
	"ID",
	"NAME",
	"CODE",
	"DETAIL_PAGE_URL",
	"PROPERTY_L_MATERIAL",
	"PROPERTY_S_DATE_END",
	"PROPERTY_S_ADRESS",
	"PROPERTY_L_STATUS",
	"PROPERTY_N_FLOORS",
	"PROPERTY_S_FACADE",
	"PROPERTY_F_PICTURE",
	"PROPERTY_S_TYPE",
];
foreach($arResult["ITEMS"] as &$arItem){
	$rsElement = CIBlockElement::GetList([], ["PROPERTY_E_PROJECT" => $arItem["ID"], "!PROPERTY_S_TYPE" => "PARKING", "IBLOCK_ID" => IBLOCK_ID_HOUSE], false, false, $arSelect);
	while($arElement = $rsElement->GetNext()){
		if(!in_array($arElement["ID"], $arHouse))
			continue;
		if(!empty($arElement["PROPERTY_F_PICTURE_VALUE"])){
			foreach($arElement["PROPERTY_F_PICTURE_VALUE"] as $pic_id)
				$arElement["PICTURE"][] = CFile::GetPath($pic_id);
		}
		$arElement["DETAIL_PAGE_URL"] = "/business/filter/house-".$arElement["ID"]."/";
		$arItem["HOUSE"][] = $arElement;
	}
}