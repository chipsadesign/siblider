<?
foreach($arResult["ITEMS"] as $key => &$arItem){
	if(!$arItem["PROPERTIES"]["S_COORD"]["VALUE"]){
        unset($arResult["ITEMS"][$key]);
        continue;
	}

	$arItem["NAME"] = $arItem["DISPLAY_PROPERTIES"]["E_PROJECT"]["LINK_ELEMENT_VALUE"][$arItem["DISPLAY_PROPERTIES"]["E_PROJECT"]["VALUE"]]["NAME"];

	$arTmpResult[$arItem["DISPLAY_PROPERTIES"]["E_PROJECT"]["VALUE"]][] = [
		"NAME" => $arItem["NAME"],
		"DETAIL_PAGE_URL" => $arItem["DETAIL_PAGE_URL"],
		"PROPERTIES" => [
			"S_COORD" => [
				"VALUE" => $arItem["PROPERTIES"]["S_COORD"]["VALUE"]
			],
			"F_MAP" => [
				"VALUE" => $arItem["PROPERTIES"]["F_MAP"]["VALUE"]
			]
		]
	];
}
unset($arItem);

$arResultItems = [];
foreach($arTmpResult as $arProject){
	$arItem = [];
	if(count($arProject) > 1){
		foreach($arProject as $arProjectItem){
			if($arProjectItem["PROPERTIES"]["F_MAP"]["VALUE"])
				$arItem = $arProjectItem;
		}
		if(empty($arItem)){
			$arItem = $arProject[0];
		}
	}else{
		$arItem = $arProject[0];
	}
	$arResultItems[] = $arItem;
}
$arResult["ITEMS"] = $arResultItems;

$rsElement = CIBLockElement::GetList([], ["IBLOCK_ID" => 24, "ACTIVE" => "Y", "!PROPERTY_F_MAP" => false, "!PROPERTY_S_COORD" => false], false, false, ["ID", "NAME", "PROPERTY_S_COORD", "PROPERTY_F_MAP"]);
while($arElement = $rsElement->Fetch()){
	$arItem = [
		"NAME" => $arElement["NAME"],
		"PROPERTIES" => [
			"F_MAP" => [
				"VALUE" => $arElement["PROPERTY_F_MAP_VALUE"]
			],
			"S_COORD" => [
				"VALUE" => $arElement["PROPERTY_S_COORD_VALUE"]
			]
		]
	];
	$arResult["INFRA"][] = $arItem;
}