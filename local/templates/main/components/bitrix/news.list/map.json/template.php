<?
$arFeatures = [];
foreach($arResult["ITEMS"] as $arItem){
    if(!$arItem["PROPERTIES"]["S_COORD"]["VALUE"])
        continue;

    $arCoord = explode(",", $arItem["PROPERTIES"]["S_COORD"]["VALUE"]);

    $icon = "/local/templates/main/images/point.svg?1";
    $size = [50, 100];
    $model = false;
    if(intval($arItem["PROPERTIES"]["F_MAP"]["VALUE"])){
        $model = CFile::GetPath($arItem["PROPERTIES"]["F_MAP"]["VALUE"]);
    }

    $arFeatures[] = [
        "model" => $model,
        "type" => "Feature",
        "properties" => [
            "title" => "<a href='".$arItem["DETAIL_PAGE_URL"]."'>".$arItem["NAME"]."</a>",
            "name" => $arItem["NAME"],
            "link" => $arItem["DETAIL_PAGE_URL"],
            "popup_content" => "<a href='".$arItem["DETAIL_PAGE_URL"]."'>".$arItem["NAME"]."</a>",
            "icon" => [
                "html" => '<img class="map--icon" src="'.$icon.'>',
                "src" => $icon,
                "popupAnchor" => [0, 2000],
                "size" => $size,
            ]
        ],
        "geometry" => [
            "type" => "Point",
            "coordinates" => [
                trim($arCoord[0]),
                trim($arCoord[1])
            ]
        ]
    ];
}
unset($arItem);

foreach($arResult["INFRA"] as $arItem){
    $arCoord = explode(",", $arItem["PROPERTIES"]["S_COORD"]["VALUE"]);

    $icon = "/local/templates/main/images/point.svg?1";
    $size = [50, 100];
    if(intval($arItem["PROPERTIES"]["F_MAP"]["VALUE"])){
        $icon = CFile::GetPath($arItem["PROPERTIES"]["F_MAP"]["VALUE"]);
        $size = [180, 100];
    }

    $arFeatures[] = [
        "type" => "Infra",
        "properties" => [
            "class" => "mapbox--infra hidden",
            // "title" => "<a href='".$arItem["DETAIL_PAGE_URL"]."'>".$arItem["NAME"]."</a>",
            // "name" => $arItem["NAME"],
            // "popup_content" => $arItem["NAME"],
            "icon" => [
                "html" => '<img class="map--icon" src="'.$icon.'">',
                "src" => $icon,
                "popupAnchor" => [-5, -25],
                "size" => $size,
            ]
        ],
        "geometry" => [
            "type" => "Point",
            "coordinates" => [
                trim($arCoord[0]),
                trim($arCoord[1])
            ]
        ]
    ];
}


$arJson = [
    "type" => "FeatureCollection",
    "crs" => [
        "type" => "name",
        "properties" => [
            "name" => "urn:ogc:def:crs:OGC:1.3:CRS84"
        ]
    ],
    "features" => $arFeatures
];

if(count($arResult["ITEMS"]) == 1){
    $arJson["mapCenter"] = explode(",", $arResult["ITEMS"][0]["PROPERTIES"]["S_COORD"]["VALUE"]);
}

echo json_encode($arJson);