<?
foreach($arResult["ITEMS"] as &$arItem){
    $arProps = $arItem["DISPLAY_PROPERTIES"];
    $userID = $arProps["E_USER"]["VALUE"];
    $arUsers[] = $userID;
}
$arUsers = array_unique($arUsers);

foreach($arUsers as $id){
    $rsUser = CUser::GetList(($by="personal_country"), ($order="desc"), ["ID" => $id]);
    if($arUser = $rsUser->Fetch()){
        $arName = [
            $arUser["NAME"],
            $arUser["LAST_NAME"]
        ];
        $name = trim(implode(" ", $arName));
        if(strlen($name) == 0)
            $name = $arUser["LOGIN"];
        $arResult["USERS"][$arUser["ID"]] = [
            "NAME" => $name,
            "POSITION" => $arUser["WORK_POSITION"]
        ];
    }
}