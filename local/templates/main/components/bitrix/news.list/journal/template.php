<div class="page__header  page__header--with-border">
	<h1 class="page__title">Журнал изменений</h1>
</div>

<div class="page__text">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
            $arProps = $arItem["DISPLAY_PROPERTIES"];
            $arHouse = $arProps["E_HOUSE"]["LINK_ELEMENT_VALUE"][$arProps["E_HOUSE"]["VALUE"]];
            $userID = $arProps["E_USER"]["VALUE"];
            $arFile = $arProps["F_FILE"]["FILE_VALUE"];
            $arUser = $arResult["USERS"][$userID];
        ?>

        <?=$arItem["NAME"]?>
        <ul>
            <li>Объект: <a href="<?=$arHouse["DETAIL_PAGE_URL"]?>"><?=$arHouse["NAME"]?></a></li>
            <?if($arUser["NAME"]):?>
                <li>Опубликовал: <?=$arUser["NAME"]?></li>
            <?endif;?>
            <?if($arUser["POSITION"]):?>
                <li>Должность: <?=$arUser["POSITION"]?></li>
            <?endif;?>
            <li>Дата: <?=$arItem["DATE_CREATE"]?></li>
            <?if(!empty($arFile)):?>
                <li><a href="<?=$arFile["SRC"]?>">Посмотреть файл (<?=siteHandlers::formatFileSize($arFile["FILE_SIZE"])?>)</a></li>
            <?endif;?>
            <?if(!empty($arProps["S_VIDEO"])):?>
                <li><a href="<?=$arProps["S_VIDEO"]?>">Посмотреть видео</a>
            <?endif;?>
        </ul>
    <?endforeach;?>
</div>

<?=$arResult["NAV_STRING"]?>