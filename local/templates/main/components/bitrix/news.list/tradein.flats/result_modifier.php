<?
$rsPrograms = CIBlockElement::GetList([], ["IBLOCK_ID" => 23, "ACTIVE" => "Y", "!PROPERTY_E_FLATS" => false], false, false, ["ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_E_FLATS"]);
while($arPrograms = $rsPrograms->GetNext()){
    foreach($arPrograms["PROPERTY_E_FLATS_VALUE"] as $id){
        $arResult["PROGRAMS"][$id][] = [
            "NAME" => $arPrograms["NAME"],
            "URL" => $arPrograms["DETAIL_PAGE_URL"]
        ];
    }
}