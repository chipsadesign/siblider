<?if(!empty($arResult["ITEMS"])):?>
    <div class="tradein__list">
        <h2 class="h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"><?=$arResult["DESCRIPTION"]?></h2>
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <div class="tradein__list-item  wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
                <div class="tradein__list-icon">
                    <img src="<?=$arItem["DISPLAY_PROPERTIES"]["F_ICO"]["FILE_VALUE"]["SRC"]?>" alt="">
                </div>
                <div class="tradein__list-content">
                    <div class="tradein__list-title"><?=$arItem["NAME"]?></div>
                    <div class="small"><?=$arItem["PREVIEW_TEXT"]?></div>
                </div>
                <?if(!empty($arResult["PROGRAMS"][$arItem["ID"]])):?>
                    <div class="tradein__list-header">подходящие программы</div>
                    <ul class="list">
                        <?foreach($arResult["PROGRAMS"][$arItem["ID"]] as $arProgram):?>
                            <li><a class="small" href="<?=$arProgram["URL"]?>"><?=$arProgram["NAME"]?></a></li>
                        <?endforeach;?>
                    </ul>
                <?endif;?>
            </div>
        <?endforeach;?>
    </div>
<?endif;?>