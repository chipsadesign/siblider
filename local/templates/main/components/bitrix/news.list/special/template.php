<?if(!isset($_REQUEST["AJAX_REQUEST"])):?>
	<div class="offers">
<?endif;?>

	<?foreach($arResult["ITEMS"] as $arItem):?>
		<div  class="offers__item offer  wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="offer__link">
				<div class="offer__img-wrap">
					<div class="offer__img" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>')"></div>
					<?if($arItem["PROPERTIES"]["S_DATE"]["VALUE"]):?>
						<div class="offer__date"><?=$arItem["PROPERTIES"]["S_DATE"]["VALUE"]?></div>
					<?endif;?>
				</div>
				<div class="button button--light  button--icon-animate">
					<span class="h1"><?=$arItem["NAME"]?></span><span class="icon-arrow  icon-arrow--right"></span>
				</div>
			</a>
		</div>
	<?endforeach;?>

	<?if(isset($_REQUEST["PAGEN_1"])):?>
		<?if($arResult["NAV_RESULT"]->NavPageNomer < $arResult["NAV_RESULT"]->NavPageCount):?>
			<div class="offers__item offer wow fadeInUp offers__item--button js-button-add-more-parent">
				<a href="#"
					href="#"
					class="button button--big-dark button--down button--rectangle-animate js-button-add-more"
					data-total="<?=$arResult["NAV_RESULT"]->NavPageCount?>"
					data-pagen="1"
					data-page="<?=$arResult["NAV_RESULT"]->NavPageNomer + 1;?>"
					>
					<span>Показать ещё</span><span class="icon-arrow icon-arrow--down"></span>
				</a>
			</div>
		<?endif;?>
	<?else:?>
		<?if($arResult["SHOW_ARCHIVE"] == "Y"):?>
			<div class="offers__item offer wow fadeInUp offers__item--button js-button-add-more-parent">
				<a
					href="#"
					class="button button--big-dark button--down button--rectangle-animate js-button-add-more"
					data-pagen="1"
					data-page="1"
					>
					<span>архив спецпредложений</span><span class="icon-arrow icon-arrow--down"></span>
				</a>
			</div>
		<?endif;?>
	<?endif;?>

<?if(!isset($_REQUEST["AJAX_REQUEST"])):?>
	</div>
<?endif;?>