<?if(!empty($arResult["ITEMS"])):?>
	<div class="news-slider-wrap slider" id="newsSliderWrap">
		<div class="slider__container swiper-container" id="newsSlider">
			<div class="slider__wrap  swiper-wrapper">
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<div data-url="<?=$arItem["DETAIL_PAGE_URL"]?>" data-title="<?=$arItem["NAME"]?>" data-data="<?=FormatDate("j F Y", MakeTimeStamp($arItem["DATE_CREATE"]))?>" class="news-slider__item news-slide slider__item swiper-slide">
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="news-slide__img  slider__img" style="background-image: url('<?=$arItem["DETAIL_PICTURE"]["SRC"]?>');"></a>
					</div>
				<?endforeach;?>
			</div>
			<div class="slider__curtain js-slider-curtain"></div>
		</div>
		<div class="slider__nav">
			<div class="slider__nav-item js-slider-prev"></div>
			<div class="slider__nav-item js-slider-next"></div>
		</div>
		<div class="news-slider-wrap__info wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
			<div class="news-slider-wrap__header" id="newsSliderInfo">
				<div class="news-slider-wrap__date" id="newsDate"></div>
				<a href="<?=$arResult["ITEMS"][0]["DETAIL_PAGE_URL"]?>" class="news-slider-wrap__title  small" id="newsTitle"></a>
			</div>
			<a href="<?=$arResult["ITEMS"][0]["DETAIL_PAGE_URL"]?>" id="newsLink" class="button  button--light  button--icon-animate  news-slider-wrap__more">подробнее<span class="icon-arrow  icon-arrow--right"></span></a>
		</div>
		<div class="slider__counter">
			<span id="currentSlide">1</span>/<span id="lastSlide">6</span>
		</div>
	</div>
	<script>
		(function(){
			document.addEventListener('DOMContentLoaded', function(event){
	            $('#lastSlide').html($('#newsSlider .swiper-slide').length);
				var currentSlideIndex = 0,
					isDragged = false,
					imagesSwiper = new Swiper('#newsSlider', {
						speed: 1100,
						effect: 'fade',
						fadeEffect: {
						    crossFade: true
						},
						loop: true,
						longSwipes: false,
						longSwipesMs: '100ms',
						on: {
							init: function() {
								$('#newsSliderWrap .js-slider-prev').on('click', function(){
									if (!isDragged) {
										$('#newsSlider .slider__wrap').addClass('animateCurtain-to-right');
										imagesSwiper.slidePrev(1100);
										setTimeout(function(){
											$('#newsSlider .slider__wrap').addClass('animateCurtain-hide-to-right');
											setTimeout(function(){
												$('#newsSlider .slider__wrap').removeClass('animateCurtain-hide-to-right').removeClass('animateCurtain-to-right');
												imagesSwiper.allowSlideNext = true;
												imagesSwiper.allowSlidePrev = true;
												imagesSwiper.allowTouchMove = true;
												isDragged = false;
											}, 500);
										}, 600);
									}
									if (imagesSwiper != undefined) {
										imagesSwiper.allowSlideNext = false;
										imagesSwiper.allowSlidePrev = false;
										imagesSwiper.allowTouchMove = false;
									}
								});
								$('#newsSliderWrap .js-slider-next').on('click', function(){
									if (!isDragged) {
										$('#newsSlider .slider__wrap').addClass('animateCurtain-to-left');
										imagesSwiper.slideNext(1100);
										setTimeout(function(){
											$('#newsSlider .slider__wrap').addClass('animateCurtain-hide-to-left');
											setTimeout(function(){
												$('#newsSlider .slider__wrap').removeClass('animateCurtain-hide-to-left').removeClass('animateCurtain-to-left');
												imagesSwiper.allowSlideNext = true;
												imagesSwiper.allowSlidePrev = true;
												imagesSwiper.allowTouchMove = true;
												isDragged = false;
											}, 500);
										}, 600);
									}
									if (imagesSwiper != undefined) {
										imagesSwiper.allowSlideNext = false;
										imagesSwiper.allowSlidePrev = false;
										imagesSwiper.allowTouchMove = false;
									}
								});
							},
							touchStart: function(touchstart){
								isDragged = true;
							},
							touchMove: function(touchMove) {
								if (imagesSwiper.touches.diff > 0) {
									var nextCurtainXOffset = ($('#newsSlider .js-slider-curtain').width() / 2) - imagesSwiper.touches.diff;
								} else {
									var nextCurtainXOffset = -($('#newsSlider .js-slider-curtain').width() / 2) - imagesSwiper.touches.diff;
								}
								$('#newsSlider .js-slider-curtain').css('transform', 'translate3d(' +  nextCurtainXOffset + 'px, 0, 0)');
							},
							touchEnd: function(touchEnd){
								if (imagesSwiper.touches.diff > 0) {
									$('#newsSlider .js-slider-curtain').css('transform', 'translate3d(-100%, 0, 0)').css('transition', 'transform 1s cubic-bezier(.55, .13, .46, .85)');
								} else if (imagesSwiper.touches.diff < 0) {
									$('#newsSlider .js-slider-curtain').css('transform', 'translate3d(100%, 0, 0)').css('transition', 'transform 1s cubic-bezier(.55, .13, .46, .85)');
								} else {
									isDragged = false;
								}
								setTimeout(function(){
									$('#newsSlider .js-slider-curtain').css('transition', 'unset');
									isDragged = false;
								}, 1000);
							},
							slideChange: function(){
								$('#newsSliderInfo').addClass('animate-hide-to-left');
	                            setTimeout(function(){
	                            	$('#newsLink').attr('href', $('#newsSlider .swiper-slide-active').attr('data-url'));
	                            	$('#newsTitle').html($('#newsSlider .swiper-slide-active').attr('data-title')).attr('href', $('#newsSlider .swiper-slide-active').attr('data-url'));
	                            	$('#newsDate').html($('#newsSlider .swiper-slide-active').attr('data-data'));
	                            	$('#currentSlide').html(imagesSwiper.realIndex + 1);
	                            	$('#newsSliderInfo').addClass('animate-visible-to-left');
	                            	$('#newsSliderInfo').removeClass('animate-hide-to-left');
	                            	setTimeout(function(){
										$('#newsSliderInfo').removeClass('animate-visible-to-left');
									},500);
	                            },500);
							}
						}
					});
			});
		})()
	</script>
<?endif;?>