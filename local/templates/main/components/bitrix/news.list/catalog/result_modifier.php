<?
$arSelect = [
	"ID",
	"NAME",
	"DETAIL_PAGE_URL",
	"PROPERTY_L_MATERIAL",
	"PROPERTY_S_DATE_END",
	"PROPERTY_S_ADRESS",
	"PROPERTY_L_STATUS",
	"PROPERTY_N_FLOORS",
	"PROPERTY_S_FACADE",
	"PROPERTY_F_PICTURE",
	"PROPERTY_S_TYPE",
	"PROPERTY_L_ACTIVE",
];
foreach($arResult["ITEMS"] as &$arItem){
	$rsElement = CIBlockElement::GetList([], ["PROPERTY_E_PROJECT" => $arItem["ID"], "IBLOCK_ID" => IBLOCK_ID_HOUSE], false, false, $arSelect);
	$arItem["HAS_ACTIVE"] = false;
	while($arElement = $rsElement->GetNext()){
		if(!empty($arElement["PROPERTY_F_PICTURE_VALUE"])){
			foreach($arElement["PROPERTY_F_PICTURE_VALUE"] as $pic_id)
				$arElement["PICTURE"][] = CFile::GetPath($pic_id);
		}
		if($arElement["PROPERTY_S_TYPE_VALUE"] == "PARKING")
			$arElement["DETAIL_PAGE_URL"] .= "parking/";
		$arItem["HOUSE"][] = $arElement;
		if($arElement["PROPERTY_L_ACTIVE_VALUE"] == "Y")
			$arItem["HAS_ACTIVE"] = true;
	}
}

$rsElement = CIBlockElement::GetList([], ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "PROPERTY_L_ARCHIVE_VALUE" => "Y"], false, ["nTopCount" => 1], ["ID"]);
if($arElement = $rsElement->Fetch()){
	$arResult["ARCHIVE"] = "Y";
}