<?if(!isset($_REQUEST["AJAX_REQUEST"])):?>
	<div class="news-list">
<?endif;?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="news-list__item one-news wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
		<div class="one-news__img">
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="one-news__link" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');">
			</a>
			<div class="one-news__date"><?=FormatDate("j F Y", MakeTimeStamp($arItem["DATE_CREATE"]))?></div>
		</div>
		<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="small one-news__title"><?=$arItem["NAME"]?><span class="icon-arrow icon-arrow--right"></span></a>
	</div>
<?endforeach;?>
<?if($arResult["NAV_RESULT"]->NavPageNomer < $arResult["NAV_RESULT"]->NavPageCount):?>
	<a
		href="#"
		class="button button--big-dark button--down button--rectangle-animate js-button-add-more"
		data-total="<?=$arResult["NAV_RESULT"]->NavPageCount?>"
		data-pagen="1"
		data-page="<?=$arResult["NAV_RESULT"]->NavPageNomer + 1;?>"
		>
		<span>больше новостей</span><span class="icon-arrow  icon-arrow--down"></span>
	</a>
<?endif;?>

<?if(!isset($_REQUEST["AJAX_REQUEST"])):?>
	</div>
<?endif;?>