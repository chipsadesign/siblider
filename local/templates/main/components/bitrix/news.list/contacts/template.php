<div class="contacts__main-row">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?if(mb_strtolower(trim($arItem["NAME"])) !== "отдел продаж"
            && mb_strtolower(trim($arItem["NAME"])) !== "приемная"
        ) continue;?>
            <div class="contacts__main-col">
                <h1 class="contacts__title wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".2s"><?=$arItem["NAME"]?></h1>
                <?foreach($arItem["PROPERTIES"]["S_CONTACTS"]["VALUE"] as $key => $val):?>
                    <?$desc = $arItem["PROPERTIES"]["S_CONTACTS"]["DESCRIPTION"][$key];?>
                    <div class="contacts__info-item wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".3s">
                        <div class="contacts__info-title"><?=$desc?></div>
                        <a href="<?=mb_strtolower($desc) == "e-mail" || mb_strtolower($desc) == "email" ? "mailto" : "tel";?>:<?=$val?>" class="contacts__info-value h1" itemprop="<?=mb_strtolower($desc) == "e-mail" || mb_strtolower($desc) == "email" ? "email" : "telephone";?>"><?=$val?></a>
                    </div>
                <?endforeach;?>
            </div>
    <?endforeach;?>
</div>
<?$this->SetViewTarget("other_contacts");?>
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?if(mb_strtolower($arItem["NAME"]) === "отдел продаж"
            || mb_strtolower($arItem["NAME"]) === "приемная"
        ) continue;?>
        <div class="contacts__info-item  wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
            <div class="contacts__info-title"><?=$arItem["NAME"]?></div>
            <?foreach($arItem["PROPERTIES"]["S_CONTACTS"]["VALUE"] as $key => $val):?>
                <?$desc = $arItem["PROPERTIES"]["S_CONTACTS"]["DESCRIPTION"][$key];?>
                <div class="contacts__info-value"><?=$desc?>: <br><a href="<?=mb_strtolower($desc) == "e-mail" || mb_strtolower($desc) == "email" ? "mailto" : "tel";?>:<?=$val?>"><?=$val?></a></div>
            <?endforeach;?>
        </div>
    <?endforeach;?>
<?$this->EndViewTarget();?>