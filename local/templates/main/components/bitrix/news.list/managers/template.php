<?if(!empty($arResult["ITEMS"])):?>
    <div class="contacts__managers">
        <h2 class="contacts__managers-title h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">ваши менеджеры</h2>
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?$arProps = $arItem["PROPERTIES"];?>
            <div class="contacts__manager manager wow fadeInUp js-manager" data-wow-delay=".2s" data-wow-duration=".5s">
                <div class="manager__img">
                    <?if($arItem["PREVIEW_PICTURE"]["SRC"]):?>
                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                    <?endif;?>
                    <?if($arProps["S_POSITION"]["VALUE"]):?>
                        <div class="manager__post"><?=$arProps["S_POSITION"]["VALUE"]?></div>
                    <?endif;?>
                </div>
                <div class="manager__name"><?=$arItem["NAME"]?></div>
                <div class="manager__contacts">
                    <?if($arProps["S_PHONE"]["VALUE"]):?>
                        <div class="manager__contacts-item">тел: <a href="tel:<?=$arProps["S_PHONE"]["VALUE"]?>"><?=$arProps["S_PHONE"]["VALUE"]?></a></div>
                    <?endif;?>
                    <?if($arProps["S_EMAIL"]["VALUE"]):?>
                        <div class="manager__contacts-item">e-mail: <a href="mailto:<?=$arProps["S_EMAIL"]["VALUE"]?>"><?=$arProps["S_EMAIL"]["VALUE"]?></a></div>
                    <?endif;?>
                </div>
                <?if(!empty($arResult["HOUSES"][$arItem["ID"]])):?>
                    <div class="manager__objects">
                        <div class="manager__objects-title js-open-button">ведение объектов <span class="icon-triangle"></span></div>
                        <div class="manager__objects-dropdown">
                            <div class="manager__objects-dropdown-wrap">
                                <?foreach($arResult["HOUSES"][$arItem["ID"]] as $arHouse):?>
                                    <a href="<?=$arHouse["URL"]?>"><?=$arHouse["NAME"]?></a>
                                <?endforeach;?>
                            </div>
                        </div>
                    </div>
                <?endif;?>
            </div>
        <?endforeach;?>
    </div>
<?endif;?>