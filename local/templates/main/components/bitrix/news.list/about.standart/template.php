<?if(!empty($arResult["ITEMS"])):?>
	<div class="stardards" id="standart">
		<h2 class="h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">стандарты</h2>
		<div class="standards__list js-stardards-list wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
			<?foreach($arResult["ITEMS"] as $key => $arItem):?>
				<div class="standards__item  js-dropdown dropdown">
					<div class="dropdown__header js-open-button">
						<div class="dropdown__title">
							<span><?=$key > 9 ? "" : 0;?><?=$key + 1?></span> <?=$arItem["NAME"]?>
						</div>
						<span class="icon-triangle"></span>
					</div>
					<div class="dropdown__content"><?=$arItem["PREVIEW_TEXT"]?></div>
				</div>
			<?endforeach;?>
		</div>
		<script>
			(function(){
				document.addEventListener('DOMContentLoaded', function(event){
					$('.js-open-button').on('click', function(){
	                    $(this).toggleClass('is-opened');
	                    $(this).siblings().slideToggle(300);
	                });

	                $(window).on('scroll', function(){
	                	if (($(window).scrollTop() + $(window).height()) >= $('.js-stardards-list').offset().top) {
	                		$('.js-stardards-list').addClass('is-in-viewport');
	                	}
	                });

	                $('.js-dropdown').each(function(index){
	                    $(this).css('animation-delay', ((index + 1) * 0.1) + 's');
	                });
	            });
	        })()
		</script>
	</div>
<?endif;?>