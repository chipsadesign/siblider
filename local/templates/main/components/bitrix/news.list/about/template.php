<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
		$props = $arItem["PROPERTIES"];
		$dprops = $arItem["DISPLAY_PROPERTIES"];
		$type = $props["L_TYPE"]["VALUE_XML_ID"];
	?>

	<?if($type == "OL_LIST"):?>
		<h2 class="big about__title wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s" id="target_<?=$arItem["ID"]?>"><?=$props["S_HEADER"]["VALUE"]?></h2>
		<div class="list">
			<div class="list__title wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"><?=$props["S_SUBHEADER"]["VALUE"]?></div>
			<ol class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".4s">
				<?foreach($props["M_TEXT"]["VALUE"] as $value):?>
					<li><?=$value["TEXT"]?></li>
				<?endforeach;?>
			</ol>
		</div>
	<?endif;?>

	<?if($type == "PARALAX_RIGHT" || $type == "PARALAX_LEFT"):?>
		<div id="target_<?=$arItem["ID"]?>" class="parallax-img <?=$type == "PARALAX_LEFT" ? "about__first-img" : "about__second-img"?>">
			<div class="js-parallax-img parallax-img__img" data-direction="<?=$type == "PARALAX_LEFT" ? "1" : "-1"?>" data-speed="1" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');"></div>
		</div>
	<?endif;?>

	<?if($type == "SL_TEXT"):?>
		<div class="dark-slider swiper-container" id="aboutTextSlider<?=$arItem["ID"]?>">
			<div class="swiper-wrapper">
				<?foreach($props["M_TEXT"]["VALUE"] as $value):?>
					<div class="dark-slider__item swiper-slide">
						<div class="dark-slider__text wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"><?=$value["TEXT"]?></div>
					</div>
				<?endforeach;?>
			</div>
			<div class="dark-slider__dots"></div>
		</div>
		<script>
			(function(){
				document.addEventListener('DOMContentLoaded', function(event){
					$('body').addClass('light');

					var textSlider = new Swiper('#aboutTextSlider<?=$arItem["ID"]?>', {
						speed: 500,
						effect: 'slide',
						loop: true,
						autoplay: {
						    delay: 4000,
						},
						pagination: {
						    el: '.dark-slider__dots',
						    type: 'bullets',
							bulletClass: 'dark-slider__dot',
							bulletActiveClass: 'active'
						},
					});
				});
			})()
		</script>
	<?endif;?>

	<?if($type == "TXT"):?>
		<div id="target_<?=$arItem["ID"]?>" class="about__text wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"><?=$arItem["PREVIEW_TEXT"]?></div>
	<?endif;?>

	<?if($type == "SL_IMG"):?>
		<div id="target_<?=$arItem["ID"]?>" class="slider  about__slider" id="aboutImageSlider<?=$arItem["ID"]?>Wrap">
			<div class="slider__container swiper-container" id="aboutImageSlider<?=$arItem["ID"]?>">
				<div class="slider__wrap  swiper-wrapper">
					<?foreach($dprops["F_IMG"]["FILE_VALUE"] as $key => $val):?>
						<div data-title="<?=$dprops["F_IMG"]["DESCRIPTION"][$key]?>" class="slider__item swiper-slide parallax-img">
							<div class="js-parallax-img parallax-img__img js-image" data-direction="1" data-speed="1" style="background-image: url('<?=$val["SRC"]?>');"></div>
						</div>
					<?endforeach;?>
				</div>
				<div class="slider__curtain js-slider-curtain"></div>
			</div>
			<div class="slider__small-info"><span id="aboutImageSlider<?=$arItem["ID"]?>Info"></span></div>
			<div class="slider__nav slider__nav--triangle">
				<div class="slider__nav-item js-slider-prev"></div>
				<div class="slider__nav-item js-slider-next"></div>
			</div>
			<script>
				(function(){
					document.addEventListener('DOMContentLoaded', function(event){
						var currentSlideIndex = 0,
							isDragged = false,
							imagesSwiper = new Swiper('#aboutImageSlider<?=$arItem["ID"]?>', {
								speed: 1100,
								effect: 'fade',
								fadeEffect: {
								    crossFade: true
								},
								loop: true,
								longSwipes: false,
								longSwipesMs: '100ms',
								on: {
									init: function() {
										$('#aboutImageSlider<?=$arItem["ID"]?>Wrap .js-slider-prev').on('click', function(){
											if (!isDragged) {
												$('#aboutImageSlider<?=$arItem["ID"]?> .slider__wrap').addClass('animateCurtain-to-right');
												imagesSwiper.slidePrev(1100);
												setTimeout(function(){
													$('#aboutImageSlider<?=$arItem["ID"]?> .slider__wrap').addClass('animateCurtain-hide-to-right');
													setTimeout(function(){
														$('#aboutImageSlider<?=$arItem["ID"]?> .slider__wrap').removeClass('animateCurtain-hide-to-right').removeClass('animateCurtain-to-right');
														imagesSwiper.allowSlideNext = true;
														imagesSwiper.allowSlidePrev = true;
														imagesSwiper.allowTouchMove = true;
														isDragged = false;
													}, 500);
												}, 600);
											}
											if (imagesSwiper != undefined) {
												imagesSwiper.allowSlideNext = false;
												imagesSwiper.allowSlidePrev = false;
												imagesSwiper.allowTouchMove = false;
											}
										});
										$('#aboutImageSlider<?=$arItem["ID"]?>Wrap .js-slider-next').on('click', function(){
											if (!isDragged) {
												$('#aboutImageSlider<?=$arItem["ID"]?> .slider__wrap').addClass('animateCurtain-to-left');
												imagesSwiper.slideNext(1100);
												setTimeout(function(){
													$('#aboutImageSlider<?=$arItem["ID"]?> .slider__wrap').addClass('animateCurtain-hide-to-left');
													setTimeout(function(){
														$('#aboutImageSlider<?=$arItem["ID"]?> .slider__wrap').removeClass('animateCurtain-hide-to-left').removeClass('animateCurtain-to-left');
														imagesSwiper.allowSlideNext = true;
														imagesSwiper.allowSlidePrev = true;
														imagesSwiper.allowTouchMove = true;
														isDragged = false;
													}, 500);
												}, 600);
											}
											if (imagesSwiper != undefined) {
												imagesSwiper.allowSlideNext = false;
												imagesSwiper.allowSlidePrev = false;
												imagesSwiper.allowTouchMove = false;
											}
										});
										$('#aboutImageSlider<?=$arItem["ID"]?>Info').html($('#aboutImageSlider<?=$arItem["ID"]?> .swiper-slide:first-child').attr('data-title'));
									},
									touchStart: function(touchstart){
										isDragged = true;
									},
									touchMove: function(touchMove) {
										if (imagesSwiper.touches.diff > 0) {
											var nextCurtainXOffset = ($('#aboutImageSlider<?=$arItem["ID"]?> .js-slider-curtain').width() / 2) - imagesSwiper.touches.diff;
										} else {
											var nextCurtainXOffset = -($('#aboutImageSlider<?=$arItem["ID"]?> .js-slider-curtain').width() / 2) - imagesSwiper.touches.diff;
										}
										$('#aboutImageSlider<?=$arItem["ID"]?> .js-slider-curtain').css('transform', 'translate3d(' +  nextCurtainXOffset + 'px, 0, 0)');
									},
									touchEnd: function(touchEnd){
										if (imagesSwiper.touches.diff > 0) {
											$('#aboutImageSlider<?=$arItem["ID"]?> .js-slider-curtain').css('transform', 'translate3d(-100%, 0, 0)').css('transition', 'transform 1s cubic-bezier(.55, .13, .46, .85)');
										} else if (imagesSwiper.touches.diff < 0) {
											$('#aboutImageSlider<?=$arItem["ID"]?> .js-slider-curtain').css('transform', 'translate3d(100%, 0, 0)').css('transition', 'transform 1s cubic-bezier(.55, .13, .46, .85)');
										} else {
											isDragged = false;
										}
										setTimeout(function(){
											$('#aboutImageSlider<?=$arItem["ID"]?> .js-slider-curtain').css('transition', 'unset');
											isDragged = false;
										}, 1000);
									},
									slideChange: function(e){
										$('#aboutImageSlider<?=$arItem["ID"]?>Info').addClass('animate-hide-to-left');
			                            setTimeout(function(){
			                            	$('#aboutImageSlider<?=$arItem["ID"]?>Info').html($('#aboutImageSlider<?=$arItem["ID"]?> .swiper-slide-active').attr('data-title'));
			                            	$('#aboutImageSlider<?=$arItem["ID"]?>Info').addClass('animate-visible-to-left');
			                            	$('#aboutImageSlider<?=$arItem["ID"]?>Info').removeClass('animate-hide-to-left');
			                            	setTimeout(function(){
												$('#aboutImageSlider<?=$arItem["ID"]?>Info').removeClass('animate-visible-to-left');
											},500);
			                            },500);
									},
								}
							});
					});
				})()
			</script>
		</div>
	<?endif;?>

	<?if($type == "UL_LIST"):?>
		<h2 id="target_<?=$arItem["ID"]?>" class="big wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"><?=$props["S_HEADER"]["VALUE"]?></h2>
		<div class="list">
			<div class="list__title  list__title--small wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"><?=$props["S_SUBHEADER"]["VALUE"]?></div>
			<ul class="list__without-dots wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".4s">
				<?foreach($props["M_TEXT"]["VALUE"] as $value):?>
					<li class="small"><?=$value["TEXT"]?></li>
				<?endforeach;?>
			</ul>
		</div>
	<?endif;?>
<?endforeach;?>

<script>
	(function(){
		document.addEventListener('DOMContentLoaded', function(event){
			var imagesArray = [];
			initParallax();
			$(window).on('scroll', function() {
				setPosition('.js-parallax-img');
			});

			$(window).resize(function(){
				initParallax();
			});
		});
	})()
</script>