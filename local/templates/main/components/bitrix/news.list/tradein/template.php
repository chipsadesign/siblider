<?if(!empty($arResult["ITEMS"])):?>
    <div class="tradein__list">
        <?=$arResult["DESCRIPTION"]?>
        <?foreach($arResult["ITEMS"] as $key => $arItem):?>
            <div class="tradein__list-item  tradein__list-item--vertical wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".1s">
                <?if($key < count($arResult["ITEMS"]) - 1):?>
                    <span class="icon-arrow"></span>
                <?endif;?>
                <div class="tradein__list-icon">
                    <img src="<?=$arItem["DISPLAY_PROPERTIES"]["F_ICO"]["FILE_VALUE"]["SRC"]?>" alt="">
                </div>
                <div class="tradein__list-content">
                    <div class="tradein__list-title"><?=$arItem["NAME"]?></div>
                    <div class="small"><?=$arItem["PREVIEW_TEXT"]?></div>
                </div>
            </div>
        <?endforeach;?>
    </div>
<?endif;?>