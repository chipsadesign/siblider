<? if (!isset($_REQUEST["AJAX_REQUEST"])) : ?>
	<div class="calculator__mortgage-list" id="mortgageList">
	<? endif; ?>

	<? foreach ($arResult["ITEMS"] as $arItem) : ?>
		<?
		$prcnt = $arItem["PROPERTIES"]["N_PRCNT"]["VALUE"];
		$ps = ($prcnt / 100) / 12;
		$sk = $_REQUEST["CREDIT"] ? $_REQUEST["CREDIT"] : $arParams["FILTER_DEF"]["price"] - $arParams["FILTER_DEF"]["first"];
		$kp = $_REQUEST["TIME"] ? $_REQUEST["TIME"] * 12 : $arParams["FILTER_DEF"]["long"] * 12;
		$pay = number_format($sk * $ps / (1 - (1 / pow(1 + $ps, $kp))), 0, ".", " ");
		if ($pay == 0)
			continue;
		?>
		<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="calculator__mortgage-item mortgage-item wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
			<div class="mortgage-item__header">
				<div class="mortgage-item__logo">
					<img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="logo">
				</div>
				<div class="mortgage-item__title">
					<div class="mortgage-item__bank"><?= $arItem["NAME"] ?></div>
					<? if ($arItem["PROPERTIES"]["S_SUBHEAD"]["VALUE"]) : ?>
						<div class="mortgage-item__name"><?= $arItem["PROPERTIES"]["S_SUBHEAD"]["VALUE"] ?></div>
					<? endif; ?>
				</div>
			</div>
			<div class="mortgage-item__content">
				<div class="mortgage-item__info">
					<div class="mortgage-item__info-title">платеж в месяц</div>
					<div class="mortgage-item__info-value"><?= $pay ?></div>
				</div>
				<div class="mortgage-item__info">
					<div class="mortgage-item__info-title">ставка</div>
					<div class="mortgage-item__info-value"><?= $arItem["PROPERTIES"]["N_PRCNT"]["VALUE"] ?>%</div>
				</div>
				<div class="mortgage-item__info">
					<div class="mortgage-item__info-title">срок до</div>
					<div class="mortgage-item__info-value"><?= $arItem["PROPERTIES"]["N_YEARS"]["VALUE"] ?> лет</div>
				</div>
				<span class="icon-arrow icon-arrow--right"></span>
			</div>
		</a>
	<? endforeach; ?>

	<a href="/calculator-ipoteka/domrf/" class="calculator__mortgage-item mortgage-item">
		<div class="mortgage-item__header">
			<div class="mortgage-item__logo">
				<img src="<?=SITE_TEMPLATE_PATH?>/images/bank/domrf.png" alt="logo">
			</div>
			<div class="mortgage-item__title">
				<div class="mortgage-item__bank">Бизнес-ипотека ДОМ.РФ</div>
				<div class="mortgage-item__name">Залог приобретаемой коммерческой недвижимости</div>
			</div>
		</div>
		<div class="mortgage-item__content">

			<div class="mortgage-item__info">
				<div class="mortgage-item__info-title">ставка</div>
				<div class="mortgage-item__info-value">7.5%</div>
			</div>
			<div class="mortgage-item__info">
				<div class="mortgage-item__info-title">срок до</div>
				<div class="mortgage-item__info-value">10 лет</div>
			</div>
			<span class="icon-arrow icon-arrow--right"></span>
		</div>
	</a>

	<? if (!isset($_REQUEST["AJAX_REQUEST"])) : ?>
	</div>
<? endif; ?>