<?if(!empty($arResult["YEARS"]) && !empty($arResult["AWARDS"])):?>
	<div class="awards">
		<div class="awards__header">
			<h2 class="h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">награды и достижения</h2>
			<?if(count($arResult["AWARDS"]) > 2):?>
				<div class="awards__select h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s" id="awardsOpen"><span>все </span><span class="icon-triangle"></span></div>
			<?endif;?>
		</div>
		<div class="awards__main-list">
			<?$cnt = 0;?>
			<?foreach($arResult["YEARS"] as $arYear):?>
				<?if(!empty($arResult["AWARDS"][$arYear["ID"]])):?>
					<div class="awards__inner-list  js-awards-list wow fadeIn" data-wow-duration=".1s" data-wow-delay="0s">
						<div class="awards__date h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"><?=$arYear["NAME"]?> год</div>
						<div class="awards__item">
							<?foreach($arResult["AWARDS"][$arYear["ID"]] as $award):?>
								<div class="small wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s"><?=$award?></div>
							<?endforeach;?>
						</div>
					</div>
					<?$cnt++?>
					<?if($cnt == 2) break;?>
				<?endif;?>
			<?endforeach;?>
		</div>
		<?if(count($arResult["AWARDS"]) > 2):?>
			<div class="awards__hidden-list" id="awardsHiddenList">
				<?$cnt = 0;?>
				<?foreach($arResult["YEARS"] as $arYear):?>
					<?if(!empty($arResult["AWARDS"][$arYear["ID"]])):?>
						<?$cnt++?>
						<?if($cnt < 3) continue;?>
						<div class="awards__inner-list  js-awards-list wow fadeIn" data-wow-duration=".1s" data-wow-delay="0s">
							<div class="awards__date h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"><?=$arYear["NAME"]?> год</div>
							<div class="awards__item">
								<?foreach($arResult["AWARDS"][$arYear["ID"]] as $award):?>
									<div class="small wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s"><?=$award?></div>
								<?endforeach;?>
							</div>
						</div>
					<?endif;?>
				<?endforeach;?>
			</div>
		<?endif;?>
		<script>
			(function(){
				document.addEventListener('DOMContentLoaded', function(event){
					$('#awardsOpen').on('click', function(){
						$(this).addClass('is-active');
	                    $(this).toggleClass('is-opened');
	                    var self = this;
						setTimeout(function(){
							if ($(self).hasClass('is-opened')) {
								$('#awardsOpen span:first-child').text('свернуть');
							} else {
								$('#awardsOpen span:first-child').text('все');
							}

							$(self).removeClass('is-active');
						}, 300);
	                    $('#awardsHiddenList').slideToggle(300);
	                });

	                $(window).on('scroll', function(){
	                    if (($(window).scrollTop() + $(window).height()) >= $('.js-awards-list').offset().top) {
	                        $('.js-awards-list').addClass('is-in-viewport');
	                    }
	                });
	            });
	        })()
		</script>
	</div>
<?endif;?>