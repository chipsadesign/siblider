<?if(isset($_REQUEST["AJAX_REQUEST"])):?>
    <?$house = -1;?>
    <?
        $isFirst = true;
    ?>
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?if($arItem["PROPERTIES"]["S_STATUS"]["VALUE"] != "AVAILABLE") continue;?>
        <?if(isset($_REQUEST["status_active"]) && $arItem["PROPERTIES"]["S_STATUS"]["VALUE"] !== "AVAILABLE") continue;?>

        <?if((!isset($rq["house_id"]) || $rq["house_id"] == 0) && $house != $arItem["PROPERTIES"]["E_HOUSE"]["VALUE"]):?>
            <?if(!$isFirst):?>
                </div>
            <?endif?>
            <?$isFirst = false?>
            <div class="table__row light">
                <div class="table__link">
                    <div class="table__name"><?=$arResult["HOUSE_NAMES"][$arItem["PROPERTIES"]["E_HOUSE"]["VALUE"]];?></div>
                </div>
            </div>
            <div class="table__section" data-sort-section>
        <?endif;?>

        <?$house = $arItem["PROPERTIES"]["E_HOUSE"]["VALUE"];?>
        <div class="table__row" data-sort-row>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="table__link">
                <div class="table__room-number"><span>квартира: </span>№ <?=$arItem["NAME"]?></div>
                <div class="table__floor"><span>этаж: </span><?=$arItem["PROPERTIES"]["N_FLOOR"]["VALUE"]?></div>
                <div class="table__rooms"><span>комнат</span><span><?=$arItem["PROPERTIES"]["N_ROOMS"]["VALUE"]?></span></div>
                <div class="table__square"><span>площадь от: </span><?=$arItem["PROPERTIES"]["N_AREA"]["VALUE"]?> м<sup>2</sup></div>
                <div class="table__cost" data-sort-price data-sort-value="<?=$arItem["PROPERTIES"]["N_PRICE"]["VALUE"]?>"><span>цена от: </span><?=number_format($arItem["PROPERTIES"]["N_PRICE"]["VALUE"], 0, ".", " ")?> ₽</div>
            </a>
            <div class="table__booking">
                <a data-fancybox data-src="#bookModal" href="" class="choose-flat__book js-flatBook" data-flat="<?=$arResult["HOUSE_NAMES"][$arItem["PROPERTIES"]["E_HOUSE"]["VALUE"]]?>, <?=$arItem["NAME"]?>">
                    <span>забронировать на 24 часа</span>
                </a>
            </div>
        </div>
    <?endforeach;?>
        </div>
<?endif;?>