<?if(isset($_REQUEST["AJAX_REQUEST"])):?>
    <?
    $arStatus = [
        "AVAILABLE" => "building__flat--sales",
        "BOOKED" => "building__flat--booked"
    ];
    ?>

    <div class="building__wrap">
        <div class="building__floors">
            <?$floors = array_reverse($arResult["FLOOR"]);?>
            <?foreach($floors as $floor):?>
                <div><?=$floor?></div>
            <?endforeach;?>
        </div>
        <div class="building__block">
            <?foreach($floors as $floor):?>
                <div class="building__floor">
                    <?foreach($arResult["FLOOR_FLAT"][$floor] as $arItem):?>
                        <?
                            $curStatus = $arItem["PROPERTIES"]["S_STATUS"]["VALUE"];
                            $status = "";
                            if(isset($arStatus[$curStatus]))
                                $status = $arStatus[$curStatus];
                        ?>
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="building__flat <?=$status?>"><?if($curStatus == "AVAILABLE"):?><?=$arItem["PROPERTIES"]["N_ROOMS"]["VALUE"]?><?endif;?></a>
                    <?endforeach;?>
                </div>
            <?endforeach;?>
        </div>
    </div>
<?endif;?>