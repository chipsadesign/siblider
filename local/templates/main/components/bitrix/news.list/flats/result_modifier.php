<?
// Дома
$rsHouse = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_HOUSE, "ACTIVE" => "Y", "!PROPERTY_S_TYPE" => "PARKING"], false, false, ["ID", "NAME"]);
while($arHouse = $rsHouse->Fetch()){
    if(CSite::InDir("/business/")){
        $rsFlat = CIBlockElement::GetList(
            [],
            [
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "PROPERTY_L_COMMERCE_VALUE" => "Y",
                "!PROPERTY_N_PRESET_CODE" => "кл",
                "ACTIVE" => "Y",
                "PROPERTY_E_HOUSE" => $arHouse["ID"],
                "!PROPERTY_S_STATUS" => STOP_BUSINESS_STATUS
            ],
            false,
            [
                "nTopCount" => 1
            ],
            [
                "ID"
            ]
        );
        if($arFlat = $rsFlat->Fetch()){
            $arResult["HOUSE"][] = $arHouse;
            $arHouseIDs[] = $arHouse["ID"];
        }
    }else{
        $rsFlat = CIBlockElement::GetList(
            [],
            [
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "!PROPERTY_L_COMMERCE_VALUE" => "Y",
                "!PROPERTY_N_PRESET_CODE" => "кл",
                "ACTIVE" => "Y",
                "PROPERTY_E_HOUSE" => $arHouse["ID"]
            ],
            false,
            [
                "nTopCount" => 1
            ],
            [
                "ID"
            ]
        );
        if($arFlat = $rsFlat->Fetch()){
            $arResult["HOUSE"][] = $arHouse;
            $arHouseIDs[] = $arHouse["ID"];
        }
    }
    $arResult["HOUSE_NAMES"][$arHouse["ID"]] = $arHouse["NAME"];
}

$minPrice == 0;
$maxPrice == 0;
foreach($arResult["ITEMS"] as $key => $arItem){
    if(in_array($arItem["PROPERTIES"]["E_HOUSE"]["VALUE"], $arHouseIDs)){
        if($arItem["PROPERTIES"]["N_PRICE"]["VALUE"] < $minPrice || $minPrice == 0){
            $minPrice = $arItem["PROPERTIES"]["N_PRICE"]["VALUE"];
        }
        if($arItem["PROPERTIES"]["N_PRICE"]["VALUE"] > $maxPrice || $maxPrice == 0){
            $maxPrice = $arItem["PROPERTIES"]["N_PRICE"]["VALUE"];
        }

        $arResult["FLOOR_FLAT"][$arItem["PROPERTIES"]["N_FLOOR"]["VALUE"]] = $arItem;
        $arResult["FLOOR"][] = $arItem["PROPERTIES"]["N_FLOOR"]["VALUE"];
        $arResult["AREA"][] = $arItem["PROPERTIES"]["N_AREA"]["VALUE"];
        $arResult["ROOMS"][] = $arItem["PROPERTIES"]["N_ROOMS"]["VALUE"];
    }
}

$arResult["MIN_PRICE"] = $minPrice;
$arResult["MAX_PRICE"] = $maxPrice;

$arResult["ROOMS"] = array_unique($arResult["ROOMS"]);
sort($arResult["ROOMS"]);

$arResult["AREA"] = array_unique($arResult["AREA"]);
sort($arResult["AREA"]);

$arResult["FLOOR"] = array_unique($arResult["FLOOR"]);
sort($arResult["FLOOR"]);