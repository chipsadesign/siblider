<?if(!isset($rq["AJAX_REQUEST"])):?>
    <form class="form wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s" id="filterForm" data-file="<?=$_SERVER["DOCUMENT_ROOT"].$this->GetFolder()."/filter.php"?>">
<?endif;?>
        <?
            $flatType = "flat";
            if(CSite::InDir("/business/")){
                $flatType = "commerc";

            }
            if(isset($_REQUEST["flats_type"]))
                $flatType = $_REQUEST["flats_type"];
        ?>
        <input type="hidden" name="SORT[PROPERTY_E_HOUSE]" value="ASC">
        <input type="hidden" name="SORT[PROPERTY_N_PRICE]" value="ASC">
        <input type="hidden" name="flats_type" value="<?=$flatType?>" class="js-flatType">
        <?if(isset($arResult["ROOMS"]) && count($arResult["ROOMS"]) > 1):?>
            <div class="form__item js-form-item">
                <div class="form__item-title">кол-во комнат</div>
                <div class="form__item-content  form__item-content--flex">
                    <?foreach($arResult["ROOMS"] as $room):?>
                        <label class="input-label  input-label--checkbox">
                            <input type="checkbox" name="rooms[]" class="checkbox" value="<?=$room;?>">
                            <span class="checkbox-custom  checkbox-custom--number  checkbox-custom--brown" data-hoverContent="<?=$room?>" data-content="<?=$room?>"></span>
                        </label>
                    <?endforeach;?>
                </div>
            </div>
        <?endif;?>
        <?if(isset($arResult["AREA"]) && ceil($arResult["AREA"][0]) != ceil($arResult["AREA"][count($arResult["AREA"]) - 1])):?>
            <div class="form__item js-form-item">
                <input type="hidden" name="min_area">
                <input type="hidden" name="max_area">
                <div class="form__item-title">площадь</div>
                <div class="form__item-content">
                    <div class="custom-range">
                        <div class="custom-range__values">
                            <div id="rangeSquareFirstValue"><?=floor($arResult["AREA"][0])?></div><div id="rangeSquareLastValue"><?=ceil($arResult["AREA"][count($arResult["AREA"]) - 1])?></div>
                        </div>
                        <div id="rangeSquare" data-step="1" data-min="<?=floor($arResult["AREA"][0])?>" data-max="<?=ceil($arResult["AREA"][count($arResult["AREA"]) - 1])?>" class="js-range"></div>
                    </div>
                    <script>
                        (function(){
                            <?if(!isset($rq["AJAX_REQUEST"])):?>
                                document.addEventListener('DOMContentLoaded', function(event){
                            <?endif;?>
                                var sliderSquare = document.getElementById('rangeSquare');
                                initNoUiSliderTwoPoint(sliderSquare);
                                var sliderValues = [
                                    document.getElementById('rangeSquareFirstValue'),
                                    document.getElementById('rangeSquareLastValue')
                                ];
                                sliderSquare.noUiSlider.on('update', function( values, handle ) {
                                    sliderValues[handle].innerHTML = values[handle];
                                    $('[name="min_area"]').val(values[0]);
                                    $('[name="max_area"]').val(values[1]);
                                });
                                sliderSquare.noUiSlider.on('change', function( values, handle ) {
                                    $('#filterReset').show().addClass('is-visible');
                                    $('#filterForm').addClass('is-filled');
                                    $('#filterForm').submit();
                                });
                            <?if(!isset($rq["AJAX_REQUEST"])):?>
                                });
                            <?endif;?>
                        })();
                    </script>
                </div>
            </div>
        <?endif;?>
        <?if(isset($arResult["FLOOR"]) && count($arResult["FLOOR"]) > 1):?>
            <div class="form__item js-form-item">
                <input type="hidden" name="min_floor">
                <input type="hidden" name="max_floor">
                <div class="form__item-title">этаж</div>
                <div class="form__item-content">
                    <div class="custom-range">
                        <div class="custom-range__values">
                            <div id="rangeFloorFirstValue"><?=floor($arResult["FLOOR"][0])?></div><div id="rangeFloorLastValue"><?=ceil($arResult["FLOOR"][count($arResult["FLOOR"]) - 1])?></div>
                        </div>
                        <div id="rangeFloor" data-step="<?=floor($arResult["FLOOR"][0])?>"  data-min="<?=floor($arResult["FLOOR"][0])?>" data-max="<?=ceil($arResult["FLOOR"][count($arResult["FLOOR"]) - 1])?>"  class="js-range"></div>
                    </div>
                    <script>
                        (function(){
                            <?if(!isset($rq["AJAX_REQUEST"])):?>
                                document.addEventListener('DOMContentLoaded', function(event){
                            <?endif;?>
                                var sliderFloor = document.getElementById('rangeFloor');
                                initNoUiSliderTwoPoint(sliderFloor);
                                var sliderValues = [
                                    document.getElementById('rangeFloorFirstValue'),
                                    document.getElementById('rangeFloorLastValue')
                                ];
                                sliderFloor.noUiSlider.on('update', function( values, handle ) {
                                    sliderValues[handle].innerHTML = values[handle];
                                    $('[name="min_floor"]').val(values[0]);
                                    $('[name="max_floor"]').val(values[1]);
                                });
                                sliderFloor.noUiSlider.on('change', function( values, handle ) {
                                    $('#filterReset').show().addClass('is-visible');
                                    $('#filterForm').addClass('is-filled');
                                    $('#filterForm').submit();
                                });
                            <?if(!isset($rq["AJAX_REQUEST"])):?>
                                });
                            <?endif;?>
                        })()
                    </script>
                </div>
            </div>
        <?endif;?>
        <?if(isset($arResult["MIN_PRICE"]) && isset($arResult["MAX_PRICE"])):?>
            <div class="form__item js-form-item">
                <div class="form__item-title">цена</div>
                <div class="form__item-content  form__item-content--flex form__item-content--flex-start">
                    <?if($arResult["MIN_PRICE"] < 2000000):?>
                        <label class="input-label  input-label--checkbox">
                            <input type="checkbox" name="price_range[]" class="checkbox" value="0-2000000">
                            <span class="checkbox-custom  checkbox-custom--mark  checkbox-custom--brown"></span>
                            <div>до 2 млн</div>
                        </label>
                    <?endif;?>
                    <?if($arResult["MIN_PRICE"] < 4000000 && $arResult["MAX_PRICE"] > 2000000):?>
                        <label class="input-label  input-label--checkbox">
                            <input type="checkbox" name="price_range[]" class="checkbox" value="2000000-4000000">
                            <span class="checkbox-custom  checkbox-custom--mark  checkbox-custom--brown"></span>
                            <div>от 2 до 4 млн</div>
                        </label>
                    <?endif;?>
                    <?if($arResult["MIN_PRICE"] < 6000000 && $arResult["MAX_PRICE"] > 4000000):?>
                        <label class="input-label  input-label--checkbox">
                            <input type="checkbox" name="price_range[]" class="checkbox" value="4000000-6000000">
                            <span class="checkbox-custom  checkbox-custom--mark  checkbox-custom--brown"></span>
                            <div>от 4 до 6 млн</div>
                        </label>
                    <?endif;?>
                    <?if($arResult["MAX_PRICE"] > 6000000):?>
                        <label class="input-label  input-label--checkbox">
                            <input type="checkbox" name="price_range[]" class="checkbox" value="6000000-999999999">
                            <span class="checkbox-custom  checkbox-custom--mark  checkbox-custom--brown"></span>
                            <div>от 6 млн</div>
                        </label>
                    <?endif;?>
                </div>
            </div>
        <?endif;?>
        <?/*<div class="form__item js-form-item">
            <label class="input-label input-label--checkbox">
                <input type="checkbox" name="status_active" class="checkbox" value="Y" <?=!isset($_REQUEST["AJAX_REQUEST"]) || $_REQUEST["status_active"] == "Y" ? "checked" : ""?>>
                <span class="checkbox-custom  checkbox-custom--brown  checkbox-custom--mark"></span>
                <span class="input-label__flat-text">только доступные в продаже</span>
            </label>
        </div>*/?>
        <div class="choose-flat__counter tablet-hide js-flatCount" id="flatsCounter"><?=count($arResult["ITEMS"])?> <?=siteHandlers::getInclinationByNumber(count($arResult["ITEMS"]), ["подходящих вариант", "подходящих варианта", "подходящих вариантов"]);?></div>
        <button type="reset" style="display: none;" class="button js-form-item" id="filterReset">сбросить фильтр <span class="icon-close"></span></button>
        <script>
            (function(){
                <?if(!isset($rq["AJAX_REQUEST"])):?>
                    document.addEventListener('DOMContentLoaded', function(event){
                <?endif;?>
                    $('#filterForm input[type="checkbox"]').change(function(){
                        $('#filterReset').show().addClass('is-visible');
                        $('#filterForm').addClass('is-filled');
                        // filterChooseFlat();
                    });
                    $('#filterReset').on('click', function(e){
                        e.preventDefault();
                        //сброс чекбоксов
                        $('#filterForm input[type="checkbox"]').each(function(){
                            $(this).prop( "checked", false );
                        });
                        //сброс селектов
                        $('#filterForm .js-select').each(function(){
                            $(this).val(null).trigger('change').removeClass('is-filled');
                        });
                        //сброс бегунков
                        $('#filterForm .js-range').each(function(){
                            $(this)[0].noUiSlider.reset();
                        });
                        // filterChooseFlat();
                        $('#filterReset').removeClass('is-visible');
                        setTimeout(function(){
                            $('#filterReset').hide();
                            $('#filterForm').removeClass('is-filled');
                            $('#filterForm').submit();
                        }, 500);
                    });
                    $('#flatsCounter').on('click', function(){
                        $.fancybox.close();
                    });
                <?if(!isset($rq["AJAX_REQUEST"])):?>
                    });
                <?endif;?>
            })()
        </script>
        <script>
            (function(){
                <?if(!isset($rq["AJAX_REQUEST"])):?>
                    document.addEventListener('DOMContentLoaded', function(event){
                <?endif;?>
                    /*$('#chooseflatCallChooseForm').on('click', function(){
                        var filterForm = $('#filterForm');
                        if (!filterForm.hasClass('is-visible')) {
                            filterForm.show().addClass('is-visible');
                            setTimeout(function(){
                                filterForm.find('.js-form-item').each(function(){$(this).addClass('is-visible');});
                            }, 500);
                        }
                        else {
                            filterForm.find('.js-form-item').each(function(){$(this).removeClass('is-visible');});
                            setTimeout(function(){
                                filterForm.removeClass('is-visible');
                                setTimeout(function(){
                                    filterForm.hide();
                                }, 500);
                            }, 500);
                        }
                    });*/
                <?if(!isset($rq["AJAX_REQUEST"])):?>
                    });
                <?endif;?>
            })()
        </script>
<?if(!isset($rq["AJAX_REQUEST"])):?>
    </form>
    <script>
        $(document).ready(function(){
            $('#filterForm').submit();
        });
    </script>
<?endif;?>