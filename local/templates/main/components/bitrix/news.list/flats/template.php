<?global ${$arParams["FILTER_NAME"]};?>
<div class="page__left-content">
	<div class="page__left-content-wrap">
		<div class="page__header page__header--with-border wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
			<h1 class="page__title">
				проект: <select class="custom-select" id="selectObject">
					<option value="0">Все</option>
					<?foreach ($arResult["HOUSE"] as $arHouse):?>
						<option <?=${$arParams["FILTER_NAME"]}["PROPERTY_E_HOUSE"] == $arHouse["ID"] ? "selected" : ""?> value="<?=$arHouse["ID"]?>"><?=$arHouse["NAME"]?></option>
					<?endforeach;?>
				</select>
				<script>
					(function(){
						document.addEventListener('DOMContentLoaded', function(event){
							initSelect2($('#selectObject'));
							<?if (!isset(${$arParams["FILTER_NAME"]}["PROPERTY_E_HOUSE"])):?>
								$('#changeView').fadeOut(300);
								$('.js-signUpFlat').fadeOut(300);
							<?endif;?>

							$('#selectObject').on('select2:select', function(e) {
								if ($('#selectObject').val() == 0) {
									$('#changeView.show-building--open').click();
									$('#changeView').fadeOut(300);
									$('.js-signUpFlat').fadeOut(300);
								} else {
									$('#changeView').fadeIn(300);
									$('.js-signUpFlat').fadeIn(300);
								}
							});

							initFancyBox();

							if ($(window).width() <= 1024) {
								$('.page #filterFormWrap').hide();
							}

							$(window).resize(function(){
								if ($(window).width() > 1024) {
									$('.page #filterFormWrap').show();
								} else {
									$('.page #filterFormWrap').hide();
								}
							});
						});
					})()
				</script>
			</h1>
			<div class="choose-flat__form-btn" data-fancybox="filter" data-src="#filterFormWrap"><span class="icon-range"></span></div>
		</div>
		<div class="choose-flat__filter">
			<div class="modal modal--filter" id="filterFormWrap">
				<div class="modal__wrap">
					<div class="modal__header"></div>
					<div class="modal__content">
						<?include $_SERVER["DOCUMENT_ROOT"].$this->GetFolder()."/filter.php"?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="page__right-content" id="fancyboxParent">
	<div class="table" data-sort-ajax-table>
		<div class="table__header wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s" id="tableHeader">
			<div class="table__header-wrap">
				<div class="table__in-sale js-building-label">продано</div>
				<div class="table__in-book js-building-label">забронировано</div>
				<div class="table__room-number js-table-label">№ <?=CSITE::InDir("/flats/") || $_REQUEST["flat_type"] == "flat" ? "квартиры" : ""?></div>
				<div class="table__floor js-table-label">этаж</div>
				<div class="table__rooms js-table-label">кол-во комнат</div>
				<div class="table__square js-table-label">площадь</div>
				<div class="table__cost js-table-label">
					<a href="#" class="sort-by link link--pseudo" data-sort-button="price">
						<div class="link__text">цена</div>
						<div class="sort-by__icon link__icon icon-triangle"></div>
					</a>
				</div>
			</div>
			<div class="choose-flat__links">
				<a href="#" class="show-building" id="changeView"><span class="icon-buildings-new"></span><span class="icon-list-new"></span></a>
				<a id="fancyMap" href="#modalMap" data-file="<?=$_SERVER["DOCUMENT_ROOT"].$this->GetFolder()."/map.php"?>"><span class="icon-pointer"></span></a>
			</div>
		</div>
		<div class="table__content wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".4s" id="tableContent">
			<div class="table__list" id="tableList" data-file="<?=$_SERVER["DOCUMENT_ROOT"].$this->GetFolder()."/flat_list.php"?>">
				<?include $_SERVER["DOCUMENT_ROOT"].$this->GetFolder()."/flat_list.php"?>
			</div>
			<div class="table__building building" id="building" style="display: none;" data-file="<?=$_SERVER["DOCUMENT_ROOT"].$this->GetFolder()."/flat_block.php"?>">
				<?include $_SERVER["DOCUMENT_ROOT"].$this->GetFolder()."/flat_block.php"?>
			</div>
		</div>
		<div class="preloader">
			<div class="preloader__item"></div>
			<div class="preloader__item"></div>
			<div class="preloader__item"></div>
		</div>
	</div>
	<div class="choose-flat__footer wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".4s">
		<a data-fancybox data-src="#demonstrationModal" href="javascript:;" class="button button--center-black js-signUpFlat" data-hoverContent="записаться на показ" data-content="записаться на показ"></a>
		<div class="choose-flat__counter js-flatCount"><?=count($arResult["ITEMS"])?> <?=siteHandlers::getInclinationByNumber(count($arResult["ITEMS"]), ["подходящих вариант", "подходящих варианта", "подходящих вариантов"]);?></div>
	</div>
	<script>
		(function(){
			document.addEventListener('DOMContentLoaded', function(event){
				$('#changeView').on('click', function(e){
					$(this).toggleClass('show-building--open');
					$('#tableHeader').toggleClass('white');
					if ($(this).hasClass('show-building--open')) {
						$('#tableContent').addClass('white');
						$('.js-table-label').slideToggle(250);
						$('#tableList').slideToggle(500);
						setTimeout(function(){
							$('.js-building-label').slideToggle(250);
						}, 250);
						setTimeout(function(){
							$('#building').slideToggle(500);
						}, 500);
						$('.page__left-content-wrap').addClass('no-scroll');
					} else {
						$('.js-building-label').slideToggle(250);
						$('#building').slideToggle(500);
						$('#tableContent').removeClass('white');
						setTimeout(function(){
							$('.js-table-label').slideToggle(250);
						}, 250);
						setTimeout(function(){
							$('#tableList').slideToggle(500);
						}, 500);
						$('.page__left-content-wrap').removeClass('no-scroll');
					}
				});
			});
		})()
	</script>
</div>

<div class="map-wrapper">
	<?include($_SERVER["DOCUMENT_ROOT"]."/".$this->getFolder()."/map.php");?>
</div>