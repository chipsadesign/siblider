<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arProps = $arResult["PROPERTIES"];

CModule::IncludeModule('iblock');
CModule::IncludeModule('highloadblock');
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$hlblock = HL\HighloadBlockTable::getById(2)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$arResult["SIDE_OF_WORLD"] = false;
$rsData = $entity_data_class::getList(array(
	"select" => ["*"],
	"order" => ["ID" => "ASC"],
	"filter" => ["UF_XML_ID" => $arProps["S_SIDE_OF_WORLD"]["VALUE"]]
));

$arSideOfWorld = $rsData->Fetch();
if(intval($arSideOfWorld["UF_FILE"]) > 0){
	$arResult["SIDE_OF_WORLD"] = CFile::GetPath($arSideOfWorld["UF_FILE"]);
}

$rsHouse = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_HOUSE, "ID" => $arResult["PROPERTIES"]["E_HOUSE"]["VALUE"]], false, false, ["ID", "NAME", "PROPERTY_E_PROJECT", "PROPERTY_S_ADRESS", "DETAIL_PAGE_URL", "CODE", "PROPERTY_N_FLOORS", "PROPERTY_S_DATE_START", "PROPERTY_S_DATE_END", "PROPERTY_N_ENTRANCE", "PROPERTY_L_MATERIAL", "PROPERTY_S_FACING"]);
if($arHouse = $rsHouse->GetNext()){
    $arResult["HOUSE"] = $arHouse;

    if($arHouse["PROPERTY_E_PROJECT_VALUE"]){
        $rsProject = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_OBJECT, "ID" => $arHouse["PROPERTY_E_PROJECT_VALUE"]], false, false, ["ID", "NAME"]);
        if($arProject = $rsProject->Fetch()){
            $arResult["PROJECT"] = $arProject;
        }
    }
}

$rsFloor = CIBlockElement::GetList(
	[],
	[
		"IBLOCK_ID" => IBLOCK_ID_FLOOR,
		"PROPERTY_E_HOUSE" => $arResult["HOUSE"]["ID"],
		"PROPERTY_S_FLOOR" => $arResult["PROPERTIES"]["N_FLOOR"]["VALUE"],
	],
	false,
	false,
	[
		"ID",
		"PREVIEW_PICTURE"
	]
);
if($arFloor = $rsFloor->Fetch()){
	$arResult["FLOOR_ID"] = $arFloor["PREVIEW_PICTURE"];
	$arResult["FLOOR"] = CFile::GetPath($arFloor["PREVIEW_PICTURE"]);
}

if($arProps["N_PRESET_CODE"]["VALUE"]){
    $rsSameFlat = CIBlockElement::GetList(["PROPERTY_N_PRICE" => "ASC"], ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "PROPERTY_E_HOUSE" => $arProps["E_HOUSE"]["VALUE"], "PROPERTY_N_PRESET_CODE" => $arProps["N_PRESET_CODE"]["VALUE"], "PROPERTY_S_STATUS" => "AVAILABLE"], false, false, ["ID", "NAME", "PROPERTY_F_PRESET", "PROPERTY_N_FLOOR", "PROPERTY_N_ROOMS", "PROPERTY_N_AREA", "PROPERTY_N_PRICE", "DETAIL_PAGE_URL", "PROPERTY_N_PRESET_CODE"]);
    while($arSameFlat = $rsSameFlat->GetNext()){
        $arFile = CFile::ResizeImageGet($arSameFlat["PROPERTY_F_PRESET_VALUE"], ["width" => 100, "height" => 100]);
        $arSameFlat["PROPERTY_F_PRESET_VALUE"] = $arFile["src"];
        $arResult["SAME"][] = $arSameFlat;
    }
}

if($arResult["DISPLAY_PROPERTIES"]["F_PRESET"]["VALUE"]){
    $arFile = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["F_PRESET"]["VALUE"], ["width" => 800, "height" => 800]);
    $arResult["DISPLAY_PROPERTIES"]["F_PRESET"]["FILE_VALUE"]["SRC"] = $arFile["src"];
}else{
    $arResult["DISPLAY_PROPERTIES"]["F_PRESET"]["FILE_VALUE"]["SRC"] = "/local/templates/main/images/holder.png";
}

if(!empty($arResult["DISPLAY_PROPERTIES"]["F_PHOTOS"]["FILE_VALUE"])){
    foreach($arResult["DISPLAY_PROPERTIES"]["F_PHOTOS"]["FILE_VALUE"] as &$arPhoto){
        $arFile = CFile::ResizeImageGet($arPhoto, ["width" => 800, "height" => 800]);
        $arPhoto["SRC"] = $arFile["src"];
    }
    unset($arPhoto);
}


// Получаем номера телефонов менеджеров обьекта
$arResult["MANAGER_PHONES"] = [];
if(intval($arResult["PROPERTIES"]["E_HOUSE"]["VALUE"]) > 0){
	$rsManagers = CIBlockElement::GetList(
		[
			"SORT" => "ASC"
		],
		[
			"IBLOCK_ID" => IBLOCK_ID_MANAGER,
			"ACTIVE_DATE" => "Y",
			"ACTIVE" => "Y",
			"ID" => CIBlockElement::SubQuery(
				"PROPERTY_E_MANAGER",
				[
					"IBLOCK_ID" => IBLOCK_ID_HOUSE,
					"ID" => $arResult["PROPERTIES"]["E_HOUSE"]["VALUE"]
				])
		],
		false,
		false,
		[
			"ID",
			"PROPERTY_S_PHONE"
		]
	);

	while($arManager = $rsManagers->fetch()){
			$arResult["MANAGERS_IDs"][] = $arManager["ID"];
		if (empty($arManager["PROPERTY_S_PHONE_VALUE"])) {
			continue;
		}
		if(count($arResult["MANAGER_PHONES"]) < 2){
			$strPhonehref = $arManager["PROPERTY_S_PHONE_VALUE"];

			$strPhonehref = str_replace("+7", "*", $arManager["PROPERTY_S_PHONE_VALUE"]);
			$strPhonehref = str_replace(["-", " ", "(", ")",], "", $strPhonehref);
			if(strlen($strPhonehref) == 7){
				$strPhonehref = "8319".$strPhonehref;
			}
			$arResult["MANAGER_PHONES"][] = [
				"TEXT" => $arManager["PROPERTY_S_PHONE_VALUE"],
				"LINK" => $strPhonehref,
			];
		}
	}
}

$arResult["MANAGER_PHONES_TITLE"] = "МЕНЕДЖЕР".((count($arResult["MANAGER_PHONES"]) == 1) ? "" : "Ы")." ОБЪЕКТА";