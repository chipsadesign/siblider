<?
    $arProps = $arResult["PROPERTIES"];
    $arDisProps = $arResult["DISPLAY_PROPERTIES"];
    $arHouse = $arResult["HOUSE"];
?>

<div class="page__left-content">
    <div class="page__left-content-wrap">
        <div class="page__header page__header--with-border flat-page__header">
            <h1 class="page__title flat-page__title wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".2s"><?=$arResult["PROJECT"]["NAME"]?> <?=$arResult["HOUSE"]["NANE"]?>, кв. <?=$arResult["NAME"]?></h1>
        </div>
        <div class="flat-section">
            <div class="characteristics flat-characteristics-list">
                <?if($arHouse["PROPERTY_S_ADRESS_VALUE"]):?>
                    <div class="flat-characteristics-list__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
                        <div class="characteristics__title">Адрес строения</div>
                        <div class="characteristics__value"><?=$arHouse["PROPERTY_S_ADRESS_VALUE"]?></div>
                    </div>
                <?endif;?>
                <?if($arHouse["PROPERTY_S_DATE_START_VALUE"]):?>
                    <div class="flat-characteristics-list__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
                        <div class="characteristics__title">Начало строительства</div>
                        <div class="characteristics__value"><?=$arHouse["PROPERTY_S_DATE_START_VALUE"]?></div>
                    </div>
                <?endif;?>
                <?if($arHouse["PROPERTY_S_DATE_END_VALUE"]):?>
                    <div class="flat-characteristics-list__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
                        <div class="characteristics__title">Дата сдачи</div>
                        <div class="characteristics__value"><?=$arHouse["PROPERTY_S_DATE_END_VALUE"]?></div>
                    </div>
                <?endif;?>
                <?if($arHouse["PROPERTY_N_FLOORS_VALUE"]):?>
                    <div class="flat-characteristics-list__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
                        <div class="characteristics__title">Количество этажей</div>
                        <div class="characteristics__value"><?=$arHouse["PROPERTY_N_FLOORS_VALUE"]?></div>
                    </div>
                <?endif;?>
                <?if($arHouse["PROPERTY_N_ENTRANCE_VALUE"]):?>
                    <div class="flat-characteristics-list__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
                        <div class="characteristics__title">Количество подъездов</div>
                        <div class="characteristics__value"><?=$arHouse["PROPERTY_N_ENTRANCE_VALUE"]?></div>
                    </div>
                <?endif;?>
                <?if($arHouse["PROPERTY_L_MATERIAL_VALUE"]):?>
                    <div class="flat-characteristics-list__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
                        <div class="characteristics__title">Материал стен</div>
                        <div class="characteristics__value"><?=$arHouse["PROPERTY_L_MATERIAL_VALUE"]?></div>
                    </div>
                <?endif;?>
                <?if($arHouse["PROPERTY_S_FACING_VALUE"]):?>
                    <div class="flat-characteristics-list__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
                        <div class="characteristics__title">Отделка</div>
                        <div class="characteristics__value"><?=$arHouse["PROPERTY_S_FACING_VALUE"]?></div>
                    </div>
                <?endif;?>
            </div>
        </div>
        <div class="flat-section">
            <?if($arProps["N_PRICE"]["VALUE"]):?>
                <div class="characteristics flat-characteristics-list flat__characteristics-cost-container">
                    <div class="flat__characteristics-cost flat-characteristics-list__item">
                        <div class="flat__characteristics-cost__label js-reveal" data-reveal-animation="js-reveal--fadeInLeft" data-reveal-delay="0.2">Стоимость продажи</div>
                        <div class="flat__characteristics-cost__value h1 js-reveal" data-reveal-animation="js-reveal--fadeInLeft" data-reveal-delay="0.2"><?=number_format($arProps["N_PRICE"]["VALUE"], 0, ".", " ");?> ₽</div>
                    </div>
                    <div class="flat-characteristics-list__item flat__mortgage-link-container">
                        <a href="/calculator/" class="flat__mortgage-link hover-underline js-reveal" data-reveal-animation="js-reveal--fadeInLeft" data-reveal-delay="0.2">узнать ваш ежемесячный платеж по ипотеке</a>
                    </div>
                </div>
            <?endif;?>
            <ul class="list__with-dots flat-characteristics-column-list">
                <?if($arResult["HOUSE"]["PROPERTY_S_ADRESS_VALUE"]):?>
                    <li class="flat-characteristics-column-list__item js-reveal" data-reveal-animation="js-reveal--fadeInLeft" data-reveal-delay="0.2"><?=$arResult["HOUSE"]["PROPERTY_S_ADRESS_VALUE"]?></li>
                <?endif;?>
                <?if($arProps["N_AREA"]["VALUE"]):?>
                    <li class="flat-characteristics-column-list__item js-reveal" data-reveal-animation="js-reveal--fadeInLeft" data-reveal-delay="0.2">Общая площадь: <?=$arProps["N_AREA"]["VALUE"]?> м²</li>
                <?endif;?>
                <?if($arProps["S_ENTRANCE"]["VALUE"]):?>
                    <li class="flat-characteristics-column-list__item js-reveal" data-reveal-animation="js-reveal--fadeInLeft" data-reveal-delay="0.2">Подъезд: <?=$arProps["S_ENTRANCE"]["VALUE"]?></li>
                <?endif;?>
                <?if($arProps["N_FLOOR"]["VALUE"]):?>
                    <li class="flat-characteristics-column-list__item js-reveal" data-reveal-animation="js-reveal--fadeInLeft" data-reveal-delay="0.2">Этаж: <?=$arProps["N_FLOOR"]["VALUE"]?></li>
                <?endif;?>
            </ul>
        </div>
        <?if(!empty($arResult["SAME"])):?>
            <div class="table fadeIn js-reveal" data-reveal-animation="js-reveal--fadeInLeft" data-reveal-delay=".4s">
                <div class="table__title">похожие квартиры</div>
                <?foreach($arResult["SAME"] as $arItem):?>
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="table__row">
                        <div class="flat__preview"><img src="<?=$arItem["PROPERTY_F_PRESET_VALUE"];?>"></div>
                        <div>№ <?=$arItem["NAME"]?></div>
                        <?if($arItem["PROPERTY_N_FLOOR_VALUE"]):?>
                            <div><?=$arItem["PROPERTY_N_FLOOR_VALUE"]?> этаж</div>
                        <?endif;?>
                        <?if($arItem["PROPERTY_N_ROOMS_VALUE"]):?>
                            <div><?=$arItem["PROPERTY_N_ROOMS_VALUE"]?> <?=siteHandlers::getInclinationByNumber($arItem["PROPERTY_N_ROOMS_VALUE"], ["комната", "комнаты", "комнат"])?></div>
                        <?endif;?>
                        <?if($arItem["PROPERTY_N_AREA_VALUE"]):?>
                            <div>площадь: <?=$arItem["PROPERTY_N_AREA_VALUE"]?> м<sup>2</sup></div>
                        <?endif;?>
                        <?if($arItem["PROPERTY_N_PRICE_VALUE"]):?>
                            <div><?=number_format($arItem["PROPERTY_N_PRICE_VALUE"], 0, ".", " ");?> ₽</div>
                        <?endif;?>
                    </a>
                <?endforeach;?>
            </div>
        <?endif;?>
        <a href="/flats/filter/house-<?=$arResult["HOUSE"]["ID"]?>/" class="button button--center-black" data-hovercontent="Все квартиры объекта" data-content="Все квартиры объекта"></a>
    </div>
</div>
<div class="page__right-content" id="fancyboxParent">
    <div class="flat__photo wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
    	<?if(!empty($arResult["SIDE_OF_WORLD"])):?>
	        <div class="flat__photo-compass">
	            <img src="<?=$arResult["SIDE_OF_WORLD"];?>" class="flat-compass-img" alt="">
	        </div>
    	<?endif;?>
        <?if($arDisProps["F_PRESET"]["FILE_VALUE"]["SRC"]):?>
            <a href="<?=$arDisProps["F_PRESET"]["FILE_VALUE"]["SRC"]?>" data-link="<?=$arDisProps["F_PRESET"]["FILE_VALUE"]["SRC"]?>" id="flatImg" data-fancybox="image" class="flat__photo-fancybox-link wow" data-wow-duration=".5s" data-wow-delay=".2s">
                <img src="<?=$arDisProps["F_PRESET"]["FILE_VALUE"]["SRC"]?>" data-img="<?=$arDisProps["F_PRESET"]["FILE_VALUE"]["SRC"]?>" alt="photo" class="flat__photo-img">
            </a>
        <?endif;?>
        <div class="flat__right-bottom">
            <?if($arResult["FLOOR"]):?>
                <a href="<?=$arResult["FLOOR"]?>" class="flat__photo-link js-flat__photo-link" data-fancybox="floorPlan" data-link="<?=$arResult["FLOOR"]?>">
                    <span class="flat__photo-link-text">Планировка этажа</span>
                    <span class="icon-choose-flat floorPlan-icon"></span>
                </a>
            <?endif;?>
            <a href="<?=$arResult["DETAIL_PAGE_URL"];?>pdf/" target="_blank" id="flatLink" class="flat__photo-link">
                <span class="flat__photo-link-text">Скачать</span>
                <img src="/local/templates/main/images/download.svg" alt="">
            </a>
            <?if(!empty($arDisProps["F_PHOTOS"]["FILE_VALUE"])):?>
                <a href="javascript:;" id="morePhotos" class="flat__photo-link">Больше фото</a>
                <script>
                    (function(){
                        document.addEventListener('DOMContentLoaded', function(event){
                            //пути до фотографий
                            var morePhotos = [
                                <?foreach($arDisProps["F_PHOTOS"]["FILE_VALUE"] as $arFile):?>
                                    {src : '<?=$arFile["SRC"]?>'},
                                <?endforeach;?>
                            ];
                            var params = {
                                overlayShow: false,
                                arrows: true,
                                infobar: false,
                                keyboard: true,
                                touch:false,
                                toolbar: true,
                                animationEffect: "fade",
                                animationDuration: 1000,
                                transitionDuration: 1000,
                                baseClass: 'fancyBox-base  fancyBox-base--fixed',
                                slideClass: 'fancyBox-slide',
                                buttons: [
                                    "zoom",
                                    "fullScreen",
                                    "close"
                                ],
                            };
                            $('#morePhotos').on('click', function(e){
                                e.preventDefault();
                                $.fancybox.open(
                                    morePhotos,
                                    params
                                );
                            });
                            $('.page-header').addClass('page-header--no-bg');
                            $('#changeImgBtn').on('click', function(e){
                                var currentImg = $('#flatImg img').attr('src'),
                                    nextImg = $('#flatImg img').attr('data-img'),
                                    currentDownloadLink = $('#flatLink').attr('href'),
                                    nextDownloadLink = $('#flatLink').attr('data-link'),
                                    currentImgLink = $('#flatImg').attr('href'),
                                    nextImgLink = $('#flatImg').attr('data-link');
                                $('#flatImg').addClass('fadeOutLeft');
                                $('#changeImgBtn').addClass('is-changed');
                                setTimeout(function(){
                                    $('#flatImg img').attr('src', nextImg);
                                    $('#flatImg img').attr('data-img', currentImg);
                                    $('#flatImg').attr('href', nextImgLink);
                                    $('#flatImg').attr('data-link', currentImgLink);
                                    $('#flatLink').attr('href', nextDownloadLink);
                                    $('#flatLink').attr('data-link', currentDownloadLink);

                                    $('#flatImg').removeClass('fadeOutLeft');
                                    $('#flatImg').addClass('fadeInRightFull');

                                    var currentText = $('#changeImgBtn').text();
                                    $('#changeImgBtn').text($('#changeImgBtn').attr('data-text'));
                                    $('#changeImgBtn').attr('data-text', currentText);

                                    $('#changeImgBtn').removeClass('is-changed');
                                    setTimeout(function(){
                                        $('#flatImg').removeClass('fadeInRightFull');
                                    }, 700);
                                }, 500);
                            });

                            initFancyBox();
                        });
                    })()
                </script>
            <?endif;?>
        </div>
    </div>
    <a data-fancybox data-src="#bookingModal" href="javascript:;" class="button button--center-black" data-hoverContent="забронировать на 24 часа" data-content="забронировать на 24 часа"></a>
    <div class="modal  modal--flat" style="display: none;" id="bookingModal">
        <div class="modal__wrap">
            <div class="modal__title h1">забронировать</div>
            <form class="modal__form js-form" data-ya-target="order_done">
                <?=bitrix_sessid_post();?>
                <input type="hidden" name="action" value="FLAT_BOOK">
				<?foreach($arResult["MANAGERS_IDs"] as $id):?>
	                <input type="hidden" name="managers[]" value="<?=$id;?>">
				<?endforeach;?>
                <input type="hidden" name="FIELDS[FLAT_NAME]" value="<?=$arResult["HOUSE"]["NAME"]?>, <?=$arResult["NAME"]?>" id="js-field_flatName">
                <label class="input-label">
                    <span>* Фамилия Имя Отчество</span>
                    <input type="text" name="FIELDS[NAME]" class="required">
                </label>
                <label class="input-label">
                    <span>* Мобильный телефон</span>
                    <input type="text" name="FIELDS[PHONE]" class="required input--phone" pattern="^\+7\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}">
                </label>
                <label class="input-label">
                    <span>Адрес эл. почты</span>
                    <input type="email" name="FIELDS[EMAIL]">
                </label>
                <label class="input-label  input-label--checkbox">
                    <input type="checkbox" name="agreed" class="checkbox required">
                    <span class="checkbox-custom  checkbox-custom--mark"></span>
                    <span class="input-label__agreed-text">Согласен с условиями <a href="/personal-confirm/">Согласия</a> на обработку персональных данных</span>
                </label>
                <button class="modal__submit button button--center-black" type="submit" data-hoverContent="Забронировать" data-content="Забронировать"></button>
            </form>
        </div>
    </div>
    <div id="successMessage" class="success-message" style="display: none;">
        <div class="success-message__inner">
            <div class="h1">Ваша заявка успешно отправлена!</div>
            <div class="success-message__subtitle">Наши менеджеры перезвонят вам в течение рабочего дня.</div>
        </div>
    </div>
</div>