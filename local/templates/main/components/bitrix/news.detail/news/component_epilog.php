<script>
    (function(){
        document.addEventListener('DOMContentLoaded', function(event){
            $('.page h1').each(function(){
                $(this).addClass('wow fadeInDown').attr('data-wow-duration', '.5s').attr('data-wow-delay', '.2s');
            });
            $('.page h2, .page p').each(function(){
                $(this).addClass('wow fadeInUp').attr('data-wow-duration', '.5s').attr('data-wow-delay', '.2s');
            });
            $('.page .list__title').each(function(){
                $(this).addClass('wow fadeInUp').attr('data-wow-duration', '.5s').attr('data-wow-delay', '.2s');
            });
            $('.page img, .page .js-parallax-img, .page .blue-bg, .page table').each(function(){
                $(this).addClass('wow fadeIn').attr('data-wow-duration', '.5s').attr('data-wow-delay', '.2s');
            });
            $('.page .list ul, .page .list ol, .page ul, .page ol').each(function(){
                $(this).addClass('wow fadeInUp').attr('data-wow-duration', '.5s').attr('data-wow-delay', '.4s');
            });
            var imagesArray = [];
            initParallax();
            $(window).on('scroll', function() {
                setPosition('.js-parallax-img');
            });
            $(window).resize(function(){
                initParallax();
            });
        });
    })()
</script>