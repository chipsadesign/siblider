<?
include $_SERVER["DOCUMENT_ROOT"]."/local/templates/main/components/bitrix/news.detail/flat/result_modifier.php";
$arDisProps = &$arResult["DISPLAY_PROPERTIES"];
if($arDisProps["F_PRESET"]["FILE_VALUE"]["SRC"]){
	$arFile = CFile::ResizeImageGet($arDisProps["F_PRESET"]["FILE_VALUE"]["ID"], ["width" => 300, "height" => 325], BX_RESIZE_IMAGE_PROPORTIONAL);
	$arDisProps["F_PRESET"]["FILE_VALUE"]["SRC"] = $arFile["src"];
}

if($arResult["FLOOR"]){
	$arFile = CFile::ResizeImageGet($arResult["FLOOR_ID"], ["width" => 320, "height" => 210], BX_RESIZE_IMAGE_PROPORTIONAL);
	$arResult["FLOOR"] = $arFile["src"];
}