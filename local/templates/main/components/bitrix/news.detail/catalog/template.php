<?
	$showMap = false; // добавлять ли модалку карты
	$showMapBlock = false; // есть ли слайд с картой
?>

<div class="page__header">
	<h1 class="page__title big wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".2s"><?=$arResult["NAME"]?></h1>
</div>
<div class="object__info">
	<div class="characteristics object__characteristics">
		<?foreach($arResult["MAIN_PROPS"] as $code):?>
			<?
				$name = $arResult["PROPERTIES"][$code]["NAME"];
				$val = $arResult["PROPERTIES"][$code]["VALUE"];
				if(strpos($code, "L_") === 0)
					$val = $arResult["DISPLAY_PROPERTIES"][$code]["VALUE"];

				if(!$val) continue
			?>
			<div class="characteristics__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
				<div class="characteristics__title"><?=$name?></div>
				<div class="characteristics__value"><?=$val?></div>
			</div>
		<?endforeach;?>
	</div>
	<?if(!empty($arResult["PREVIEW_PICTURE"])):?>
		<div class="object__logo wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
			<img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="logo">
		</div>
	<?endif;?>
</div>

<?if(!empty($arResult["DETAIL_PICTURE"])):?>
	<div class="parallax-img object__first-img wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
		<div class="js-parallax-img parallax-img__img detail-img" data-direction="1" data-speed="1" style="background-image: url('<?=$arResult["DETAIL_PICTURE"]["SRC"]?>');"></div>
	</div>
<?endif;?>
<?$initParallax = false;?>
<?if(!empty($arResult["SLIDES"])):?>
	<?$initParallax = true;?>
	<a id="description"></a>
	<?foreach($arResult["SLIDES"] as $arSlide):?>
		<?if($arSlide["PROPERTY_L_TYPE_ENUM_ID"] == 20): //кратинка справа?>
			<div class="object__half-container">
				<div class="object__content">
					<h2 class="h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"><?=$arSlide["NAME"]?></h2>
					<?=$arSlide["PREVIEW_TEXT"];?>
					<?if($arSlide["CODE"]):?>
						<a href="<?=$arSlide["CODE"]?>" class="button button--light button--rectangle-animate button--icon-animate wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
							<span>подробнее</span><span class="icon-arrow icon-arrow--right"></span>
						</a>
					<?endif;?>
				</div>
				<?if($arSlide["PREVIEW_PICTURE"] && empty($arSlide["SLIDER"])):?>
					<div class="parallax-img wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
						<div class="js-parallax-img parallax-img__img" data-direction="-1" data-speed="3" style="background-image: url('<?=$arSlide["PREVIEW_PICTURE"]?>');"></div>
						<?if(
							$arSlide["PROPERTY_L_MAP_VALUE"] == "Y"
							&& $arResult["PROPERTIES"]["S_COORD"]["VALUE"]
						):?>
							<?$showMap = true;?>
							<a href="#modalMap" class="fancyMap button button--light button--rectangle-animate button--icon-animate wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
								<span>смотреть объект на карте</span><span class="icon-arrow icon-arrow--right"></span>
							</a>
						<?endif;?>
					</div>
				<?endif;?>
				<?if(!empty($arSlide["SLIDER"])):?>
					<div class="slider  slider--half">
						<div class="slider__container swiper-container js-slides">
							<div class="slider__wrap  swiper-wrapper">
								<?
									if($arSlide["PREVIEW_PICTURE"]){
										$arSlide["SLIDER"] = array_merge(
											[["SRC" => $arSlide["PREVIEW_PICTURE"]]],
											$arSlide["SLIDER"]
										);
									}
								?>
								<?foreach($arSlide["SLIDER"] as $arImg):?>
									<div class="slider__slide parallax-img swiper-slide wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
										<div class="js-parallax-img  parallax-img__img" data-direction="-1" data-speed="3"  style="background-image: url('<?=$arImg["SRC"]?>');"></div>
									</div>
								<?endforeach;?>
							</div>
							<div class="slider__nav slider__nav--triangle">
								<div class="slider__nav-item js-slider-prev"></div>
								<div class="slider__nav-item js-slider-next"></div>
							</div>
						</div>
						<script>
							(function(){
								document.addEventListener('DOMContentLoaded', function(event){
									var slidesSwiper = new Swiper('.js-slides', {
										speed: 1100,
										effect: 'fade',
										fadeEffect: {
											crossFade: true
										},
										loop: true,
										longSwipes: false,
										longSwipesMs: '100ms',
										navigation: {
											nextEl: '.js-slider-prev',
											prevEl: '.js-slider-next',
										},
									});
								});
							})()
						</script>
					</div>
				<?endif;?>
			</div>
		<?endif;?>

		<?if($arSlide["PROPERTY_L_TYPE_ENUM_ID"] == 19): //картинка слева?>
			<?$initParallax = true;?>
			<div class="object__half-container">
				<?if($arSlide["PREVIEW_PICTURE"] && empty($arSlide["SLIDER"])):?>
					<div class="parallax-img wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
						<div class="js-parallax-img parallax-img__img" data-direction="-1" data-speed="3" style="background-image: url('<?=$arSlide["PREVIEW_PICTURE"]?>');"></div>
						<?if(
							$arSlide["PROPERTY_L_MAP_VALUE"] == "Y"
							&& $arResult["PROPERTIES"]["S_COORD"]["VALUE"]
						):?>
							<?$showMap = true;?>
							<a href="#modalMap" class="fancyMap button button--light button--rectangle-animate button--icon-animate wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
								<span>смотреть объект на карте</span><span class="icon-arrow icon-arrow--right"></span>
							</a>
						<?endif;?>
					</div>
				<?endif;?>
				<?if(!empty($arSlide["SLIDER"])):?>
					<div class="slider  slider--half">
						<div class="slider__container swiper-container js-slides">
							<div class="slider__wrap  swiper-wrapper">
								<?
									if($arSlide["PREVIEW_PICTURE"]){
										$arSlide["SLIDER"] = array_merge(
											[["SRC" => $arSlide["PREVIEW_PICTURE"]]],
											$arSlide["SLIDER"]
										);
									}
								?>
								<?foreach($arSlide["SLIDER"] as $arImg):?>
									<div class="slider__slide parallax-img swiper-slide wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
										<div class="js-parallax-img  parallax-img__img" data-direction="-1" data-speed="3"  style="background-image: url('<?=$arImg["SRC"]?>');"></div>
									</div>
								<?endforeach;?>
							</div>
							<div class="slider__nav slider__nav--triangle">
								<div class="slider__nav-item js-slider-prev"></div>
								<div class="slider__nav-item js-slider-next"></div>
							</div>
						</div>
						<script>
							(function(){
								document.addEventListener('DOMContentLoaded', function(event){
									var slidesSwiper = new Swiper('.js-slides', {
										speed: 1100,
										effect: 'fade',
										fadeEffect: {
											crossFade: true
										},
										loop: true,
										longSwipes: false,
										longSwipesMs: '100ms',
										navigation: {
											nextEl: '.js-slider-prev',
											prevEl: '.js-slider-next',
										},
									});
								});
							})()
						</script>
					</div>
				<?endif;?>
				<div class="object__content">
					<h2 class="h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"><?=$arSlide["NAME"]?></h2>
					<?=$arSlide["PREVIEW_TEXT"];?>
					<?if($arSlide["CODE"]):?>
						<a href="<?=$arSlide["CODE"]?>" class="button button--light button--rectangle-animate button--icon-animate wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
							<span>подробнее</span><span class="icon-arrow icon-arrow--right"></span>
						</a>
					<?endif;?>
				</div>
			</div>
		<?endif;?>

		<?if($arSlide["PROPERTY_L_TYPE_ENUM_ID"] == 24 && $arResult["PROPERTIES"]["S_COORD"]["VALUE"]): //карта слева?>
			<?$showMapBlock = true;?>
			<?$arMapContainer[] = "mapBlock".$arSlide["ID"];?>
			<div class="object__half-container">
				<div class="parallax-img wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
					<div id="mapBlock<?=$arSlide["ID"]?>"></div>
					<a href="#modalMap" class="fancyMap button button--light button--rectangle-animate button--icon-animate wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
						<span>Увеличить карту</span><span class="icon-arrow icon-arrow--right"></span>
					</a>
				</div>
				<div class="object__content">
					<h2 class="h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"><?=$arSlide["NAME"]?></h2>
					<?=$arSlide["PREVIEW_TEXT"];?>
				</div>
			</div>
		<?endif;?>

		<?if($arSlide["PROPERTY_L_TYPE_ENUM_ID"] == 25 && $arResult["PROPERTIES"]["S_COORD"]["VALUE"]): //карта справа?>
			<?$showMapBlock = true;?>
			<?$arMapContainer[] = "mapBlock".$arSlide["ID"];?>
			<div class="object__half-container">
				<div class="object__content">
					<h2 class="h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"><?=$arSlide["NAME"]?></h2>
					<?=$arSlide["PREVIEW_TEXT"];?>
				</div>
				<div class="parallax-img wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
					<div id="mapBlock<?=$arSlide["ID"]?>"></div>
					<a href="#modalMap" class="fancyMap button button--light button--rectangle-animate button--icon-animate wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
						<span>Увеличить карту</span><span class="icon-arrow icon-arrow--right"></span>
					</a>
				</div>
			</div>
		<?endif;?>

		<?if($arSlide["PROPERTY_L_TYPE_ENUM_ID"] == 21): //слайдер?>
			<?$initParallax = true;?>
			<?$arDesc = $arSlide["PROPERTY_S_IMG_DESC_VALUE"];?>
			<div class="slider object__slider">
				<div class="slider__container swiper-container js-build-gallery">
					<div class="slider__wrap swiper-wrapper">
						<?foreach($arSlide["SLIDER"] as $key => $arImg):?>
							<div <?=strlen($arImg["DESCRIPTION"]) ? 'data-title="'.$arImg["DESCRIPTION"].'"' : "data-title=''";?> class="slider__item swiper-slide parallax-img">
								<div class="js-parallax-img parallax-img__img js-image" data-direction="1" data-speed="1.2" style="background-image: url('<?=$arImg["SRC"]?>');"></div>
								<?if(strlen(trim($arDesc[$key]))):?>
									<div class="slider__text"><?=$arDesc[$key]?></div>
								<?endif;?>
							</div>
						<?endforeach;?>
					</div>
					<div class="slider__curtain js-slider-curtain"></div>
		            <div class="slider__text-dots">
		            	<div class="slider__text-dots-wrap swiper-pagination js-build-gallery-dots"></div>
		            </div>
					<div class="slider__nav slider__nav--triangle">
						<div class="slider__nav-item js-slider-prev"></div>
						<div class="slider__nav-item js-slider-next"></div>
					</div>
				</div>
			</div>
		<?endif;?>
	<?endforeach;?>
<?endif;?>

<?if(!empty($arResult["FLATS"])):?>
	<div class="object__flats" id="flats">
		<div class="object__choose-flats">
			<div class="object__form-btn" id="objectCallChooseForm" data-fancybox="filter" data-src="#filterFormWrap"><span class="icon-range"></span></div>
            <div class="modal modal--filter" id="filterFormWrap">
                <div class="modal__wrap">
                    <div class="modal__header"></div>
                    <div class="modal__content">
						<form class="form" id="filterForm_detail">
							<input type="hidden" name="house_id" value="<?=$arResult["ID"]?>">
							<input type="hidden" name="action" value="object_detail">
							<input type="hidden" name="status_active_h" value="y">
			            	<div class="form__row  form__row--half">
								<?if($arResult["MIN_PRICE"] > 0 && $arResult["MAX_PRICE"] > 0):?>
									<input type="hidden" name="min_price">
									<input type="hidden" name="max_price">
									<div class="form__item js-form-item wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
										<div class="form__item-title">цена</div>
										<div class="form__item-content">
											<div class="custom-range">
												<div class="custom-range__values">
													<div id="rangeCostFirstValue"><?=$arResult["MIN_PRICE"]?></div>
													<div id="rangeCostLastValue"><?=$arResult["MAX_PRICE"]?></div>
												</div>
												<div id="rangeCost" data-step="100000" data-min="<?=$arResult["MIN_PRICE"]?>" data-max="<?=$arResult["MAX_PRICE"]?>" class="js-range"></div>
											</div>
											<script>
												(function(){
													document.addEventListener('DOMContentLoaded', function(event){
														var sliderCost = document.getElementById('rangeCost');
														initNoUiSliderTwoPoint(sliderCost);
														var sliderValues = [
															document.getElementById('rangeCostFirstValue'),
															document.getElementById('rangeCostLastValue')
														];
														sliderCost.noUiSlider.on('update', function( values, handle ) {
															sliderValues[handle].innerHTML = values[handle];
															$('[name="min_price"]').val(values[0]);
															$('[name="max_price"]').val(values[1]);
														});
														sliderCost.noUiSlider.on('change', function( values, handle ) {
															filterObject();
														});
													});
												})()
											</script>
										</div>
									</div>
								<?endif;?>
								<?if(!empty($arResult["ROOMS"])):?>
									<div class="form__item js-form-item wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
										<div class="form__item-title">кол-во комнат</div>
										<div class="form__item-content form__item-content--flex">
					                        <?foreach($arResult["ROOMS"] as $room):?>
					    						<label class="input-label input-label--checkbox">
					    							<input type="checkbox" name="rooms[]" class="checkbox" value="<?=$room?>">
					    							<span class="checkbox-custom checkbox-custom--number checkbox-custom--brown" data-hoverContent="<?=$room?>" data-content="<?=$room?>"></span>
					    						</label>
											<?endforeach;?>
										</div>
									</div>
									<script>
										(function(){
											document.addEventListener('DOMContentLoaded', function(event){
												$('#filterForm_detail input[type="checkbox"]').change(function(){
													filterObject();
												});
											});
										})();
									</script>
								<?endif;?>
			            	</div>
					        <div class="choose-flat__counter  desktop-hide" id="flatsCounter"><span>154</span> подходящих варианта</div>
					        <button type="reset" class="button desktop-hide js-form-item" id="filterReset">сбросить фильтр <span class="icon-close"></span></button>
					        <script>
								(function(){
									document.addEventListener('DOMContentLoaded', function(event){
										$('#filterReset').on('click', function(e){
					                        e.preventDefault();
					                        //сброс чекбоксов
					                        $('#filterForm_detail input[type="checkbox"]').each(function(){
					                            $(this).prop( "checked", false );
					                        });
					                        //сброс селектов
					                        $('#filterForm_detail .js-select').each(function(){
					                            $(this).val(null).trigger('change').removeClass('is-filled');
					                        });
					                        //сброс бегунков
					                        $('#filterForm_detail .js-range').each(function(){
					                            $(this)[0].noUiSlider.reset();
					                        });
					                        setTimeout(function(){
					                            $('#filterForm_detail').removeClass('is-filled');
					                            filterObject();
					                        }, 500);
					                    });
					                    $('#flatsCounter').on('click', function(){
					                    	$.fancybox.close();
					                    });
					        		});
					        	})()
					        </script>
						</form>
					</div>
				</div>
			</div>
			<div class="object__result-flats wow fadeInUp room" data-wow-duration=".5s" data-wow-delay=".2s" id="objectFlats">
				<div class="object__result-flats-wrap">
					<div class="table" data-sort-table data-sort-direction="-1">
						<div class="table__title wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
							<div class="table__floor">квартира</div>
							<div class="table__square">этаж</div>
							<div class="table__place">кол-во комнат</div>
							<div class="table__size">площадь от</div>
							<div class="table__price">
								<a href="#" class="sort-by link link--pseudo" data-sort-button="price">
									<div class="link__text">цена от</div>
									<div class="sort-by__icon link__icon icon-triangle"></div>
								</a>
							</div>
						</div>
						<div class="table__content wow fadeIn js-flatsList" data-wow-duration=".5s" data-wow-delay=".3s" data-sort-section>
							<?foreach($arResult["FLATS"] as $arFlat):?>
								<div class="table__item js-flat-preview" data-id="<?=$arFlat["ID"]?>" data-name="<?=$arResult["NAME"];?>, <?=$arFlat["NAME"];?>" data-sort-row>
									<div class="table__floor">
										№ <?=$arFlat["NAME"]?>
									</div>
									<div class="table__square"><?=$arFlat["PROPERTIES"]["N_FLOOR"]?></div>
									<div class="table__place"><span data-hoverContent="<?=$arFlat["PROPERTIES"]["N_ROOMS"]?>" data-content="<?=$arFlat["PROPERTIES"]["N_ROOMS"]?>"></span></div>
									<div class="table__size"><?=$arFlat["PROPERTIES"]["N_AREA"]?> м<sup>2</sup></div>
									<div class="table__price" data-sort-price data-sort-value="<?=$arFlat["PROPERTIES"]["N_PRICE"]?>"><?=number_format($arFlat["PROPERTIES"]["N_PRICE"], 0, ".", " ")?> ₽</div>
								</div>
							<?endforeach;?>
						</div>
					</div>
				</div>
				<div class="preloader">
					<div class="preloader__item"></div>
					<div class="preloader__item"></div>
					<div class="preloader__item"></div>
				</div>
			</div>
			<a href="/flats/filter/house-<?=$arResult["ID"]?>/" class="button button--black button--rectangle-animate button--icon-animate wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
				<span>Показать похожие</span><span class="icon-arrow icon-arrow--right"></span>
			</a>
			<script>
				(function(){
					document.addEventListener('DOMContentLoaded', function(event){
						$(document).on('click', '.js-flat-preview', function(){
							var id = $(this).data('id');
							var strName = $(this).data('name');
							$('#js-field_flatName').val(strName);
	                        $('#flat').addClass('is-changed');
	                        setTimeout(function(){
	                            $('.js-flatTarget').hide().addClass('is-stopAnimation');
	                            $('#flat_'+id).show(0, function(){
	                                $('#flat').removeClass('is-changed');
	                            });
	                        }, 600);
	                        if ($(window).width()<= 1000) {
	                        	var offset = $('#flat').offset().top - $('#pageHeader').outerHeight() - $('aside').outerHeight();
                            	$('html, body').animate({scrollTop: offset}, 500);
	                        }
						});

                        if ($(window).width() <= 1000) {
                            $('#flats #filterFormWrap').hide();
                        }

                        $(window).resize(function(){
                            if ($(window).width() > 1000) {
                                $('#flats #filterFormWrap').show();
                            } else {
                                $('#flats #filterFormWrap').hide();
                            }
                        });
					});
				})()
			</script>
		</div>
		<div class="object__flat" id="flat">
			<div class="h1 wow fadeInUp js-flatH1" data-wow-duration=".5s" data-wow-delay=".2s">Цены от <?=number_format($arResult["MIN_PRICE"]/1000, 0, ".", " ");?> тыс. руб.</div>
			<?foreach($arResult["FLATS"] as $key => $arFlat):?>
				<?$arProps = $arFlat["PROPERTIES"];?>
				<div class="object__flat-wrapper js-flatTarget <?=$key > 0 ? "is-hidden" : ""?>" id="flat_<?=$arFlat["ID"]?>">
					<div class="object__flat-number <?if($key === 0):?>wow fadeInUp<?endif;?>" data-wow-duration=".5s" data-wow-delay=".2s">Квартира № <span id="flatNumber"><?=$arFlat["NAME"]?></span></div>
					<?if(!is_null($arProps["F_PRESET"])):?>
						<a href="<?=$arFlat["F_PRESET_ORIGINAL"]?>" data-fancybox="image" class="object__flat-img <?if($key === 0):?>wow fadeIn<?endif;?>" data-wow-duration=".5s" data-wow-delay=".2s" id="flatFancyBoxLink">
							<img src="<?=$arProps["F_PRESET"]?>" alt="flat" id="flatImage">
						</a>
					<?endif;?>
					<div class="characteristics object__flat-info">
						<?if($arProps["N_AREA"]):?>
							<div class="characteristics__item <?if($key === 0):?>wow fadeInUp<?endif;?>" data-wow-duration=".5s" data-wow-delay=".2s">
								<div class="characteristics__title">площадь</div>
								<div class="characteristics__value" id="flatSquare"><?=$arProps["N_AREA"]?> м</div>
							</div>
						<?endif;?>
						<?if($arProps["N_ROOMS"]):?>
							<div class="characteristics__item <?if($key === 0):?>wow fadeInUp<?endif;?>" data-wow-duration=".5s" data-wow-delay=".3s">
								<div class="characteristics__title">кол-во комнат</div>
								<div class="characteristics__value" id="flatRooms"><?=$arProps["N_ROOMS"]?></div>
							</div>
						<?endif;?>
						<?if($arProps["N_FLOOR"]):?>
							<div class="characteristics__item <?if($key === 0):?>wow fadeInUp<?endif;?>" data-wow-duration=".5s" data-wow-delay=".4s">
								<div class="characteristics__title">этаж</div>
								<div class="characteristics__value" id="flatFloor"><?=$arProps["N_FLOOR"]?></div>
							</div>
						<?endif;?>
						<?if($arProps["S_ENTRANCE"]):?>
							<div class="characteristics__item <?if($key === 0):?>wow fadeInUp<?endif;?>" data-wow-duration=".5s" data-wow-delay=".5s">
								<div class="characteristics__title">подъезд</div>
								<div class="characteristics__value" id="flatEntrance"><?=$arProps["S_ENTRANCE"]?></div>
							</div>
						<?endif;?>
						<?if($arProps["N_PRICE"]):?>
							<div class="characteristics__item <?if($key === 0):?>wow fadeInUp<?endif;?>" data-wow-duration=".5s" data-wow-delay=".6s">
								<div class="characteristics__title">цена</div>
								<div class="characteristics__value" id="flatCost"><?=number_format($arProps["N_PRICE"], 0, ".", " ")?> ₽</div>
							</div>
						<?endif;?>
					</div>
					<div class="buttons-wrap">
						<a href="<?=$arFlat["DETAIL_PAGE_URL"]?>" class="button button--black button--rectangle-animate button--icon-animate <?if($key === 0):?>wow fadeIn<?endif;?>" data-wow-duration=".5s" data-wow-delay=".2s">
							<span>смотреть подробнее</span><span class="icon-arrow icon-arrow--right"></span>
						</a>
	                    <a data-fancybox data-src="#bookingModal" href="javascript:;" class="button button--black button--rectangle-animate button--icon-animate wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
	                        <span>забронировать<br>на 24 часа</span><span class="icon-arrow  icon-arrow--right"></span>
	                    </a>
	                </div>
					<div class="preloader">
						<div class="preloader__item"></div>
						<div class="preloader__item"></div>
						<div class="preloader__item"></div>
					</div>
				</div>
			<?endforeach;?>
		</div>
	</div>
<?endif;?>

<?if(isset($arResult["COMMERCE"]) || isset($arResult["PARKING"])):?>
	<div class="object__half-container">
		<?if(isset($arResult["COMMERCE"])):?>
			<a href="/business/filter/house-<?=$arResult["ID"]?>/" class="object__big-link big-link" id="commerce">
				<div class="big-link__img" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/commerceLink.jpg');"></div>
				<div class="big-link__text h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">бизнес недвижимость</div>
				<div class="button button--light button--rectangle-animate wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
					<span>подробнее</span><span class="icon-arrow icon-arrow--right"></span>
				</div>
			</a>
		<?endif;?>
		<?if(isset($arResult["PARKING"])):?>
			<a href="<?=$arResult["PARKING"]?>" class="object__big-link big-link" id="parking">
				<div class="big-link__img" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/parkingLink.jpg');"></div>
				<div class="big-link__text h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">паркинг</div>
				<div class="button button--light button--rectangle-animate wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
					<span>подробнее</span><span class="icon-arrow icon-arrow--right"></span>
				</div>
			</a>
		<?endif;?>
	</div>
<?endif;?>

<?if(!empty($arResult["BANNERS"])):?>
	<div class="object__half-container">
		<?foreach($arResult["BANNERS"] as $arBanner):?>
			<a href="<?=$arBanner["PROPERTY_S_LINK_URL_VALUE"]?>" class="object__big-link big-link <?=!$arBanner["PREVIEW_PICTURE"] ? "big-link--black" : "";?>">
				<?if($arBanner["PREVIEW_PICTURE"]):?>
					<div class="big-link__img" style="background-image: url('<?=$arBanner["PREVIEW_PICTURE"]?>');"></div>
				<?endif;?>
				<div class="big-link__text big wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s"><?=$arBanner["PROPERTY_S_TEXT_VALUE"]?></div>
				<div class="button button--light button--rectangle-animate">
					<span><?=$arBanner["PROPERTY_S_LINK_TEXT_VALUE"]?></span><span class="icon-arrow icon-arrow--right"></span>
				</div>
			</a>
		<?endforeach;?>
	</div>
<?endif;?>

<?if(isset($arResult["VARIANTS"])):?>
	<div class="object__purchase purchases" id="purchase">
		<div class="h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">Способы приобретения</div>
		<?foreach($arResult["VARIANTS"] as $arVariant):?>
			<div class="purchases__item purchase wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
				<div class="purchase__header">
					<div class="purchase__title"><?=$arVariant["NAME"];?></div>
					<?if($arVariant["PROPERTY_S_SUBHEAD_VALUE"]):?>
						<div class="purchase__bet"><?=$arVariant["PROPERTY_S_SUBHEAD_VALUE"]?></div>
					<?endif;?>
				</div>
				<div class="purchase__content">
					<?=$arVariant["PREVIEW_TEXT"]?>
					<div class="purchase__link"><a href="<?=$arVariant["CODE"];?>">подробнее</a></div>
				</div>
			</div>
		<?endforeach;?>
	</div>
<?endif;?>

<?if(
	isset($arResult["MANAGER"])
	|| $arResult["PROPERTIES"]["S_ADRESS_CONTACT"]["VALUE"]
	|| $arResult["PROPERTIES"]["S_WORKTIME"]["VALUE"]
	|| $arResult["PROPERTIES"]["S_DINER"]["VALUE"]
):?>
	<div class="object__contacts" id="contacts">
		<div class="object__contacts-slider contacts-slider owl-carousel" id="contactsSlider">
			<div class="contacts-slider__item contacts-slider__item--wide js-contacts-item">
				<div class="h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">контакты</div>
				<?if($arResult["PROPERTIES"]["S_ADRESS_CONTACT"]["VALUE"]):?>
					<div class="contacts-slider__address wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"><?=$arResult["PROPERTIES"]["S_ADRESS_CONTACT"]["VALUE"]?></div>
				<?endif;?>
				<div class="contacts-slider__info">
					<div class="contacts-slider__optional-info small wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">Индивидуальные визиты в удобное для Вас время проводятся по записи.</div>
					<div class="contacts-slider__working-hours">
						<?if($arResult["PROPERTIES"]["S_WORKTIME"]["VALUE"]):?>
							<div class="contacts-slider__info-item wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
								<div class="contacts-slider__info-title">часы работы</div>
								<div class="contacts-slider__info-value small"><?=$arResult["PROPERTIES"]["S_WORKTIME"]["VALUE"]?></div>
							</div>
						<?endif;?>
						<?if($arResult["PROPERTIES"]["S_DINER"]["VALUE"]):?>
							<div class="contacts-slider__info-item wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
								<div class="contacts-slider__info-title">обед</div>
								<div class="contacts-slider__info-value small"><?=$arResult["PROPERTIES"]["S_DINER"]["VALUE"]?></div>
							</div>
						<?endif;?>
					</div>
				</div>
			</div>
			<?if(isset($arResult["MANAGER"])):?>
				<?foreach($arResult["MANAGER"] as $arManager):?>
					<div class="contacts-slider__item js-contacts-item wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
						<div class="manager">
							<div class="manager__img">
								<?if($arManager["PREVIEW_PICTURE"]):?>
									<img src="<?=$arManager["PREVIEW_PICTURE"]?>">
								<?endif;?>
								<?if($arManager["PROPERTY_S_POSITION_VALUE"]):?>
									<div class="manager__post"><?=$arManager["PROPERTY_S_POSITION_VALUE"]?></div>
								<?endif;?>
							</div>
							<div class="manager__name"><?=$arManager["NAME"]?></div>
							<?if(
								isset($arManager["PROPERTY_S_PHONE_VALUE"])
								|| isset($arManager["PROPERTY_S_EMAIL_VALUE"])
							):?>
								<div class="manager__contacts">
									<?if(isset($arManager["PROPERTY_S_PHONE_VALUE"])):?>
                                    	<div class="manager__contacts-item">тел: <a href="tel:<?=$arManager["PROPERTY_S_PHONE_VALUE"]?>"><?=$arManager["PROPERTY_S_PHONE_VALUE"]?></a></div>
                                    <?endif;?>
									<?if(isset($arManager["PROPERTY_S_EMAIL_VALUE"])):?>
                                    	<div class="manager__contacts-item">e-mail: <a href="mailto:<?=$arManager["PROPERTY_S_EMAIL_VALUE"]?>"><?=$arManager["PROPERTY_S_EMAIL_VALUE"]?></a></div>
                                    <?endif;?>
                                </div>
							<?endif;?>
						</div>
					</div>
				<?endforeach;?>
			<?endif;?>
		</div>
		<div class="object__contacts-slider-nav slider__nav--triangle">
			<div class="object__contacts-slider-progress">
				<div id="contactsSliderProgress"></div>
			</div>
		</div>
	</div>
	<script>
		(function(){
			document.addEventListener('DOMContentLoaded', function(event){
				resizeContactsItem();
				$(window).resize(function(){
					resizeContactsItem();
				});
				var sliderLength = $('.js-contacts-item').length;
				$('#contactsSlider').owlCarousel({
					smartSpeed: 300,
					fluidSpeed: 300,
					navSpeed: 300,
					margin: 0,
					nav: true,
					autoWidth: true,
					navText: ['', ''],
					dots: false,
					navContainer: '.object__contacts-slider-nav',
					onInitialized: function(event){
						var sliderItems = $('#contactsSlider .owl-item.active').length;
						$('#contactsSliderProgress').css('transform', 'scaleX(' + (sliderItems / sliderLength) +')');

						if (event.item.count < 2) {
							$('#contactsSlider').addClass('is-single-slide');
						}
					},
					onTranslated: function(event){
						var sliderItems = $('#contactsSlider .owl-item.active').length,
							item = event.item.index,
							progress = (item + sliderItems) / sliderLength;
						$('#contactsSliderProgress').css('transform', 'scaleX(' + progress +')');
					}
				});

				function resizeContactsItem() {
					$('.js-contacts-item').each(function(){
						var smallSlideOnPage = 4,
							smallSlideOnFirstPage = 1;
						if ($(window).width() <= 1000) {
							smallSlideOnPage = 1;
						}
						else if ($(window).width() <= 1280) {
							smallSlideOnPage = 2;
						} else if ($(window).width() <= 1440) {
							smallSlideOnPage = 3;
						}
						$(this).css('width', ($('#contactsSlider').width() / smallSlideOnPage));
						if ($(this).hasClass('contacts-slider__item--wide') && smallSlideOnPage != 1) {
							$(this).css('width', ($('#contactsSlider').width() / smallSlideOnPage * (smallSlideOnPage - smallSlideOnFirstPage)));
						}
					});
				}
			});
		})()
	</script>
<?endif;?>

<?if(isset($arResult["PROGRESS"])):?>
	<div class="object__stages-slider-wrap wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s" id="construction">
		<div class="stages-slider owl-carousel" id="stagesSlider">
			<?foreach($arResult["PROGRESS"] as $arSlide):?>
				<?if($arSlide["PROPERTY_S_VIDEO_VALUE"]):?>
					<?
						$video = $arSlide["PROPERTY_S_VIDEO_VALUE"];
						if(strpos($video, "https://youtu.be/") !== false)
							$videoCode = str_replace("https://youtu.be/", "", $video);
						if(strpos($video, "?v=") !== false)
							$videoCode = substr($video, strpos($video, "?v=") + 3);
					?>
					<div class="stages-slider__item js-stages-slide" data-stage="<?=$arSlide["NAME"]?>">
						<div class="stages-slider__img stages-slider__img--video-preview" style="background-image: url('//img.youtube.com/vi/<?=$videoCode?>/0.jpg');">
							<a href="<?=$video?>" data-fancybox="video"></a>
						</div>
					</div>
				<?else:?>
					<div class="stages-slider__item js-stages-slide" data-stage="<?=$arSlide["NAME"]?>">
						<div class="stages-slider__img" style="background-image: url('<?=$arSlide["PREVIEW_PICTURE"]?>');">
							<a href="<?=$arSlide["PREVIEW_PICTURE"]?>" data-fancybox="image" ></a>
						</div>
					</div>
				<?endif;?>
			<?endforeach;?>

			<div class="stages-slider__item js-stages-slide" data-stage="">
				<?//заглушка, чтобы последний реальный слайд мог развернуться до большого размера?>
			</div>
		</div>
		<div class="stages-slider__header">
			<div class="stages-slider__title h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s"">ход строительства</div>
			<div class="stages-slider__status" id="currStageTitle"><?=$arResult["PROGRESS"][0]["NAME"]?></div>
		</div>
		<div class="stages-slider__nav slider__nav--triangle">
			<div class="stages-slider__nav-title" id="nextStageTitle"><?=$arResult["PROGRESS"][1]["NAME"]?></div>
		</div>
	</div>
	<script>
		(function(){
			document.addEventListener('DOMContentLoaded', function(event){
				var currentSlideIndex = 0;
				$('#stagesSlider').owlCarousel({
					smartSpeed: 500,
					fluidSpeed: 500,
					navSpeed: 500,
					margin: 0,
					nav: true,
					navText: ['', ''],
					dots: false,
					navContainer: '.stages-slider__nav',
					responsive : {
						0: {
							items: 1,
						},
						1001: {
							items: 2
						}
					},
					onInitialized: function(e) {
						var firstItem = e.item.index;
						var firstSlide = $('#stagesSlider .owl-item:eq(' + firstItem + ') .js-stages-slide');
						$('#currStageTitle').html(firstSlide.attr('data-stage'));
					},
					onTranslate: function(e) {
						$('#currStageTitle').addClass('animate-hide-to-left');
						$('#nextStageTitle').addClass('animate-hide-to-left-full');
						currentSlideIndex = e.item.index;
						var currentSlide = $('#stagesSlider .owl-item:eq(' + currentSlideIndex + ') .js-stages-slide');
						var nextSlide = $('#stagesSlider .owl-item:eq(' + (currentSlideIndex + 1) + ') .js-stages-slide');
						setTimeout(function(){
							$('#currStageTitle').html(currentSlide.attr('data-stage'));
							$('#nextStageTitle').html(nextSlide.attr('data-stage'));
							$('#currStageTitle').addClass('animate-visible-to-left');
							$('#nextStageTitle').addClass('animate-visible-to-left-full');
							$('#currStageTitle').removeClass('animate-hide-to-left');
							$('#nextStageTitle').removeClass('animate-hide-to-left-full');
							setTimeout(function(){
								$('#currStageTitle').removeClass('animate-visible-to-left');
								$('#nextStageTitle').removeClass('animate-visible-to-left-full');
							},350);
						},350);
					}
				});

				initFancyBox();
			});
		})()
	</script>
<?endif;?>

<?if(isset($arResult["DOCS"])):?>
	<div class="object__documents documents" id="documents">
		<div class="h1 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">документация</div>
		<?foreach($arResult["DOCS"] as $d_key => $arDoc):?>
			<div class="documents__item document wow fadeInUp <?if($d_key > 9):?>js--hiddenDoc<?endif;?>" data-wow-duration=".5s" data-wow-delay=".2s" <?if($d_key > 9):?>style="display: none"<?endif;?>>
				<div class="document__title"><a href="<?=$arDoc["FILE"]?>" target="_blank"><?=$arDoc["NAME"]?></a></div>
				<div class="document__info">
					<div class="document__date small">Дата публикации: <span><?=$arDoc["DATE_CREATE"]?></span></div>
					<?if($arDoc["PROPERTY_S_PUBLISHER_VALUE"]):?>
						<div class="document__owner small">Опубликовал: <span><?=$arDoc["PROPERTY_S_PUBLISHER_VALUE"]?></span></div>
					<?endif;?>
					<?if($arDoc["PROPERTY_S_POSITION_VALUE"]):?>
						<div class="document__owner-post small">Должность: <span><?=$arDoc["PROPERTY_S_POSITION_VALUE"]?></span></div>
					<?endif;?>
				</div>
			</div>
		<?endforeach;?>
		<?if(count($arResult["DOCS"]) > 10):?>
			<a href="#" class="button button--light button--down button--rectangle-animate js-button-add-more wow fadeIn js--showDocs" data-wow-duration=".5s" data-wow-delay=".2s"><span>Показать все</span><span class="icon-arrow icon-arrow--down"></span></a>
		<?endif;?>
	</div>
	<script>
		$('.js--showDocs').click(function(){
			$('.js--hiddenDoc').show();
			$(this).remove();
		});
	</script>
<?endif;?>

<?if(isset($arResult["NEXT"])):?>
	<?$initParallax = true;?>
	<a href="<?=$arResult["NEXT"]["DETAIL_PAGE_URL"]?>" class="object__next-object wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s" <?if($arResult["NEXT"]["PREVIEW_PICTURE"]):?>style="background-image: url('<?=$arResult["NEXT"]["PREVIEW_PICTURE"]?>');"<?endif;?>>
		<div class="button button--light button--rectangle-animate"><span>следующий проект</span><span class="icon-arrow icon-arrow--right"></span></div>
	</a>
	<script>
		(function(){
			document.addEventListener('DOMContentLoaded', function(event){
				var imagesArray = [];
				// initParallax();
				$(window).on('scroll', function() {
					setPosition('.js-parallax-img');
				});
				$(window).resize(function(){
					imagesArray = [];
					// initParallax();
				});
			});
		})()
	</script>
<?endif;?>

<?//Сайдбар?>
<?$this->SetViewTarget("sidebar");?>
	<aside class="aside  aside--wide">
		<a href="/catalog/" class="back-btn  back-btn--top">к списку<br>объектов<span class="icon-arrow icon-arrow--top"></a>
		<script>
			(function(){
				document.addEventListener('DOMContentLoaded', function(event){
					$('#headerLogo').addClass('page-header__logo--grey page-header__logo--wide');

					$(document).on('scroll', function(){
						var scroll_top = $(document).scrollTop();
						$('.js-aside-links a').each(function(){
							var hash = $(this).attr("href");
							var target = $(hash);
							if (target.position().top <= scroll_top && target.position().top + target.outerHeight() > scroll_top) {
								$('.js-aside-links a.active').removeClass('active');
								$(this).addClass('active');
							} else {
								$(this).removeClass('active');
							}
						});
					});

					$('.js-scroll-to-link').on('click', function(){$(this).addClass('active');});
				});
			})()
		</script>
		<ul class="aside__top-links js-aside-links">
			<?if(!empty($arResult["SLIDES"])):?>
				<li><a class="js-scroll-to-link  active" href="#description">описание</a></li>
			<?endif;?>
			<?if(!empty($arResult["FLATS"])):?>
				<li><a class="js-scroll-to-link" href="#flats">квартиры</a></li>
			<?endif;?>
			<?if(!empty($arResult["COMMERCE"])):?>
				<li><a class="js-scroll-to-link" href="#commerce">коммерческая недвижимость</a></li>
			<?endif;?>
			<?if(!empty($arResult["PARKING"])):?>
				<li><a class="js-scroll-to-link" href="#parking">паркинг</a></li>
			<?endif;?>
			<?if(!empty($arResult["VARIANTS"])):?>
				<li><a class="js-scroll-to-link" href="#purchase">способы приобретения</a></li>
			<?endif;?>
			<?if(isset($arResult["MANAGER"])
					|| $arResult["PROPERTIES"]["S_ADRESS_CONTACT"]["VALUE"]
					|| $arResult["PROPERTIES"]["S_WORKTIME"]["VALUE"]
					|| $arResult["PROPERTIES"]["S_DINER"]["VALUE"]
			):?>
				<li><a class="js-scroll-to-link" href="#contacts">контактная информация</a></li>
			<?endif;?>
			<?if(!empty($arResult["PROGRESS"])):?>
				<li><a class="js-scroll-to-link" href="#construction">ход строительства</a></li>
			<?endif;?>
			<?if(!empty($arResult["DOCS"])):?>
				<li><a class="js-scroll-to-link" href="#documents">документация</a></li>
			<?endif;?>
		</ul>
		<?if(!empty($arResult["PROPERTIES"]["SD_SIDE_LINKS"]["VALUE"])):?>
			<ul class="aside__bottom-links">
				<?foreach($arResult["PROPERTIES"]["SD_SIDE_LINKS"]["VALUE"] as $key => $link):?>
					<li><a class="hover-underline" href="<?=$link?>"><?=$arResult["PROPERTIES"]["SD_SIDE_LINKS"]["DESCRIPTION"][$key]?></a></li>
				<?endforeach;?>
			</ul>
		<?endif;?>
	</aside>
<?$this->EndViewTarget();?>

<?if($showMap || $showMapBlock):?>
	<?$mapID = $arResult["ID"];?>
    <?include($_SERVER["DOCUMENT_ROOT"]."/".$this->getFolder()."/map.php");?>
<?endif;?>

<?if($showMapBlock):?>
	<script>
		<?foreach($arMapContainer as $mapContainer):?>
			<?include $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/includes/map.js.php"?>
		<?endforeach;?>
	</script>
<?endif;?>

<div class="modal  modal--flat" style="display: none;" id="bookingModal">
    <div class="modal__wrap">
        <div class="modal__title h1">забронировать</div>
        <form class="modal__form js-form" autocomplete="off" data-ya-target="order_done">
			<?=bitrix_sessid_post();?>
			<input type="hidden" name="action" value="FLAT_BOOK">
			<?foreach($arResult["MANAGER"] as $arManager):?>
				<input type="hidden" name="managers[]" value="<?=$arManager["ID"];?>">
			<?endforeach;?>
			<input type="hidden" name="FIELDS[FLAT_NAME]" value="<?=$arResult["HOUSE"]["NAME"]?>, <?=$arResult["NAME"]?>" id="js-field_flatName">
            <label class="input-label">
                <span>* Фамилия Имя Отчество</span>
                <input type="text" name="FIELDS[NAME]" class="required">
            </label>
            <label class="input-label">
                <span>* Мобильный телефон</span>
                <input type="text" name="FIELDS[PHONE]" autocomplete="nope" class="required input--phone" pattern="^\+7\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}">
            </label>
            <label class="input-label">
                <span>Адрес эл. почты</span>
                <input type="email" name="FIELDS[EMAIL]">
            </label>
            <label class="input-label  input-label--checkbox">
                <input type="checkbox" name="agreed" class="checkbox required">
                <span class="checkbox-custom  checkbox-custom--mark"></span>
                <span class="input-label__agreed-text">Согласен с условиями <a href="/personal-confirm/">Согласия</a> на обработку персональных данных</span>
            </label>
            <button class="modal__submit button button--center-black" type="submit" data-hoverContent="Забронировать" data-content="Забронировать"></button>
        </form>
    </div>
</div>

<?if($initParallax):?>
	<script>
		document.addEventListener('DOMContentLoaded', function(event){
			initParallax();
		});
	</script>
<?endif;?>