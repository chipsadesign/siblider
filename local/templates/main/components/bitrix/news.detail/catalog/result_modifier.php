<?
if (intval($arResult["DETAIL_PICTURE"]["ID"]) > 0) {
	$arFile = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"]["ID"], ["width" => 1000, "height" => 1000]);
	$src = $arFile["src"];
	// $src = substr($src, 0, strrpos($src, ".")).".webp";
	$arResult["DETAIL_PICTURE"]["SRC"] = $src;
}

/**
 * Свойства для отображения в шапке
 */
$arResult["MAIN_PROPS"] = [
	"S_ADRESS",
	"S_DATE_START",
	"N_FLOORS",
	"L_MATERIAL",
	"S_DATE_END",
	"N_ENTRANCE",
	"S_FACING",
	"S_STATUS",
];

/**
 * Дёргаем квартиры текущего дома
 */
$arFlatSelect = [
	"ID",
	"NAME",
	"CODE",
	"DETAIL_PAGE_URL",
	"PROPERTY_N_FLOOR",
	"PROPERTY_N_ROOMS",
	"PROPERTY_N_AREA",
	"PROPERTY_N_PRICE",
	"PROPERTY_S_ENTRANCE",
	"PROPERTY_F_PRESET",
	"PROPERTY_N_PRESET_CODE",
	"PROPERTY_ID_HOUSE",
	"PROPERTY_S_STATUS",
];
$maxPrice = 0;
$minPrice = 0;
$rsFlat = CIBlockElement::GetList(["PROPERTY_N_PRICE" => "ASC"], ["IBLOCK_ID" => IBLOCK_ID_FLAT, "PROPERTY_E_HOUSE" => $arResult["ID"], "!PROPERTY_L_COMMERCE_VALUE" => "Y", "PROPERTY_S_STATUS" => "AVAILABLE", "!PROPERTY_N_PRESET_CODE" => "кл"], false, false, $arFlatSelect);
while($arFlat = $rsFlat->GetNext()){
	$arFlatResult = [];
	foreach($arFlat as $key => $val){
		if(strpos($key, "~") === 0)
			continue;
		if(strpos($key, "PROPERTY_") === 0){
			$key = str_replace("PROPERTY_", "", $key);
			$key = str_replace("_VALUE", "", $key);

			if($key == "F_PRESET"){
				if($val){
					$arFlatResult["F_PRESET_ORIGINAL"] = CFile::GetPath($val);
					$arFile = CFile::ResizeImageGet($val, array("width" => 70, "height" => "70"), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					$arFlatResult["PROPERTIES"][$key."_PREVIEW"] = $arFile["src"];

					$arFile = CFile::ResizeImageGet($val, array("width" => 600, "height" => "600"), BX_RESIZE_IMAGE_PROPORTIONAL, true);
				}else{
					$arFlatResult["PROPERTIES"][$key."_PREVIEW"] = "/local/templates/main/images/holderPreview.png";

					$arFile["src"] = "/local/templates/main/images/holder.png";
				}
				$val = $arFile["src"];
			}elseif(strpos($key, "F_") === 0 && !is_null($val)){
				$val = CFile::GetPath($val);
			}

			$arFlatResult["PROPERTIES"][$key] = $val;

			if($key == "N_PRICE" && ($val < $minPrice || $minPrice == 0)){
				$minPrice = $val;
			}
			if($key == "N_PRICE" && ($val > $maxPrice || $maxPrice == 0)){
				$maxPrice = $val;
			}
		}else{
			$arFlatResult[$key] = $val;
		}
	}
	$arResult["AREA"][] = $arFlatResult["PROPERTIES"]["N_AREA"];
	$arResult["ROOMS"][] = $arFlatResult["PROPERTIES"]["N_ROOMS"];
	$arResult["FLATS"][] = $arFlatResult;
}

/**
 * Узнаём, есть ли коммерческая недвижимость
 */
$rsCommerc = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_FLAT, "PROPERTY_E_HOUSE" => $arResult["ID"], "PROPERTY_L_COMMERCE_VALUE" => "Y", "!PROPERTY_N_PRESET_CODE" => "кл"], false, ["nTopCount" => 1], ["ID"]);
if($arCommerc = $rsCommerc->Fetch()){
	$arResult["COMMERCE"] = "Y";
}

/**
 * Узнаём, есть ли парковка
 */
$rsParking = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_HOUSE, "PROPERTY_E_PROJECT" => $arResult["PROPERTIES"]["E_PROJECT"]["VALUE"], "PROPERTY_S_TYPE" => "PARKING"], false, false, ["ID", "DETAIL_PAGE_URL", "NAME", "PROPERTY_E_PROJECT"]);
if($arParking = $rsParking->GetNext()){
	$arResult["PARKING"] = $arResult["DETAIL_PAGE_URL"]."parking/";
}

$arResult["MIN_PRICE"] = $minPrice;
$arResult["MAX_PRICE"] = $maxPrice;

$arResult["ROOMS"] = array_unique($arResult["ROOMS"]);
sort($arResult["ROOMS"]);

$arResult["AREA"] = array_unique($arResult["AREA"]);
sort($arResult["AREA"]);

/**
 * Дёргаем контентные блоки
 */
$rsSlide = CIBlockElement::GetList(["SORT" => "ASC"], ["IBLOCK_ID" => IBLOCK_ID_HOUSE_SLIDES, "ACTIVE" => "Y", "PROPERTY_E_HOUSE" => $arResult["ID"]], false, false, ["ID", "NAME", "PROPERTY_L_TYPE", "PREVIEW_PICTURE", "PREVIEW_TEXT", "PROPERTY_F_IMG", "PROPERTY_L_TYPE", "CODE", "PROPERTY_L_MAP", "PROPERTY_S_IMG_DESC"]);
while($arSlide = $rsSlide->Fetch()){
	if($arSlide["PREVIEW_PICTURE"])
		$arSlide["PREVIEW_PICTURE"] = CFile::GetPath($arSlide["PREVIEW_PICTURE"]);
	if(!empty($arSlide["PROPERTY_F_IMG_VALUE"])){
		foreach($arSlide["PROPERTY_F_IMG_VALUE"] as $key => $id){
			$arSlide["SLIDER"][] = [
				"SRC" => CFile::GetPath($id),
				"DESCRIPTION" => $arSlide["PROPERTY_F_IMG_DESCRIPTION"][$key]
			];
		}
	}
	$arResult["SLIDES"][] = $arSlide;
}

/**
 * Дёргаем баннеры
 */
$rsBanner = CIBlockElement::GetList(["SORT" => "ASC"], ["IBLOCK_ID" => IBLOCK_ID_HOUSE_BANNERS, "ACTIVE" => "Y", "PROPERTY_E_HOUSE" => $arResult["ID"]], false, false, ["ID", "PREVIEW_PICTURE", "PROPERTY_S_TEXT", "PROPERTY_S_LINK_TEXT", "PROPERTY_S_LINK_URL"]);
while($arBanner = $rsBanner->Fetch()){
	if($arBanner["PREVIEW_PICTURE"])
		$arBanner["PREVIEW_PICTURE"] = CFile::GetPath($arBanner["PREVIEW_PICTURE"]);
	$arResult["BANNERS"][] = $arBanner;
}

/**
 * Дёргаем способы приобретения
 */
$rsVariants = CIBlockElement::GetList([], ["IBLOCK_ID" => IBLOCK_ID_VARIANTS, "ID" => $arResult["PROPERTIES"]["E_VARIANTS"]["VALUE"]], false, false, ["ID", "NAME", "CODE", "PROPERTY_S_SUBHEAD", "PREVIEW_TEXT"]);
while($arVariant = $rsVariants->Fetch()){
	$arResult["VARIANTS"][] = $arVariant;
}

/**
 * Дёргаем менеджеров
 */
if($arResult["PROPERTIES"]["E_MANAGER"]["VALUE"]){
	$rsManager = CIBlockElement::GetList(["SORT" => "ASC"], ["IBLOCK_ID" => IBLOCK_ID_MANAGER, "ID" => $arResult["PROPERTIES"]["E_MANAGER"]["VALUE"], "ACTIVE"], false, false, ["ID", "NAME", "PROPERTY_S_POSITION", "PREVIEW_PICTURE", "PROPERTY_S_PHONE", "PROPERTY_S_EMAIL"]);
	while($arManager = $rsManager->Fetch()){
		$arManager["PREVIEW_PICTURE"] = CFile::GetPath($arManager["PREVIEW_PICTURE"]);
		$arResult["MANAGER"][] = $arManager;
	}
}

/**
 * Дёргаем ход строительства
 */
$rsProgress = CIBlockElement::GetList(["SORT" => "ASC", "DATE_CREATE" => "DESC"], ["IBLOCK_ID" => IBLOCK_ID_PROGRESS, "ACTIVE" => "Y", "PROPERTY_E_HOUSE" => $arResult["ID"]], false, false, ["ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_S_VIDEO"]);
while($arProgress = $rsProgress->Fetch()){
	if($arProgress["PREVIEW_PICTURE"])
		$arProgress["PREVIEW_PICTURE"] = CFile::GetPath($arProgress["PREVIEW_PICTURE"]);
	$arResult["PROGRESS"][] = $arProgress;
}

/**
 * Дёргаем документы
 */
$rsDocs = CIBlockElement::GetList(["PROPERTY_D_DATE" => "DESC,nulls", "DATE_CREATE" => "DESC"], ["IBLOCK_ID" => IBLOCK_ID_DOCS, "ACTIVE" => "Y", "PROPERTY_E_HOUSE" => $arResult["ID"]], false, false, ["ID", "NAME", "PROPERTY_S_PUBLISHER", "PROPERTY_F_FILE", "PROPERTY_S_POSITION", "DATE_CREATE", "PROPERTY_D_DATE"]);
while($arDocs = $rsDocs->Fetch()){
	$arDocs["FILE"] = CFile::GetPath($arDocs["PROPERTY_F_FILE_VALUE"]);
	if($arDocs["PROPERTY_D_DATE_VALUE"])
		$arDocs["DATE_CREATE"] = $arDocs["PROPERTY_D_DATE_VALUE"];
	$arResult["DOCS"][] = $arDocs;
}

/**
 * Следующий проект
 */
$arNavParams = [
	"nPageSize" => 1,
	"nElementID" => $arResult["PROPERTIES"]["E_PROJECT"]["VALUE"]
];
$rsElement = CIBlockElement::GetList(["SORT" => "ASC"], ["IBLOCK_ID" => IBLOCK_ID_OBJECT, "ACTIVE" => "Y"], false, $arNavParams, ["ID", "NAME"]);
while($arElement = $rsElement->Fetch()){
	if($arElement["ID"] == $arResult["PROPERTIES"]["E_PROJECT"]["VALUE"])
		continue;
	if($arElement){
		$rsHouse = CIBlockElement::GetList(["SORT" => "ASC"], ["PROPERTY_E_PROJECT" => $arElement["ID"], "ACTIVE" => "Y", "!PROPERTY_S_TYPE" => "PARKING"], false, ["nTopCount" => 1], ["ID", "DETAIL_PICTURE", "DETAIL_PAGE_URL"]);
		if($arHouse = $rsHouse->GetNext()){
			if($arHouse["DETAIL_PICTURE"])
				$arHouse["PREVIEW_PICTURE"] = CFile::GetPath($arHouse["DETAIL_PICTURE"]);
			$arResult["NEXT"] = $arHouse;
		}
	}
}

// Получаем номера телефонов менеджеров обьекта
$arResult["MANAGER_PHONES"] = [];
if(!empty($arResult["MANAGER"])){
	foreach ($arResult["MANAGER"] as $arManager) {
		if (empty($arManager["PROPERTY_S_PHONE_VALUE"])) {
			continue;
		}
		$strPhonehref = $arManager["PROPERTY_S_PHONE_VALUE"];

		$strPhonehref = str_replace("+7", "*", $arManager["PROPERTY_S_PHONE_VALUE"]);
		$strPhonehref = str_replace(["-", " ", "(", ")",], "", $strPhonehref);
		if(strlen($strPhonehref) == 7){
			$strPhonehref = "8319".$strPhonehref;
		}
		$arResult["MANAGER_PHONES"][] = [
			"TEXT" => $arManager["PROPERTY_S_PHONE_VALUE"],
			"LINK" => $strPhonehref,
		];
	}
}

$arResult["MANAGER_PHONES_TITLE"] = "МЕНЕДЖЕР".((count($arResult["MANAGER_PHONES"]) == 1) ? "" : "Ы")." ОБЪЕКТА";