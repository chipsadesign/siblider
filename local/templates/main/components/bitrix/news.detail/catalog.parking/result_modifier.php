<?
/**
 * Свойства для отображения в шапке
 */
$arResult["MAIN_PROPS"] = [
    "S_ADRESS",
    "S_DATE_START",
    "N_FLOORS",
    "L_MATERIAL",
    "S_DATE_END",
    "N_ENTRANCE",
    "S_FACING"
];

/**
 * Дёргаем квартиры текущего дома
 */
$arFlatSelect = [
    "ID",
    "NAME",
    "CODE",
    "DETAIL_PAGE_URL",
    "PROPERTY_N_FLOOR",
    "PROPERTY_N_ROOMS",
    "PROPERTY_N_AREA",
    "PROPERTY_N_PRICE",
    "PROPERTY_S_ENTRANCE",
    "PROPERTY_F_PRESET",
    "PROPERTY_ID_HOUSE",
];
$maxPrice = 0;
$minPrice = 0;
$rsFlat = CIBlockElement::GetList(["PROPERTY_N_PRICE" => "ASC", "NAME" => "ASC"], ["IBLOCK_ID" => IBLOCK_ID_FLAT, "PROPERTY_E_HOUSE" => $arResult["ID"], "PROPERTY_S_STATUS" => "AVAILABLE"], false, false, $arFlatSelect);
while($arFlat = $rsFlat->GetNext()){
    $arFlatResult = [];
    foreach($arFlat as $key => $val){
        if(strpos($key, "~") === 0)
            continue;
        if(strpos($key, "PROPERTY_") === 0){
            $key = str_replace("PROPERTY_", "", $key);
            $key = str_replace("_VALUE", "", $key);

            if(strpos($key, "F_") === 0 && !is_null($val))
                $val = CFile::GetPath($val);

            $arFlatResult["PROPERTIES"][$key] = $val;

            if($key == "N_PRICE" && ($val < $minPrice || $minPrice == 0)){
                $minPrice = $val;
            }
            if($key == "N_PRICE" && ($val > $maxPrice || $maxPrice == 0)){
                $maxPrice = $val;
            }
        }else{
            $arFlatResult[$key] = $val;
        }
    }
    $arResult["AREA"][] = $arFlatResult["PROPERTIES"]["N_AREA"];
    $arResult["ROOMS"][] = $arFlatResult["PROPERTIES"]["N_ROOMS"];
    $arResult["FLATS"][] = $arFlatResult;
}

foreach($arResult["FLATS"] as $arItem){
    if($arItem["PROPERTIES"]["F_PRESET"]){
        $arResult["PRESET"] = $arItem["PROPERTIES"]["F_PRESET"];
    }
}