<div class="page__left-content">
    <div class="page__left-content-wrap">
        <div class="page__header page__header--with-border">
            <h1 class="page__title  wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".2s"><?=$arResult["NAME"]?></h1>
        </div>
        <div class="table">
            <div class="table__title wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
                <div class="table__floor">этаж</div>
                <div class="table__place">№ места</div>
                <div class="table__square">площадь</div>
                <div class="table__price">цена</div>
            </div>
            <div class="table__content wow fadeIn" data-wow-duration=".5s" data-wow-delay=".3s">
                <?foreach($arResult["FLATS"] as $arItem):?>
                    <?$arProps = $arItem["PROPERTIES"];?>
                    <div class="js-change-room table__item">
                        <div class="table__floor"><?=$arProps["N_FLOOR"]?></div>
                        <div class="table__place"><span data-hoverContent="<?=$arItem["NAME"]?>" data-content="<?=$arItem["NAME"]?>"></span></div>
                        <div class="table__square"><?=$arProps["N_AREA"]?> м<sup>2</sup></div>
                        <div class="table__price"><?=number_format($arProps["N_PRICE"], 0, ".", " ");?> р.</div>
                    </div>
                <?endforeach;?>
            </div>
        </div>
        <a href="tel:+73912152010" class="room__footer">Подробности по телефону +7(391) 215-20-10</a>
    </div>
</div>
<div class="page__right-content" id="fancyboxParent">
    <div class="preloader">
        <div class="preloader__item"></div>
        <div class="preloader__item"></div>
        <div class="preloader__item"></div>
    </div>

    <div class="room__photo wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
        <?if(isset($arResult["PRESET"])):?>
            <a href="<?=$arResult["PRESET"]?>"  id="roomPhoto" data-fancybox="image">
                <img src="<?=$arResult["PRESET"]?>" alt="photo">
            </a>
        <?endif;?>

        <div class="characteristics room__characteristics" id="roomCharacteristics">
            <?if($arResult["PROPERTIES"]["S_ADRESS"]["VALUE"]):?>
                <div class="characteristics__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".2s">
                    <div class="characteristics__title">адрес</div>
                    <div class="characteristics__value"><?=$arResult["PROPERTIES"]["S_ADRESS"]["VALUE"]?></div>
                </div>
            <?endif;?>
            <?if($arResult["PROPERTIES"]["S_DATE_END"]["VALUE"]):?>
                <div class="characteristics__item wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".4s">
                    <div class="characteristics__title">срок сдачи</div>
                    <div class="characteristics__value"><?=$arResult["PROPERTIES"]["S_DATE_END"]["VALUE"]?></div>
                </div>
            <?endif;?>
        </div>
    </div>

    <a data-fancybox data-src="#personalOffer" href="javascript:;"  class="button button--black button--icon-animate wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".2s">
        <span>получить персональное предложение</span><span class="icon-arrow icon-arrow--right"></span>
    </a>
    <div class="modal  modal--flat" style="display: none;" id="personalOffer">
        <div class="modal__wrap">
            <div class="modal__title h1">персональное предложение</div>
            <form class="modal__form js-form" data-ya-target="order_done">
                <?=bitrix_sessid_post();?>
                <input type="hidden" name="action" value="PARKING_PERSONAL">
                <input type="hidden" name="FIELDS[HOUSE_NAME]" value="<?=$arResult["NAME"]?>">
                <label class="input-label">
                    <span>* Фамилия Имя Отчество</span>
                    <input type="text" name="FIELDS[NAME]" class="required">
                </label>
                <label class="input-label">
                    <span>* Мобильный телефон</span>
                    <input type="text" name="FIELDS[PHONE]" class="required input--phone" pattern="^\+7\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}">
                </label>
                <label class="input-label">
                    <span>Адрес эл. почты</span>
                    <input type="email" name="FIELDS[EMAIL]">
                </label>
                <label class="input-label  input-label--checkbox">
                    <input type="checkbox" name="agreed" class="checkbox required">
                    <span class="checkbox-custom  checkbox-custom--mark"></span>
                    <span class="input-label__agreed-text">Согласен с условиями <a href="/personal-confirm/">Согласия</a> на обработку персональных данных</span>
                </label>
                <button class="modal__submit button button--center-black" type="submit" data-hoverContent="Получить" data-content="Получить"></button>
            </form>
        </div>
    </div>
    <div id="successMessage" class="success-message" style="display: none;">
        <div class="success-message__inner">
            <div class="h1">Ваша заявка успешно отправлена!</div>
            <div class="success-message__subtitle">Наши менеджеры перезвонят вам в течение рабочего дня.</div>
        </div>
    </div>
</div>
<script>
    (function(){
        document.addEventListener('DOMContentLoaded', function(event){
            $('.page-header').addClass('page-header--no-bg');
            $('.js-change-room').on('click', function(){
                if ($(this).hasClass('is-active')) {
                    return
                }
                $('.js-change-room.is-active').removeClass('is-active');
                $(this).addClass('is-active');

                //before
                $('#roomPhoto').addClass('fadeOutLeft');
                $('#roomCharacteristics').addClass('fadeOut');
                $('#fancyboxParent').addClass('is-changed');

                //TODO: загрузка контента

                //after
                //setTimeout Для демонстрации анимации
                setTimeout(function(){
                    $('#roomPhoto').removeClass('fadeOutLeft');
                    $('#roomCharacteristics').removeClass('fadeOut');
                    $('#roomPhoto').addClass('fadeInRightFull');
                    $('#roomCharacteristics').addClass('fadeIn');
                    $('#fancyboxParent').removeClass('is-changed');
                    setTimeout(function(){
                        $('#roomPhoto').removeClass('fadeInRightFull');
                    }, 700);
                }, 2000);

            });
            initFancyBox();
        });
    })()
</script>