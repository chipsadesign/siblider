<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<a class="menu__social" title="<?=$arItem["UF_NAME"]?>" href="<?=$arItem["UF_LINK"]?>"><img src="<?=$arItem["UF_IMG"]?>" alt="<?=$arItem["UF_NAME"]?>"></a>
<?endforeach;?>