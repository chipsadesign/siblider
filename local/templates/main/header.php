<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
	<script>
		window.onpageshow = function(event) {
		    if (event.persisted) {
		        window.location.reload();
		    }
		};
	</script>
    <?include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/includes/head.php");?>
    <body>
		<div class="curtain" id="curtain">
	        <div class="curtain__left"></div>
	        <div class="curtain__line"></div>
	        <div class="curtain__right"></div>
	    </div>
		<header class="page-header" id="pageHeader">
			<div class="page-header__logo" id="headerLogo">
				<a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.svg" alt="logo"></a>
			</div>
			<div class="page-header__menu" id="headerMenu">
				<div class="page-header__menu-btn hover-underline" id="menuOpenBtn">меню</div>
				<ul class="page-header__menu-list  is-hidden" id="mainPageMenu">
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu",
						"main",
						[
							"ROOT_MENU_TYPE" => "top",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "3600",
						],
						false
					);?>
				</ul>
			</div>
			<?/*if($APPLICATION->GetCurDir() == "/"):?>
				<div class="page-header__ctrl">
					<a href="/news/open-doors-day-february-15/" class="button button--center-black button--th-main"
						data-hovercontent="ДЕНЬ ОТКРЫТЫХ ДВЕРЕЙ: 15 ФЕВРАЛЯ"
						data-content="ДЕНЬ ОТКРЫТЫХ ДВЕРЕЙ: 15 ФЕВРАЛЯ">
					</a>
				</div>
			<?endif*/?>
			<div class="page-header__right">
				<div class="page-header__links" id="headerLinks">
					<?=Siblider::showMonthOfferLink();?>
					<a class="hover-underline" href="/contacts/">контакты</a>
					<?
						// вывод номера телефона, необходимость вывода проверяется в футере
						$APPLICATION->ShowProperty("MAIN_PHONE");
					?>
				</div>
				<?$APPLICATION->ShowViewContent("managers__contacts");?>
			</div>
			<div class="page-header__mobile-burger" id="mobileBurger"><span class="icon-burger"></span></div>
			<script>
				(function(){
					document.addEventListener('DOMContentLoaded', function(event){
						//штора
						$('#curtain').addClass('is-hidden-end');
			            setTimeout(function(){
			                $('#curtain').hide();
			                $('#curtain').removeClass('is-hidden-end');
			                $('#curtain').addClass('is-hidden-start');
			            }, 700);

						//переход между страницами сайта
		                $(document).on("click", ".page a:not([href^='#']):not([data-fancybox]):not([target='_blank']):not([href^='tel']):not([href^='mailto']):not([href^='javascript:;']), .aside a:not([href^='#']):not([data-fancybox]):not([target='_blank']):not([href^='tel']):not([href^='mailto']):not([href^='javascript:;']), #pageHeader a:not([href^='#']):not([data-fancybox]):not([target='_blank']):not([href^='tel']):not([href^='mailto']):not([href^='javascript:;']), #menu a:not([href^='#']):not([data-fancybox]):not([target='_blank']):not([href^='tel']):not([href^='mailto']):not([href^='javascript:;'])", function(e){
		                	if($(this).parents().hasClass('bx-panel-folded') > 0){
		                		window.location.href = $(this).attr("href");
		                		return false;
		                	}
				            e.preventDefault();
							$('#curtain').show();
					        setTimeout(function(){
					            $('#curtain').removeClass('is-hidden-start');
					        }, 100);
				            var self = this;
				            setTimeout(function() {
				                window.location.href = $(self).attr("href");
				            }, 1000);
				        });
					});
				})()
			</script>
		</header>

		<?$APPLICATION->ShowViewContent("sidebar");?>

		<div class="page <?=$APPLICATION->ShowProperty("page_class")?>">
			<div class="page__wrap <?=$APPLICATION->ShowProperty("page__wrap_class");?>">