			</div>
			<footer class="page-footer" id="pageFooter">
				<a href="/journal/" class="page-footer__log">Журнал изменений</a>
				<div><span>Сделано в </span><a href="http://chipsa.ru/?utm_source=siblider&utm_medium=works&utm_campaign=all">CHIPSA</a></div>
			</footer>
		</div>

		<div class="menu is-hidden" id="menu">
			<div class="menu__left animation-start" id="menuLeft">
				<img src="<?=SITE_TEMPLATE_PATH?>/images/menuLogo.svg" alt="logo" class="menu__logo">
				<div class="menu__close" id="menuCloseBtn"><span class="icon-close"></span>закрыть</div>
				<ul class="menu__big-list">
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu",
						"main",
						[
							"ROOT_MENU_TYPE" => "top",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "3600",
						],
						false
					);?>
					<?$APPLICATION->ShowProperty("OFFERS_OF_THE_MONTH_LINK");?>
				</ul>
				<div class="menu__logo-title"><img src="<?=SITE_TEMPLATE_PATH?>/images/siblider.svg" alt="siblider"></div>
			</div>
			<div class="menu__right animation-start" id="menuRight">
				<ul class="menu__small-list">
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu",
						"main",
						[
							"ROOT_MENU_TYPE" => "second",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "3600",
						],
						false
					);?>
					<li><a class="hover-underline" href="tel:+73912152010">+7 (391) 215-20-10</a></li>
					<li>
						<?$APPLICATION->IncludeComponent(
                            "custom:socnet",
                            "footer",
                            array(),
                            false
                        );?>
					</li>
				</ul>
			</div>
		</div>

		<div id="rotateMessage" class="rotate-message">
	        <div class="rotate-message__inner">
	            <div class="rotate-message__icon">
	                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 312.513 312.513" style="enable-background:new 0 0 312.513 312.513;" xml:space="preserve" width="100%" height="100%">
	                    <g>
	                        <circle cx="97.106" cy="210.198" r="8.35" fill="#BA9077"></circle>
	                        <path d="M286.322,135.845H148.707V63.583c0-14.442-11.749-26.191-26.191-26.191H26.191C11.749,37.392,0,49.141,0,63.583V258.36   c0,14.442,11.749,26.191,26.191,26.191h65.354h22.618h172.16c14.442,0,26.191-11.75,26.191-26.191v-96.324   C312.513,147.594,300.764,135.845,286.322,135.845z M126.568,153.845h13.129c0.003,0,0.006,0,0.01,0s0.006,0,0.01,0h121.511   v112.707h-134.66V153.845z M91.544,135.845c-14.442,0-26.191,11.75-26.191,26.191v61.301H18V88.677h112.707v47.167H91.544z    M26.191,55.392h96.325c4.517,0,8.191,3.674,8.191,8.191v7.094H18v-7.094C18,59.066,21.674,55.392,26.191,55.392z M18,258.36   v-17.023h47.353v17.023c0,2.861,0.468,5.612,1.319,8.191H26.191C21.674,266.552,18,262.877,18,258.36z M83.353,258.36v-96.324   c0-4.517,3.675-8.191,8.191-8.191h17.023v112.707H91.544C87.028,266.552,83.353,262.877,83.353,258.36z M294.513,258.36   c0,4.517-3.674,8.191-8.191,8.191h-7.094V153.845h7.094c4.517,0,8.191,3.675,8.191,8.191V258.36z" fill="#BA9077"></path>
	                        <path d="M193.506,79.835c1.464,1.464,3.384,2.197,5.303,2.197s3.839-0.732,5.303-2.197c2.929-2.929,2.929-7.678,0-10.606   l-5.774-5.774c14.772,3.285,26.481,14.78,30.072,29.436l-5.583-5.583c-2.928-2.929-7.677-2.929-10.606,0   c-2.929,2.929-2.929,7.678,0,10.606l19.537,19.538c1.406,1.407,3.314,2.197,5.303,2.197s3.897-0.79,5.303-2.197l19.534-19.534   c2.929-2.929,2.929-7.678,0-10.606c-2.929-2.929-7.678-2.929-10.606,0L244,94.604c-3.454-24.236-22.793-43.409-47.106-46.618   l7.222-7.222c2.929-2.929,2.929-7.678,0-10.606c-2.929-2.929-7.678-2.929-10.606,0l-19.537,19.538   c-1.407,1.407-2.197,3.314-2.197,5.303s0.79,3.897,2.197,5.303L193.506,79.835z" fill="#BA9077"></path>
	                    </g>
	                </svg>
	            </div>
	            <div class="rotate-message__title">Пожалуйста, поверните ваше устройство в вертикальное положение</div>
	        </div>
	    </div>
		<!-- BEGIN JIVOSITE CODE {literal} -->
		<script type='text/javascript'>
		(function(){ var widget_id = 'ettv4T01cj';var d=document;var w=window;function l(){var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
		</script>
		<!-- {/literal} END JIVOSITE CODE -->
		<script type="text/javascript">
		    (function (d, w, c) {
		        (w[c] = w[c] || []).push(function() {
		            try {
		                w.yaCounter20863021 = new Ya.Metrika({id:20863021,
		                        webvisor:true,
		                        clickmap:true,
		                        trackLinks:true,
		                        accurateTrackBounce:true});
		            } catch(e) { }
		        });
		        var n = d.getElementsByTagName("script")[0],
		            s = d.createElement("script"),
		            f = function () { n.parentNode.insertBefore(s, n); };
		        s.type = "text/javascript";
		        s.async = true;
		        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
		        if (w.opera == "[object Opera]") {
		            d.addEventListener("DOMContentLoaded", f, false);
		        } else { f(); }
		    })(document, window, "yandex_metrika_callbacks");
		    </script>
		<noscript><div><img src="//mc.yandex.ru/watch/20863021" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    </body>
</html>

<?if($APPLICATION->GetViewContent("sidebar") == ""):?>
	<?
		$showSidebar = true;
		$showTitle = true;
		if(
			$APPLICATION->GetCurPage() == "/"
		)
			$showSidebar = false;
		if(
			$APPLICATION->GetCurPage() == "/404.php"
			|| $APPLICATION->GetCurPage() == "/catalog/"
			|| $APPLICATION->GetCurPage() == "/trade-in-service/"
		)
			$showTitle = false;
	?>
	<?if($showSidebar):?>
		<?$title = $APPLICATION->GetTitle(true);?>
		<?ob_start();?>
			<aside class="aside">
				<a href="/" class="back-btn" id="backBtn"><span class="icon-arrow"></span></a>
				<script>
					(function(){
						document.addEventListener('DOMContentLoaded', function(event){
							$('#headerLogo').addClass('page-header__logo--grey');
						});
					})()
				</script>
				<?if($showTitle):?>
					<div class="aside__title small"><?=$title?></div>
				<?endif;?>
			</aside>
    		<?$out1 = ob_get_contents();?>
    	<?ob_end_clean();?>

		<?$APPLICATION->AddViewContent("sidebar", $out1);?>
	<?endif;?>

<?endif;?>
<?
	// Если не выводятся номера менеджеров, выводим основной телефон
	if(empty($APPLICATION->__view["managers__contacts"])){
		$APPLICATION->SetPageProperty(
			"MAIN_PHONE",
			"<a class=\"hover-underline page-header__tel\" href=\"tel:83912152010\">+7 (391) 215-20-10</a>"
		);
	}
?>